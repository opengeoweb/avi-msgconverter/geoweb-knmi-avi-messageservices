FROM maven:3-openjdk-11 as jdk-build

WORKDIR /opt/app
COPY pom.xml .
RUN mvn verify --fail-never
COPY . .
RUN mvn package --batch-mode \
    && mv target/geoweb-knmi-avi-messageservices-*-SNAPSHOT.jar geoweb-knmi-avi-messageservices.jar

EXPOSE 8080
ENV JAVA_OPTS="-Djava.security.egd=file:/dev/urandom"
ENTRYPOINT ["java", "-jar", "geoweb-knmi-avi-messageservices.jar"]


# Build a custom Java runtime (this could be improved by creating a smaller
# runtime, only including the modules that are actually required)
FROM eclipse-temurin:11 as jre-build
WORKDIR /opt/app
COPY --from=jdk-build /opt/app/geoweb-knmi-avi-messageservices.jar .
RUN $JAVA_HOME/bin/jlink \
    --add-modules ALL-MODULE-PATH \
    --strip-debug \
    --no-man-pages \
    --no-header-files \
    --compress=2 \
    --output /javaruntime


# Build Debian-based deployment image
FROM debian:bookworm-slim

# Copy Java runtime
ENV JAVA_HOME=/opt/java/openjdk
ENV PATH "${JAVA_HOME}/bin:${PATH}"
COPY --from=jre-build /javaruntime $JAVA_HOME

# Copy application
WORKDIR /opt/app
COPY --from=jdk-build /opt/app/geoweb-knmi-avi-messageservices.jar .
COPY start.sh /opt/app/

ENV AVI_CONTAINER_PORT=8080

ENV JAVA_OPTS="-Djava.security.egd=file:/dev/urandom"
ENTRYPOINT ["sh", "start.sh"]
