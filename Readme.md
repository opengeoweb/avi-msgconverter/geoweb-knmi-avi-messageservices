[![pipeline status](https://gitlab.com/opengeoweb/avi-msgconverter/geoweb-knmi-avi-messageservices/badges/master/pipeline.svg)](https://gitlab.com/opengeoweb/avi-msgconverter/geoweb-knmi-avi-messageservices/-/commits/master)

# GeoWeb avi messageservices

Web service for converting GeoWeb TAF/SIGMET/AIRMET JSON products into TAC (alphanumeric) or IWXXM (XML) products.
The server uses the FMI fmi-avi-messageconverter libraries (see https://github.com/fmidev/fmi-avi-messageconverter) and is based on the Spring Boot framework.

## Content

[[_TOC_]]

## Running

### To run the server locally

`mvn spring-boot:run`

The server will listen to port 8080 by default

## Testing

### Run all unit tests

`mvn test`

### Run a specific test

`mvn test -Dtest=TestGeoWebSigmet`

## Running a healthcheck

`curl --request GET http://localhost:8080/healthcheck`

## Examples of tests for TAC generation

### To post a Sigmet Json and get a Tac:

`curl --header "Content-Type: application/json" --request POST --data @./src/test/resources/sigmet-readGeoWebSigmetAndCreateSigmet.json http://localhost:8080/getsigmettac`

Returns:

```
EHAA SIGMET 2 VALID 091315/091459 EHDB-
EHAA AMSTERDAM FIR TEST
SEV ICE OBS AT 1315Z WI N5321 E00025 - N5036 E00928 - N5001 E00247 - N5321 E00025 FL150 NC FCST AT 1459Z ENTIRE FIR=
```

### To post an Airmet Json and get a Tac:

`curl --header "Content-Type: application/json" --request POST --data @./src/test/resources/airmet-readGeoWebAirmetAndCreateAirmet.json http://localhost:8080/getairmettac`

Returns:

```
EHAA AIRMET A01 VALID 191533/191633 EHDB-
EHAA AMSTERDAM FIR SFC VIS 1000M (FG) OBS AT 1533Z WI N5403 E00448 - N5338 E00600 - N5320 E00437 - N5403 E00448 FL150 STNR WKN=
```

### To post a Taf Json and get a Tac:

`curl --header "Content-Type: application/json" --request POST --data @./src/test/resources/taf-readGeoWebTafAndCreateTaf.json http://localhost:8080/gettaftac`

Returns:

```
TAF EHAM 161800Z 1618/1723 18010KT 6000 SHRA MIFG BKN035 OVC060CB
BECMG 1603/1606 18010KT 1000 MIFG BKN035
FM161200 6000 +SHRA MIFG BKN040 OVC060CB=
```

### To post the same Taf Json and get a Json version of the TAC with adverse weather highlighting hints:
​
`curl --header "Content-Type: application/json" --header "Accept: application/json" --request POST --data @./src/test/resources/taf-readGeoWebTafAndCreateTaf.json http://localhost:8080/gettaftac | jq '{}'`
​
Returns:
​
```
{
    "tac": [
        [
            {
                "text": "TAF"
            },
            {
                "text": "EHAM"
            },
            {
                "text": "161800Z"
            },
            {
                "text": "1618/1724"
            },
            {
                "text": "18010KT"
            },
            {
                "text": "6000"
            },
            {
                "text": "SHRA"
            },
            {
                "text": "MIFG",
                "type": "adverse"
            },
            {
                "text": "BKN035"
            },
            {
                "text": "OVC060CB"
            }
        ],
        [
            {
                "text": "BECMG"
            },
            {
                "text": "1621/1703"
            },
            {
                "text": "18010KT"
            },
            {
                "text": "1000",
                "type": "adverse"
            },
            {
                "text": "MIFG",
                "type": "adverse"
            },
            {
                "text": "BKN035"
            }
        ],
        [
            {
                "text": "TEMPO"
            },
            {
                "text": "1706/1708"
            },
            {
                "text": "20010KT"
            },
            {
                "text": "2000"
            },
            {
                "text": "FG",
                "type": "adverse"
            },
            {
                "text": "BKN030"
            }
        ],
        [
            {
                "text": "FM170900"
            },
            {
                "text": "6000"
            },
            {
                "text": "+SHRA",
                "type": "adverse"
            },
            {
                "text": "MIFG",
                "type": "adverse"
            },
            {
                "text": "BKN040"
            },
            {
                "text": "OVC060CB"
            },
            {
                "text": "="
            }
        ]
    ],
    "errors": []
}
```

## Examples of tests for IWXXM generation

### Testing IWXXM21 Sigmet

`curl --header "Content-Type: application/json" --request POST --data @./src/test/resources/geowebsigmets/published.json http://localhost:8080/getsigmetiwxxm21`

Returns a huge XML file

### Testing IWXXM21 Airmet

`curl --header "Content-Type: application/json" --request POST --data @./src/test/resources/geowebairmets/published.json http://localhost:8080/getairmetiwxxm21`

Returns a huge XML file

### Testing IWXXM30 Sigmet

`curl --header "Content-Type: application/json" --request POST --data @./src/test/resources/geowebsigmets/published.json http://localhost:8080/getsigmetiwxxm30`

Returns a huge XML file

### Testing IWXXM30 Airmet

`curl --header "Content-Type: application/json" --request POST --data @./src/test/resources/geowebairmets/published.json http://localhost:8080/getairmetiwxxm30`

Returns a huge XML file

### Testing IWXXM2023-1 Sigmet

`curl --header "Content-Type: application/json" --request POST --data @./src/test/resources/geowebsigmets/published.json http://localhost:8080/getsigmetiwxxm2023_1`

Returns a huge XML file

### Testing IWXXM2023-1 Airmet

`curl --header "Content-Type: application/json" --request POST --data @./src/test/resources/geowebairmets/published.json http://localhost:8080/getairmetiwxxm2023_1`

Returns a huge XML file

### Testing IWXXM21 TAF
`curl --header "Content-Type: application/json" --request POST --data @./src/test/resources/taf/geowebtaf/geowebtaf-with-changegroups.json  http://localhost:8080/gettafiwxxm21`

Returns a huge XML file

### Testing IWXXM30 TAF
`curl --header "Content-Type: application/json" --request POST --data @./src/test/resources/taf/geowebtaf/geowebtaf-with-changegroups.json  http://localhost:8080/gettafiwxxm30`

Returns a huge XML file

## Running with Docker

### To build the docker

`docker build -t geoweb-geoweb-knmi-avi-messageservices .`

### To run the docker:

`docker run -it -p 8080:8081 -e AVI_CONTAINER_PORT=8081 geoweb-geoweb-knmi-avi-messageservices`

- Note: The examples described above will work as well with the docker instance.
- Environment AVI_CONTAINER_PORT can be used to specify on which port the server is started within the container, defaults to 8080

### Install on EC2

```
sudo yum -y update
sudo yum -y install git docker
sudo usermod -a -G docker ec2-user
sudo service docker start
sudo chkconfig docker on
# Logout and login

# from your ws do:
rsync -av ./ ec2-user@54.194.160.48:/home/ec2-user/geoweb-geoweb-knmi-avi-messageservices/

# Go to geoweb-geoweb-knmi-avi-messageservices folder
docker build -t geoweb-geoweb-knmi-avi-messageservices .
docker run --log-driver json-file --log-opt max-size=10m --log-opt max-file=3 -d -p 80:8080 geoweb-geoweb-knmi-avi-messageservices
```

### Test:

`curl --header "Content-Type: application/json" --request POST --data @./src/test/resources/taf-readGeoWebTafAndCreateTaf.json http://54.194.160.48/gettaftac`
