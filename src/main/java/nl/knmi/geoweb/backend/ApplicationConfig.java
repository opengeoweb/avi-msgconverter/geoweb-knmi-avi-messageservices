package nl.knmi.geoweb.backend;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsonorg.JsonOrgModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.TimeZone;

import lombok.extern.slf4j.Slf4j;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.GeoWebSigmet;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.om.CreateSigmetObjectMapper;
import nl.knmi.geoweb.backend.aviation.FIRStore;
import nl.knmi.geoweb.backend.aviation.Tools;
import nl.knmi.geoweb.backend.converters.SigmetConverters.GeoWebSigmetToFMISigmet;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.om.CreateAirmetObjectMapper;
import nl.knmi.geoweb.backend.product.GeoWebTAF.om.CreateTAFObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import fi.fmi.avi.converter.AviMessageConverter;
import fi.fmi.avi.converter.AviMessageSpecificConverter;
import fi.fmi.avi.converter.ConversionHints;
import fi.fmi.avi.converter.ConversionResult;
import fi.fmi.avi.converter.ConversionSpecification;
import fi.fmi.avi.converter.json.TAFJSONParser;
import fi.fmi.avi.converter.json.conf.JSONConverter;
import fi.fmi.avi.converter.tac.conf.TACConverter;
import fi.fmi.avi.model.sigmet.AIRMET;
import fi.fmi.avi.model.sigmet.SIGMET;
import fi.fmi.avi.model.taf.TAF;
import fi.fmi.avi.model.taf.TAFBulletin;
import fi.fmi.avi.converter.iwxxm.conf.IWXXMConverter;

@Slf4j
@Configuration
@Order(-100)
@ComponentScan("fi.fmi.avi.converter.tac")
@ComponentScan("fi.fmi.avi.converter.iwxxm")
public class ApplicationConfig implements WebMvcConfigurer {

  private static final String DATEFORMAT_ISO8601 = "yyyy-MM-dd'T'HH:mm:ss'Z'";

  public ApplicationConfig() {
    log.trace("Constructor ApplicationConfig");
  }

  private void omBaseSettings(ObjectMapper om) {
    om.registerModule(new Jdk8Module());
    om.registerModule(new JavaTimeModule());
    om.registerModule(new JsonOrgModule());
    om.setDateFormat(new SimpleDateFormat(DATEFORMAT_ISO8601));
    om.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    SimpleModule simpleModule = new SimpleModule();
    simpleModule.addSerializer(
        OffsetDateTime.class,
        new JsonSerializer<OffsetDateTime>() {
          @Override
          public void serialize(
              OffsetDateTime offsetDateTime,
              JsonGenerator jsonGenerator,
              SerializerProvider serializerProvider)
              throws IOException {
            String formattedDate = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(
                offsetDateTime);
            jsonGenerator.writeString(formattedDate);
          }
        });
    om.registerModule(simpleModule);
    om.setTimeZone(TimeZone.getTimeZone("UTC"));
    om.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
    om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    om.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    om.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
  }

  @Autowired
  FIRStore firStore;

  @Override
  public void configureMessageConverters(
      List<HttpMessageConverter<?>> converters) {
    MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();
    ObjectMapper om = new ObjectMapper();
    omBaseSettings(om);
    jsonConverter.setObjectMapper(om);
    converters.add(jsonConverter);

    log.info("INIT -----------------------------------------------------------------");
    try {
      String sigmetJson = Tools.readResource("./geowebsigmets/published.json");
      ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
      GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
          sigmetJson,
          GeoWebSigmet.class);
      ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
      /* Now convert to IWXXM 3.0 and IWXXM 2023-1 */
      final ConversionSpecification<SIGMET, String> sigmetToIWXXM30 = IWXXMConverter.SIGMET_POJO_TO_IWXXM30_STRING;
      final ConversionSpecification<SIGMET, String> sigmetToIWXXM20231 = IWXXMConverter.SIGMET_POJO_TO_IWXXM2023_1_STRING;
      geowebAviMessageConverter()
          .convertMessage(fmiSigmet.getConvertedMessage().orElseThrow(), sigmetToIWXXM30,
              ConversionHints.SIGMET);
      geowebAviMessageConverter()
              .convertMessage(fmiSigmet.getConvertedMessage().orElseThrow(), sigmetToIWXXM20231,
                      ConversionHints.SIGMET);
    } catch (final Exception e) {
      throw new RuntimeException("Unable to initialize", e);
    }
    log.info("DONE -----------------------------------------------------------------");
  }

  @Bean("sigmetObjectMapper")
  public ObjectMapper getSigmetObjectMapperBean() {
    log.trace("sigmetObjectMapper");
    ObjectMapper om = new ObjectMapper();
    omBaseSettings(om);
    return om;
  }

  @Bean("airmetObjectMapper")
  public ObjectMapper getAirmetObjectMapperBean() {
    log.trace("airmetObjectMapper");
    ObjectMapper om = new ObjectMapper();
    omBaseSettings(om);
    return om;
  }

  @Bean("tafObjectMapper")
  public ObjectMapper getTafObjectMapperBean() {
    log.trace("tafObjectMapper");
    ObjectMapper om = new ObjectMapper();
    omBaseSettings(om);
    return om;
  }

  @Bean("geoWebObjectMapper")
  public ObjectMapper getGeoWebObjectMapperBean() {
    log.trace("geoWebObjectMapper");
    ObjectMapper om = new ObjectMapper();
    omBaseSettings(om);
    return om;
  }

  @Bean(name = "objectMapper")
  @Primary
  public ObjectMapper getObjectMapperBean() {
    log.trace("ObjectMapper");
    ObjectMapper om = new ObjectMapper();
    omBaseSettings(om);
    om.setTimeZone(TimeZone.getTimeZone("UTC"));
    return om;
  }

  @Bean("testProductObjectMapper")
  public ObjectMapper getTestProductObjectMapperBean() {
    log.trace("testProductObjectMapper");
    ObjectMapper om = new ObjectMapper();
    omBaseSettings(om);
    return om;
  }

  @Bean
  public Jackson2ObjectMapperBuilder objectMapperBuilder() {
    Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
    builder.featuresToEnable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY);

    return builder;
  }

  @Bean("geowebSigmetObjectMapper")
  public ObjectMapper getGeowebSigmetObjectMapperBean() {
    log.trace("geowebSigmetObjectMapper");
    return CreateSigmetObjectMapper.getOm();
  }

  @Bean("geowebAirmetObjectMapper")
  public ObjectMapper getGeowebAirmetObjectMapperBean() {
    log.trace("geowebAirmetObjectMapper");
    return CreateAirmetObjectMapper.getOm();
  }

  @Bean("geowebTafObjectMapper")
  public ObjectMapper getGeowebTafObjectMapperBean() {
    log.trace("geowebTafObjectMapper");
    return CreateTAFObjectMapper.getOm();
  }

  @Autowired
  @Qualifier("tafTACSerializer")
  private AviMessageSpecificConverter<TAF, String> geowebTafTACSerializer;

  @Autowired
  private AviMessageSpecificConverter<TAFBulletin, String> tafBulletinTACSerializer;

  @Bean(name = "geowebTafJSONParser")
  public AviMessageSpecificConverter<String, TAF> geowebTafJSONParser() {
    return new TAFJSONParser();
  }

  @Autowired
  @Qualifier("geowebTafJSONParser")
  private AviMessageSpecificConverter<String, TAF> geowebTafJSONParser;

  @Autowired
  @Qualifier("sigmetTACSerializer")
  private AviMessageSpecificConverter<SIGMET, String> fmiSigmetTACSerializer;

  @Autowired
  @Qualifier("airmetTACSerializer")
  private AviMessageSpecificConverter<AIRMET, String> fmiAirmetTACSerializer;

  @Autowired
  @Qualifier("sigmetIWXXMStringSerializer")
  private AviMessageSpecificConverter<SIGMET, String> sigmetIWXXMStringSerializer;

  @Autowired
  @Qualifier("airmetIWXXMStringSerializer")
  private AviMessageSpecificConverter<AIRMET, String> airmetIWXXMStringSerializer;

  @Autowired
  @Qualifier("sigmetIWXXM30StringSerializer")
  private AviMessageSpecificConverter<SIGMET, String> sigmetIWXXM30StringSerializer;

  @Autowired
  @Qualifier("airmetIWXXM30StringSerializer")
  private AviMessageSpecificConverter<AIRMET, String> airmetIWXXM30StringSerializer;

  @Autowired
  @Qualifier("sigmetIWXXM20231StringSerializer")
  private AviMessageSpecificConverter<SIGMET, String> sigmetIWXXM20231StringSerializer;

  @Autowired
  @Qualifier("airmetIWXXM20231StringSerializer")
  private AviMessageSpecificConverter<AIRMET, String> airmetIWXXM20231StringSerializer;

  @Autowired
  @Qualifier("tafIWXXMStringSerializer")
  private AviMessageSpecificConverter<TAF, String> tafIWXXMStringSerializer;

  @Autowired
  @Qualifier("tafIWXXM30StringSerializer")
  private AviMessageSpecificConverter<TAF, String> tafIWXXM30StringSerializer;

  @Bean("geowebAviMessageConverter")
  public AviMessageConverter geowebAviMessageConverter() {
    final AviMessageConverter p = new AviMessageConverter();
    p.setMessageSpecificConverter(TACConverter.TAF_POJO_TO_TAC, geowebTafTACSerializer);
    p.setMessageSpecificConverter(TACConverter.TAF_BULLETIN_POJO_TO_TAC, tafBulletinTACSerializer);
    p.setMessageSpecificConverter(JSONConverter.JSON_STRING_TO_TAF_POJO, geowebTafJSONParser);
    p.setMessageSpecificConverter(TACConverter.SIGMET_POJO_TO_TAC, fmiSigmetTACSerializer);
    p.setMessageSpecificConverter(TACConverter.AIRMET_POJO_TO_TAC, fmiAirmetTACSerializer);
    p.setMessageSpecificConverter(IWXXMConverter.SIGMET_POJO_TO_IWXXM21_STRING, sigmetIWXXMStringSerializer);
    p.setMessageSpecificConverter(IWXXMConverter.AIRMET_POJO_TO_IWXXM21_STRING, airmetIWXXMStringSerializer);
    p.setMessageSpecificConverter(IWXXMConverter.SIGMET_POJO_TO_IWXXM30_STRING, sigmetIWXXM30StringSerializer);
    p.setMessageSpecificConverter(IWXXMConverter.AIRMET_POJO_TO_IWXXM30_STRING, airmetIWXXM30StringSerializer);
    p.setMessageSpecificConverter(IWXXMConverter.SIGMET_POJO_TO_IWXXM2023_1_STRING, sigmetIWXXM20231StringSerializer);
    p.setMessageSpecificConverter(IWXXMConverter.AIRMET_POJO_TO_IWXXM2023_1_STRING, airmetIWXXM20231StringSerializer);
    p.setMessageSpecificConverter(IWXXMConverter.TAF_POJO_TO_IWXXM21_STRING, tafIWXXMStringSerializer);
    p.setMessageSpecificConverter(IWXXMConverter.TAF_POJO_TO_IWXXM30_STRING, tafIWXXM30StringSerializer);
    return p;
  }
}
