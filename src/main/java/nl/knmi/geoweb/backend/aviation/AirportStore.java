package nl.knmi.geoweb.backend.aviation;

import java.io.File;
import java.io.IOException;
import java.nio.file.NotDirectoryException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Setter
@Getter
@Component
public class AirportStore {
    @Autowired
    @Qualifier("geoWebObjectMapper")
    private ObjectMapper om;

    private String airportFile;
    private String directory;
    private Map<String, AirportInfo> airportInfos;

    public AirportStore()  {
        this.airportFile = "BREM_20160310.json";
    }

    public void initStore() throws IOException {
        this.airportInfos = new HashMap<String, AirportInfo>();
        final ObjectMapper om = new ObjectMapper();

        try {
            AirportJsonRecord[] airports = om.readValue(Tools.readResource(this.airportFile), AirportJsonRecord[].class);
            for (AirportJsonRecord airport : airports) {
                try {
                    AirportInfo airportInfo = new AirportInfo(airport.getIcao(), airport.getName(), Float.parseFloat(airport.getLat()),
                            Float.parseFloat(airport.getLon()), (airport.getHeight().length() > 0) ? Float.parseFloat(airport.getHeight()) : 0);
                    airportInfos.put(airport.getIcao(), airportInfo);
                } catch (NumberFormatException e) {
                    log.error("Error parsing airport record " + airport.getIcao());
                }
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        log.debug("Found " + airportInfos.size() + " records of airportinfo");
    }

    public AirportInfo lookup(String ICAO) {
        if (airportInfos == null) {
            try {
                initStore();
            } catch (IOException e) {
                log.error("ERROR: on AirportStore.lookup(" + ICAO + ")" + " " + e);
                return null;
            }
        }
        return airportInfos.get(ICAO);
    }
}
