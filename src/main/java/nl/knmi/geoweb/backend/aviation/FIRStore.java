package nl.knmi.geoweb.backend.aviation;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.geojson.Feature;
import org.geojson.FeatureCollection;
import org.geojson.GeoJsonObject;
import org.springframework.stereotype.Component;

import fi.fmi.avi.util.JtsTools;
import fi.fmi.avi.util.JtsToolsException;


@Slf4j
@Getter
@Setter
@Component
public class FIRStore implements Cloneable {

  private String worldFIRFile;
  private String delegatedFile;
  private String simplifiedFIRFile;
  private String directory;
  private Map<String, Feature> worldFIRInfos;
  private Map<String, Feature> simplifiedFIRInfos;
  private Map<String, List<Feature>> delegatedAirspaces;

  public FIRStore() {
    this.worldFIRFile = "world_firs.json";
    this.delegatedFile = "delegated.json";
    this.simplifiedFIRFile = "simplified_firs.json";
  }

  public void initStore() throws IOException {
    this.worldFIRInfos = new HashMap<String, Feature>();
    this.delegatedAirspaces = new HashMap<String, List<Feature>>();
    this.simplifiedFIRInfos = new HashMap<String, Feature>();

    final ObjectMapper om = new ObjectMapper();

    try {
      final GeoJsonObject FIRInfo = om.readValue(
        Tools.readResource(this.worldFIRFile),
        GeoJsonObject.class
      );
      final FeatureCollection fc = (FeatureCollection) FIRInfo;
      for (final Feature f : fc.getFeatures()) {
        final String FIRname = f.getProperty("FIRname");
        final String ICAOCode = f.getProperty("ICAOCODE");
        worldFIRInfos.put(FIRname, f);
        worldFIRInfos.put(ICAOCode, f);
      }
    } catch (final IOException e) {
      log.error(e.getMessage());
    }
    log.debug("Found " + worldFIRInfos.size() + " records of FIRinfo");

    try {
      final GeoJsonObject simplifiedFIRInfo = om.readValue(
        Tools.readResource(this.simplifiedFIRFile),
        GeoJsonObject.class
      );
      final FeatureCollection simplifiedFc = (FeatureCollection) simplifiedFIRInfo;
      for (final Feature f : simplifiedFc.getFeatures()) {
        final String FIRname = f.getProperty("FIRname");
        final String ICAOCode = f.getProperty("ICAOCODE");
        simplifiedFIRInfos.put(FIRname, f);
        simplifiedFIRInfos.put(ICAOCode, f);
      }
    } catch (final IOException e) {
      log.error(e.getMessage());
    }
    log.debug(
      "Found " + simplifiedFIRInfos.size() + " records of simplified FIRinfo"
    );

    try {
      final GeoJsonObject DelegatedInfo = om.readValue(
        Tools.readResource(this.delegatedFile),
        GeoJsonObject.class
      );
      final FeatureCollection fc = (FeatureCollection) DelegatedInfo;
      for (final Feature f : fc.getFeatures()) {
        final String FIRname = f.getProperty("FIRname");
        final String ICAOCode = f.getProperty("ICAONAME");
        if (!delegatedAirspaces.containsKey(FIRname)) {
          final List<Feature> delegated = new ArrayList<Feature>();
          delegatedAirspaces.put(FIRname, delegated);
          delegatedAirspaces.put(ICAOCode, delegated);
        }
        delegatedAirspaces.get(FIRname).add(f);
      }
    } catch (final IOException e) {
      log.error(e.getMessage());
    }
  }

  public static Feature cloneThroughSerialize(final Feature t) {
    try {
      final ByteArrayOutputStream bos = new ByteArrayOutputStream();
      serializeToOutputStream(t, bos);
      final byte[] bytes = bos.toByteArray();
      final ObjectInputStream ois = new ObjectInputStream(
        new ByteArrayInputStream(bytes)
      );
      return (Feature) ois.readObject();
    } catch (final Exception e) {
      return null;
    }
  }

  private static void serializeToOutputStream(
    final Serializable ser,
    final OutputStream os
  )
    throws IOException {
    ObjectOutputStream oos = null;
    try {
      oos = new ObjectOutputStream(os);
      oos.writeObject(ser);
      oos.flush();
    } finally {
      oos.close();
    }
  }

  public Feature lookup(final String name, final boolean addDelegated) {
    if (worldFIRInfos == null) {
      try {
        initStore();
      } catch (final IOException e) {
        return null;
      }
    }

    Feature feature = null;
    if (simplifiedFIRInfos.containsKey(name)) {
      feature = cloneThroughSerialize(simplifiedFIRInfos.get(name));
    } else if (worldFIRInfos.containsKey(name)) {
      feature = cloneThroughSerialize(worldFIRInfos.get(name));
    }

    if (addDelegated) {
      if (delegatedAirspaces.containsKey(name)) {
        for (final Feature f : delegatedAirspaces.get(name)) {
          // Merge f with feature
          try {
            feature = JtsTools.merge(feature, f);
        } catch (JtsToolsException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        }
      }
    }

    return feature;
  }

  public String getFirName(String designator) {
      Feature f = lookup(designator, false);
      if (f!=null) {
          String name =  f.getProperty("FIRname");
          if (name!=null) {
            String fixedName=name.replace("FIR/UIR","")
                .replace("FIR", "")
                .replace("UIR", "")
                .replace("ACC ", "").replace(" ACC", "").trim();
            return fixedName;
          }
      }
      return null;
  }

  public String getFirType(String designator) {
    Feature f = lookup(designator, false);
    if (f!=null) {
        String name =  f.getProperty("FIRname");
        if (name!=null) {
          if (name.startsWith("FIR/UIR ") || name.endsWith(" FIR/UIR")) {
              return "FIR/UIR";
          } else if (name.startsWith("FIR ") || name.endsWith(" FIR")) {
              return "FIR";
          } else if (name.startsWith("UIR ") || name.endsWith(" UIR")) {
            return "UIR";
          } else if (name.startsWith("ACC ") || name.endsWith(" ACC")) {
            return "ACC";
          }
          return null;
        }
    }
    return null;
}
}
