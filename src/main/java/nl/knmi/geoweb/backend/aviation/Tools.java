package nl.knmi.geoweb.backend.aviation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.springframework.core.io.ClassPathResource;

public class Tools {

  /**
   * Loads a resource from the current package, e.g. useful for adding test files like sigmet json and taf json
   * @param Name The name of the resource in the package
   * @return String representation fo the resource
   * @throws IOException
   * @throws Exception
   */

  public static String readResource(String name) throws IOException {
    StringBuilder result = new StringBuilder("");
    ClassPathResource resource = new ClassPathResource(name);

    InputStream inputStream = resource.getInputStream();

    BufferedReader bufferedReader = new BufferedReader(
      new InputStreamReader(inputStream)
    );
    String line;

    while ((line = bufferedReader.readLine()) != null) {
      if (result.length() > 0) {
        result.append("\n");
      }
      result.append(line);
    }
    inputStream.close();
    return result.toString();
  }

}
