package nl.knmi.geoweb.backend.converters.AirmetConverters;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import fi.fmi.avi.converter.ConversionIssue;
import fi.fmi.avi.converter.ConversionResult;
import fi.fmi.avi.model.Airspace.AirspaceType;
import fi.fmi.avi.model.AviationCodeListUser;
import fi.fmi.avi.model.AviationCodeListUser.AeronauticalAirmetWeatherPhenomenon;
import fi.fmi.avi.model.AviationCodeListUser.WeatherCausingVisibilityReduction;
import fi.fmi.avi.model.AviationWeatherMessage.ReportStatus;
import fi.fmi.avi.model.NumericMeasure;
import fi.fmi.avi.model.PartialOrCompleteTimeInstant;
import fi.fmi.avi.model.PartialOrCompleteTimePeriod;
import fi.fmi.avi.model.TacOrGeoGeometry;
import fi.fmi.avi.model.immutable.AirspaceImpl;
import fi.fmi.avi.model.immutable.NumericMeasureImpl;
import fi.fmi.avi.model.immutable.PhenomenonGeometryWithHeightImpl;
import fi.fmi.avi.model.immutable.UnitPropertyGroupImpl;
import fi.fmi.avi.model.sigmet.AIRMET;
import fi.fmi.avi.model.sigmet.SigmetAnalysisType;
import fi.fmi.avi.model.sigmet.SigmetIntensityChange;
import fi.fmi.avi.model.sigmet.immutable.AIRMETImpl;
import fi.fmi.avi.model.sigmet.immutable.AirmetCloudLevelsImpl;
import fi.fmi.avi.model.sigmet.immutable.AirmetReferenceImpl;
import fi.fmi.avi.model.sigmet.immutable.AirmetWindImpl;
import lombok.extern.slf4j.Slf4j;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.GeoWebAirmet;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.ObservationOrForecastEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.PhenomenaEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.StatusEnum;
import nl.knmi.geoweb.backend.product.sigmetairmet.SigmetAirmetUtils;


@Slf4j
public class GeoWebAirmetToFMIAirmet {

  private static AeronauticalAirmetWeatherPhenomenon getPhenomenon(PhenomenaEnum phen, GeoWebAirmet geowebAirmet, AIRMETImpl.Builder airmetBuilder){
    AeronauticalAirmetWeatherPhenomenon airmetPhenomenon=null;
    switch (phen) {
      case FRQ_CB:
        airmetPhenomenon = AeronauticalAirmetWeatherPhenomenon.FRQ_CB;
        break;
      case FRQ_TCU:
        airmetPhenomenon = AeronauticalAirmetWeatherPhenomenon.FRQ_TCU;
        break;
      case ISOL_CB:
        airmetPhenomenon = AeronauticalAirmetWeatherPhenomenon.ISOL_CB;
        break;
      case ISOL_TCU:
        airmetPhenomenon = AeronauticalAirmetWeatherPhenomenon.ISOL_TCU;
        break;
      case ISOL_TS:
        airmetPhenomenon = AeronauticalAirmetWeatherPhenomenon.ISOL_TS;
        break;
      case ISOL_TSGR:
        airmetPhenomenon = AeronauticalAirmetWeatherPhenomenon.ISOL_TSGR;
        break;
      case MOD_ICE:
        airmetPhenomenon = AeronauticalAirmetWeatherPhenomenon.MOD_ICE;
        break;
      case MOD_MTW:
        airmetPhenomenon = AeronauticalAirmetWeatherPhenomenon.MOD_MTW;
        break;
      case MOD_TURB:
        airmetPhenomenon = AeronauticalAirmetWeatherPhenomenon.MOD_TURB;
        break;
      case MT_OBSC:
        airmetPhenomenon = AeronauticalAirmetWeatherPhenomenon.MT_OBSC;
        break;
      case OCNL_CB:
        airmetPhenomenon = AeronauticalAirmetWeatherPhenomenon.OCNL_CB;
        break;
      case OCNL_TCU:
        airmetPhenomenon = AeronauticalAirmetWeatherPhenomenon.OCNL_TCU;
        break;
      case OCNL_TS:
        airmetPhenomenon = AeronauticalAirmetWeatherPhenomenon.OCNL_TS;
        break;
      case OCNL_TSGR:
        airmetPhenomenon = AeronauticalAirmetWeatherPhenomenon.OCNL_TSGR;
        break;
      case BKN_CLD:
      case OVC_CLD:
        if (phen==PhenomenaEnum.BKN_CLD) {
          airmetPhenomenon = AeronauticalAirmetWeatherPhenomenon.BKN_CLD;
        } else {
          airmetPhenomenon = AeronauticalAirmetWeatherPhenomenon.OVC_CLD;
        }
        if (geowebAirmet.getCloudLevelInfoMode()!=null) {
          AirmetCloudLevelsImpl.Builder airmetCloudLevelsBuilder = AirmetCloudLevelsImpl.builder();
          String unit = "FT"; //KNMI GeoWeb always reports cloud levels in feet, [ft_i] in IWXXM lingo, but in model it is FT
          switch (geowebAirmet.getCloudLevelInfoMode()) {
            case BETW:
            airmetCloudLevelsBuilder.setCloudBase(NumericMeasureImpl.of(geowebAirmet.getCloudLowerLevel().getValue(), unit));
            airmetCloudLevelsBuilder.setCloudTop(NumericMeasureImpl.of(geowebAirmet.getCloudLevel() .getValue(), unit));
            break;
            case BETW_ABV:
            airmetCloudLevelsBuilder.setCloudBase(NumericMeasureImpl.of(geowebAirmet.getCloudLowerLevel().getValue(), unit));
            airmetCloudLevelsBuilder.setCloudTop(NumericMeasureImpl.of(geowebAirmet.getCloudLevel() .getValue(), unit));
            airmetCloudLevelsBuilder.setTopAbove(true);
            break;
            case BETW_SFC:
            airmetCloudLevelsBuilder.setCloudBase(NumericMeasureImpl.of(0, unit));
            airmetCloudLevelsBuilder.setCloudTop(NumericMeasureImpl.of(geowebAirmet.getCloudLevel() .getValue(), unit));
            break;
            case BETW_SFC_ABV:
            airmetCloudLevelsBuilder.setCloudBase(NumericMeasureImpl.of(0, unit));
            airmetCloudLevelsBuilder.setCloudTop(NumericMeasureImpl.of(geowebAirmet.getCloudLevel() .getValue(), unit));
            airmetCloudLevelsBuilder.setTopAbove(true);
          }
          airmetBuilder.setCloudLevels(airmetCloudLevelsBuilder.build());
        }
        break;
      case SFC_VIS:
        airmetPhenomenon = AeronauticalAirmetWeatherPhenomenon.SFC_VIS;
        if (geowebAirmet.getVisibilityCause()!=null) {
          airmetBuilder.setVisibility(NumericMeasureImpl.of(geowebAirmet.getVisibilityValue(),"M"));
          WeatherCausingVisibilityReduction cause = WeatherCausingVisibilityReduction.valueOf(geowebAirmet.getVisibilityCause().name());
          airmetBuilder.setObscuration(Arrays.asList(cause));
        }
        break;
      case SFC_WIND:
        airmetPhenomenon = AeronauticalAirmetWeatherPhenomenon.SFC_WIND;
        AirmetWindImpl.Builder airmetWindBuilder = new AirmetWindImpl.Builder();
        airmetWindBuilder.setDirection(NumericMeasureImpl.of(geowebAirmet.getWindDirection(), "deg"));
        airmetWindBuilder.setSpeed(NumericMeasureImpl.of(geowebAirmet.getWindSpeed(), "KT"));
        airmetBuilder.setWind(airmetWindBuilder.build());
        break;
      default:
        break;
      }
    return airmetPhenomenon;
  }

  /**
  * Converts the geoweb Airmet object to the FMI Airmet object.
  * @param GeoWebAirmet geowebSigmet
  * @return FMI Airmet
  */

  public static ConversionResult<AIRMET> convert(GeoWebAirmet geowebAirmet) throws JsonMappingException, JsonProcessingException {
    log.info("GeoWebAirmetToFMIAirmet convert");
    ConversionResult<AIRMET> retval = new ConversionResult<>();
    AIRMETImpl.Builder airmetBuilder = AIRMETImpl.builder();
    airmetBuilder = airmetBuilder.setTranslationTime(ZonedDateTime.now(ZoneId.of("Z")));

    /* Set status */
    StatusEnum airmetStatus = geowebAirmet.getStatus();
    if (airmetStatus == null) {
      airmetStatus = StatusEnum.DRAFT;
    }
    airmetBuilder = airmetBuilder.setReportStatus(ReportStatus.NORMAL);
    switch(airmetStatus) {
      case DRAFT:
      case PUBLISHED:
      case EXPIRED:
      airmetBuilder = airmetBuilder.setCancelMessage(false);
      break;
      case CANCELLED:
      airmetBuilder = airmetBuilder.setCancelMessage(true);
      break;
      default:
      throw new IllegalArgumentException("Unknown AIRMET Status found");
    }

    /* Set type: NORMAL, TEST, EXERCISE*/
    switch (geowebAirmet.getType()) {
      case NORMAL:
      airmetBuilder = airmetBuilder.setPermissibleUsage(AviationCodeListUser.PermissibleUsage.OPERATIONAL);
      break;
      case EXERCISE:
      airmetBuilder = airmetBuilder.setPermissibleUsage(AviationCodeListUser.PermissibleUsage.NON_OPERATIONAL);
      airmetBuilder = airmetBuilder.setPermissibleUsageReason(AviationCodeListUser.PermissibleUsageReason.EXERCISE);
      break;
      case TEST:
      default:
      airmetBuilder = airmetBuilder.setPermissibleUsage(AviationCodeListUser.PermissibleUsage.NON_OPERATIONAL);
      airmetBuilder = airmetBuilder.setPermissibleUsageReason(AviationCodeListUser.PermissibleUsageReason.TEST);
    }

    /* Set issue date */
    if (geowebAirmet.getIssueDate() != null) {
      airmetBuilder = airmetBuilder.setIssueTime(PartialOrCompleteTimeInstant.of(ZonedDateTime.from(geowebAirmet.getIssueDate())));
    }

    /* Set sequence */
    airmetBuilder.setSequenceNumber(geowebAirmet.getSequence());

    /* Set validity period */
    PartialOrCompleteTimePeriod.Builder validityTimeBuilder = PartialOrCompleteTimePeriod.builder();
    validityTimeBuilder = validityTimeBuilder.setStartTime(PartialOrCompleteTimeInstant.of(ZonedDateTime.from(geowebAirmet.getValidDateStart())));
    validityTimeBuilder = validityTimeBuilder.setEndTime(PartialOrCompleteTimeInstant.of(ZonedDateTime.from(geowebAirmet.getValidDateEnd())));
    airmetBuilder = airmetBuilder.setValidityPeriod(validityTimeBuilder);

    /* Set AirTrafficServicesUnit */
    UnitPropertyGroupImpl.Builder airTrafficServiceUnit = new UnitPropertyGroupImpl.Builder();
    airTrafficServiceUnit.setDesignator(geowebAirmet.getLocationIndicatorATSU());
    airTrafficServiceUnit.setName(geowebAirmet.getLocationIndicatorATSU());
    airTrafficServiceUnit.setType("FIC");
    airmetBuilder.setIssuingAirTrafficServicesUnit(airTrafficServiceUnit.build());

    /* Set Meteorological Watch Office */
    UnitPropertyGroupImpl.Builder meteorologicalWatchOffice = new UnitPropertyGroupImpl.Builder();
    meteorologicalWatchOffice.setDesignator(geowebAirmet.getLocationIndicatorMWO());
    meteorologicalWatchOffice.setName(geowebAirmet.getLocationIndicatorMWO()+" MWO");
    meteorologicalWatchOffice.setType("MWO");
    airmetBuilder.setMeteorologicalWatchOffice(meteorologicalWatchOffice.build());

    /* Set airspace */
    AirspaceImpl.Builder airspaceBuilder = new AirspaceImpl.Builder();
    airspaceBuilder.setDesignator(geowebAirmet.getLocationIndicatorATSR());
    airspaceBuilder.setName(geowebAirmet.getFirName());
    /* TODO: Make more general than FIR */
    airspaceBuilder.setType(AirspaceType.FIR);
    airmetBuilder.setAirspace(airspaceBuilder.build());

    /* Set phenomenon if available*/
    //TODO move down once fmi-avi-messageconverter-iwxxm no longer requires phenomenon for cancels
    if (geowebAirmet.getPhenomenon() != null) {
      airmetBuilder.setPhenomenon(getPhenomenon(geowebAirmet.getPhenomenon(), geowebAirmet, airmetBuilder));
    } else {
      retval.addIssue(new ConversionIssue(ConversionIssue.Type.MISSING_DATA, "phenomenon is missing or incorrect"));
      return retval;
    }

    /* Set reference AIRMET (describing the AIRMET to be cancelled) */
    if (geowebAirmet.getCancelsAirmetSequenceId() != null) {
      airmetBuilder = airmetBuilder.setCancelMessage(true);
      OffsetDateTime previousValidityStart = geowebAirmet.getValidDateStartOfAirmetToCancel();
      OffsetDateTime previousValidityEnd = geowebAirmet.getValidDateEndOfAirmetToCancel();
      if (previousValidityStart == null || previousValidityEnd == null) {
        retval.addIssue(new ConversionIssue(ConversionIssue.Type.MISSING_DATA, "previousValidityStart or previousValidityEnd is missing"));
        return retval;
      }

      /* AirmetReferenceImpl does not have .builder(), have to use new Builder() instead */
      AirmetReferenceImpl.Builder airmetReferenceBuilder = new AirmetReferenceImpl.Builder();

      /* Required: issuingAirTrafficServicesUnit, meteorologicalWatchOffice, sequenceNumber */
      airmetReferenceBuilder.setIssuingAirTrafficServicesUnit(airTrafficServiceUnit.build());
      airmetReferenceBuilder.setMeteorologicalWatchOffice(meteorologicalWatchOffice.build());
      airmetReferenceBuilder.setSequenceNumber(geowebAirmet.getCancelsAirmetSequenceId());

      PartialOrCompleteTimePeriod.Builder referredValidityTimeBuilder = PartialOrCompleteTimePeriod.builder()
      .setStartTime(PartialOrCompleteTimeInstant.of(ZonedDateTime.from(previousValidityStart)))
      .setEndTime(PartialOrCompleteTimeInstant.of(ZonedDateTime.from(previousValidityEnd)));

      airmetReferenceBuilder = airmetReferenceBuilder.setValidityPeriod(referredValidityTimeBuilder.build());
      airmetBuilder.setCancelledReference(airmetReferenceBuilder.build());

      /* Return this Cancel Sigmet */
      retval.setConvertedMessage(airmetBuilder.build());
      return retval;
    } else {
      airmetBuilder = airmetBuilder.setCancelMessage(false);
    }

    //TODO Move setPhenomenon code here from above when fmi-avi-messageconverter no longer requires airmetPhenomenon

    /* Set analysis geometry */
    PhenomenonGeometryWithHeightImpl.Builder geometry = new PhenomenonGeometryWithHeightImpl.Builder();

    /* Set Analysis type: whether it is an observation or a forecast */
    log.info("analysis:"+geowebAirmet.getIsObservationOrForecast());
    if (geowebAirmet.getIsObservationOrForecast() == ObservationOrForecastEnum.OBS) {
      geometry.setAnalysisType(SigmetAnalysisType.OBSERVATION);
    } else if (geowebAirmet.getIsObservationOrForecast() == ObservationOrForecastEnum.FCST) {
      geometry.setAnalysisType(SigmetAnalysisType.FORECAST);
    }

    log.info("analysistime:"+geowebAirmet.getObservationOrForecastTime());
    if (geowebAirmet.getObservationOrForecastTime() != null) {
      geometry.setTime(PartialOrCompleteTimeInstant.of(ZonedDateTime.from(geowebAirmet.getObservationOrForecastTime())));
    }

    /* Prepare geometry conversion */
    if (!((geowebAirmet.getStartGeometry()!=null)&&(geowebAirmet.getStartGeometryIntersect()!=null)) &&
      (geowebAirmet.getFirGeometry()!=null)) {
        retval.addIssue(new ConversionIssue(ConversionIssue.Type.MISSING_DATA, "AIRMET start area missing"));
        return retval;
    }

    /* Check that the geometry is (partially) contained, otherwise fail */
    if (geowebAirmet.getStartGeometry()!=null) {
      if (!SigmetAirmetUtils.isPartiallyContained(geowebAirmet.getStartGeometry(),geowebAirmet.getFirGeometry())) {
        retval.addIssue(new ConversionIssue(ConversionIssue.Type.LOGICAL, "The start position needs to be (partly) inside the FIR"));
        return retval;
      }
    }

    TacOrGeoGeometry tacOrGeoGeometry = SigmetAirmetUtils.handleGeometries(
        geowebAirmet.getStartGeometry(),
        geowebAirmet.getStartGeometryIntersect(),
        geowebAirmet.getFirGeometry());

    geometry.setGeometry(tacOrGeoGeometry);
    geometry.setApproximateLocation(false);

    /* Set levels */
    if (geowebAirmet.getLevelInfoMode() != null) {
      switch (geowebAirmet.getLevelInfoMode()) {
        case AT:
          NumericMeasure nmLower = NumericMeasureImpl.of((double) geowebAirmet.getLevel().getValue(),
                  geowebAirmet.getLevel().getUnit().toString());
          geometry.setLowerLimit(nmLower);
          break;
        case ABV:
          nmLower = NumericMeasureImpl.of((double) geowebAirmet.getLevel().getValue(),
                  geowebAirmet.getLevel().getUnit().toString());
          geometry.setLowerLimit(nmLower);
          geometry.setLowerLimitOperator(AviationCodeListUser.RelationalOperator.ABOVE);
          break;
        case BETW:
          nmLower = NumericMeasureImpl.of((double) geowebAirmet.getLowerLevel().getValue(),
                  geowebAirmet.getLowerLevel().getUnit().toString());
          geometry.setLowerLimit(nmLower);
          NumericMeasure nmUpper = NumericMeasureImpl.of((double) geowebAirmet.getLevel().getValue(),
                  geowebAirmet.getLevel().getUnit().toString());
          geometry.setUpperLimit(nmUpper);
          break;
        case BETW_SFC:
          nmLower = NumericMeasureImpl.of(0.0, "FT"); //Special case for SFC: 0FT
          geometry.setLowerLimit(nmLower);
          nmUpper =  NumericMeasureImpl.of((double) geowebAirmet.getLevel().getValue(),
                  geowebAirmet.getLevel().getUnit().toString());
          geometry.setUpperLimit(nmUpper);
          break;
        case TOPS_BLW:
          nmLower = NumericMeasureImpl.of((double) geowebAirmet.getLowerLevel().getValue(),
                  geowebAirmet.getLowerLevel().getUnit().toString());
          geometry.setLowerLimit(nmLower);
          geometry.setLowerLimitOperator(AviationCodeListUser.RelationalOperator.BELOW);
          break;
        case TOPS:
          nmUpper = NumericMeasureImpl.of((double) geowebAirmet.getLevel().getValue(),
                  geowebAirmet.getLevel().getUnit().toString());
          geometry.setUpperLimit(nmUpper);
          break;
        case TOPS_ABV:
          nmUpper = NumericMeasureImpl.of((double) geowebAirmet.getLevel().getValue(),
                  geowebAirmet.getLevel().getUnit().toString());
          geometry.setUpperLimit(nmUpper);
          geometry.setUpperLimitOperator(AviationCodeListUser.RelationalOperator.ABOVE);
          break;
        default:
          break;
      }
    }

    /* Set change */
    if (geowebAirmet.getChange() != null) {
      switch (geowebAirmet.getChange()) {
        case NC:
        geometry.setIntensityChange(SigmetIntensityChange.NO_CHANGE);
        break;
        case INTSF:
        geometry.setIntensityChange(SigmetIntensityChange.INTENSIFYING);
        break;
        case WKN:
        geometry.setIntensityChange(SigmetIntensityChange.WEAKENING);
        break;
      }
    }
    /* Set MovementType */
    if (geowebAirmet.getMovementType() != null) {
      switch (geowebAirmet.getMovementType()) {
        case STATIONARY:
        geometry.setMovingDirection(Optional.empty());
        geometry.setMovingSpeed(Optional.empty());
        break;
        case MOVEMENT:
        geometry.setMovingSpeed(NumericMeasureImpl.of(geowebAirmet.getMovementSpeed(), geowebAirmet.getMovementUnit().name()));
        /* Convert enum to degrees by multiplying */
        geometry.setMovingDirection(NumericMeasureImpl.of(geowebAirmet.getMovementDirection().ordinal()*22.5, "deg"));
        break;
      }
    } else {
      geometry.setMovingDirection(Optional.empty());
      geometry.setMovingSpeed(Optional.empty());
    }

    /* Set analysis geometry */
    airmetBuilder.setAnalysisGeometries(Arrays.asList(geometry.build()));

    retval.setConvertedMessage(airmetBuilder.build());
    return retval;
  }
}
