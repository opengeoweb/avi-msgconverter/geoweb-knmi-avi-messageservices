package nl.knmi.geoweb.backend.converters.SigmetConverters;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import fi.fmi.avi.converter.ConversionIssue;
import fi.fmi.avi.converter.ConversionResult;
import fi.fmi.avi.model.Airspace.AirspaceType;
import fi.fmi.avi.model.AviationCodeListUser;
import fi.fmi.avi.model.AviationCodeListUser.AeronauticalSignificantWeatherPhenomenon;
import fi.fmi.avi.model.AviationWeatherMessage.ReportStatus;
import fi.fmi.avi.model.NumericMeasure;
import fi.fmi.avi.model.PartialOrCompleteTimeInstant;
import fi.fmi.avi.model.PartialOrCompleteTimePeriod;
import fi.fmi.avi.model.TacOrGeoGeometry;
import fi.fmi.avi.model.immutable.AirspaceImpl;
import fi.fmi.avi.model.immutable.ElevatedPointImpl;
import fi.fmi.avi.model.immutable.NumericMeasureImpl;
import fi.fmi.avi.model.immutable.PhenomenonGeometryImpl;
import fi.fmi.avi.model.immutable.PhenomenonGeometryWithHeightImpl;
import fi.fmi.avi.model.immutable.UnitPropertyGroupImpl;
import fi.fmi.avi.model.immutable.VolcanoDescriptionImpl;
import fi.fmi.avi.model.sigmet.SIGMET;
import fi.fmi.avi.model.sigmet.SigmetAnalysisType;
import fi.fmi.avi.model.sigmet.SigmetIntensityChange;
import fi.fmi.avi.model.sigmet.immutable.SIGMETImpl;
import fi.fmi.avi.model.sigmet.immutable.SigmetReferenceImpl;
import fi.fmi.avi.model.sigmet.immutable.VAInfoImpl;
import lombok.extern.slf4j.Slf4j;
import nl.knmi.geoweb.backend.aviation.FIRStore;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.GeoWebSigmet;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.enums.*;
import nl.knmi.geoweb.backend.product.sigmetairmet.SigmetAirmetUtils;

@Slf4j
public class GeoWebSigmetToFMISigmet {

  private static AeronauticalSignificantWeatherPhenomenon getPhenomenon(PhenomenaEnum phen) {
    AeronauticalSignificantWeatherPhenomenon sigmetPhenomenon;
    switch (phen) {
      case EMBD_TS:
      sigmetPhenomenon = AeronauticalSignificantWeatherPhenomenon.EMBD_TS;
      break;
      case EMBD_TSGR:
      sigmetPhenomenon = AeronauticalSignificantWeatherPhenomenon.EMBD_TSGR;
      break;
      case FRQ_TS:
      sigmetPhenomenon = AeronauticalSignificantWeatherPhenomenon.FRQ_TS;
      break;
      case FRQ_TSGR:
      sigmetPhenomenon = AeronauticalSignificantWeatherPhenomenon.FRQ_TSGR;
      break;
      case HVY_DS:
      sigmetPhenomenon = AeronauticalSignificantWeatherPhenomenon.HVY_DS;
      break;
      case HVY_SS:
      sigmetPhenomenon = AeronauticalSignificantWeatherPhenomenon.HVY_SS;
      break;
      case OBSC_TS:
      sigmetPhenomenon = AeronauticalSignificantWeatherPhenomenon.OBSC_TS;
      break;
      case OBSC_TSGR:
      sigmetPhenomenon = AeronauticalSignificantWeatherPhenomenon.OBSC_TSGR;
      break;
      case RDOACT_CLD:
      sigmetPhenomenon = AeronauticalSignificantWeatherPhenomenon.RDOACT_CLD;
      break;
      case SEV_ICE:
      sigmetPhenomenon = AeronauticalSignificantWeatherPhenomenon.SEV_ICE;
      break;
      case SEV_ICE_FRZ_RN:
      sigmetPhenomenon = AeronauticalSignificantWeatherPhenomenon.SEV_ICE_FZRA;
      break;
      case SEV_MTW:
      sigmetPhenomenon = AeronauticalSignificantWeatherPhenomenon.SEV_MTW;
      break;
      case SEV_TURB:
      sigmetPhenomenon = AeronauticalSignificantWeatherPhenomenon.SEV_TURB;
      break;
      case SQL_TS:
      sigmetPhenomenon = AeronauticalSignificantWeatherPhenomenon.SQL_TS;
      break;
      case SQL_TSGR:
      sigmetPhenomenon = AeronauticalSignificantWeatherPhenomenon.SQL_TSGR;
      break;
      case TC:
      sigmetPhenomenon = AeronauticalSignificantWeatherPhenomenon.TC;
      break;
      case VA_CLD:
      sigmetPhenomenon = AeronauticalSignificantWeatherPhenomenon.VA;
      break;
      default:
      sigmetPhenomenon = null;
    }
    return sigmetPhenomenon;
  }

    /**
  * Converts the geoweb Sigmet object to the FMI Sigmet object.
  * @param GeoWebSigmet geowebSigmet
    @param FIRStore firStore
  * @return FMI Sigmet
  */
  public static ConversionResult<SIGMET> convert(GeoWebSigmet geowebSigmet, FIRStore firStore) throws JsonMappingException, JsonProcessingException {
    log.info("GeoWebSigmetToFMISigmet convert");
    ConversionResult<SIGMET> retval = new ConversionResult<>();
    SIGMETImpl.Builder sigmetBuilder = SIGMETImpl.builder();
    sigmetBuilder = sigmetBuilder.setTranslationTime(ZonedDateTime.now(ZoneId.of("Z")));

    /* Set status */
    StatusEnum sigmetStatus = geowebSigmet.getStatus();
    if (sigmetStatus == null) {
      sigmetStatus = StatusEnum.DRAFT;
    }
    sigmetBuilder = sigmetBuilder.setReportStatus(ReportStatus.NORMAL);
    switch(sigmetStatus) {
      case DRAFT:
      case PUBLISHED:
      case EXPIRED:
      // sigmetBuilder = sigmetBuilder.setCancelMessage(false);
      break;
      case CANCELLED:
      // sigmetBuilder = sigmetBuilder.setCancelMessage(true);
      break;
      default:
      throw new IllegalArgumentException("Unknown SIGMET Status found");
    }

    /* Set type: NORMAL, TEST, EXERCISE*/
    switch (geowebSigmet.getType()) {
      case NORMAL:
        sigmetBuilder = sigmetBuilder.setPermissibleUsage(AviationCodeListUser.PermissibleUsage.OPERATIONAL);
        break;
      case EXERCISE:
        sigmetBuilder = sigmetBuilder.setPermissibleUsage(AviationCodeListUser.PermissibleUsage.NON_OPERATIONAL);
        sigmetBuilder = sigmetBuilder.setPermissibleUsageReason(AviationCodeListUser.PermissibleUsageReason.EXERCISE);
        break;
      case TEST:
      case SHORT_TEST:
      case SHORT_VA_TEST:
      default:
        sigmetBuilder = sigmetBuilder.setPermissibleUsage(AviationCodeListUser.PermissibleUsage.NON_OPERATIONAL);
        sigmetBuilder = sigmetBuilder.setPermissibleUsageReason(AviationCodeListUser.PermissibleUsageReason.TEST);
    }

    /* Set issue date */
    if (geowebSigmet.getIssueDate() != null) {
      sigmetBuilder = sigmetBuilder.setIssueTime(PartialOrCompleteTimeInstant.of(ZonedDateTime.from(geowebSigmet.getIssueDate())));
    }

    /* Set sequence */
    sigmetBuilder.setSequenceNumber(geowebSigmet.getSequence());

    /* Set validity period */
    PartialOrCompleteTimePeriod.Builder validityTimeBuilder = PartialOrCompleteTimePeriod.builder();
    validityTimeBuilder = validityTimeBuilder.setStartTime(PartialOrCompleteTimeInstant.of(ZonedDateTime.from(geowebSigmet.getValidDateStart())));
    validityTimeBuilder = validityTimeBuilder.setEndTime(PartialOrCompleteTimeInstant.of(ZonedDateTime.from(geowebSigmet.getValidDateEnd())));
    sigmetBuilder.setValidityPeriod(validityTimeBuilder);


    /* Set AirTrafficServicesUnit */
    UnitPropertyGroupImpl.Builder airTrafficServiceUnit = new UnitPropertyGroupImpl.Builder();
    airTrafficServiceUnit.setDesignator(geowebSigmet.getLocationIndicatorATSU());
    airTrafficServiceUnit.setName(geowebSigmet.getLocationIndicatorATSU());
    airTrafficServiceUnit.setType("FIC");
    sigmetBuilder.setIssuingAirTrafficServicesUnit(airTrafficServiceUnit.build());

    /* Set Meteorological Watch Office */
    UnitPropertyGroupImpl.Builder meteorologicalWatchOffice = new UnitPropertyGroupImpl.Builder();
    meteorologicalWatchOffice.setDesignator(geowebSigmet.getLocationIndicatorMWO());
    meteorologicalWatchOffice.setType("MWO");
    meteorologicalWatchOffice.setName(geowebSigmet.getLocationIndicatorMWO()+ " MWO");
    sigmetBuilder.setMeteorologicalWatchOffice(meteorologicalWatchOffice.build());

    /* Set airspace */
    AirspaceImpl.Builder airspaceBuilder = new AirspaceImpl.Builder();
    airspaceBuilder.setDesignator(geowebSigmet.getLocationIndicatorATSR());
    airspaceBuilder.setName(geowebSigmet.getFirName());
    /* TODO: Make more general than FIR */
    airspaceBuilder.setType(AirspaceType.FIR);
    sigmetBuilder.setAirspace(airspaceBuilder.build());


    final boolean isShortTestSigmet = isShortTestSigmet(geowebSigmet);

    /* Set phenomenon and its type */
    if (geowebSigmet.getPhenomenon() != null) {
      switch (geowebSigmet.getPhenomenon()) {
        case VA_CLD:
          sigmetBuilder.setPhenomenonType(AviationCodeListUser.SigmetPhenomenonType.VOLCANIC_ASH_SIGMET);
          break;
        case TC:
          sigmetBuilder.setPhenomenonType(AviationCodeListUser.SigmetPhenomenonType.TROPICAL_CYCLONE_SIGMET);
          break;
        default:
          sigmetBuilder.setPhenomenonType(AviationCodeListUser.SigmetPhenomenonType.SIGMET);
      }
      if (!isShortTestSigmet) {
        // Phenomenon is not set for short test sigmets
        sigmetBuilder.setPhenomenon(getPhenomenon(geowebSigmet.getPhenomenon()));
      }
    } else {
      retval.addIssue(new ConversionIssue(ConversionIssue.Type.MISSING_DATA, "phenomenon is missing or incorrect"));
      return retval;
    }

    // This is everything a short (va) test sigmet has, short circuit the conversion
    if (isShortTestSigmet) {
      retval.setConvertedMessage(sigmetBuilder.build());
      return retval;
    }

    /* Set reference SIGMET (describing the SIGMET to be cancelled) */
    if (geowebSigmet.getCancelsSigmetSequenceId() != null) {
      sigmetBuilder = sigmetBuilder.setCancelMessage(true);
      OffsetDateTime previousValidityStart = geowebSigmet.getValidDateStartOfSigmetToCancel();
      OffsetDateTime previousValidityEnd = geowebSigmet.getValidDateEndOfSigmetToCancel();
      if (previousValidityStart == null || previousValidityEnd == null) {
        retval.addIssue(new ConversionIssue(ConversionIssue.Type.MISSING_DATA, "previousValidityStart or previousValidityEnd is missing"));
        return retval;
      }

      /* SigmetReferenceImpl does not have .builder(), have to use new Builder() instead */
      SigmetReferenceImpl.Builder sigmetReferenceBuilder = new SigmetReferenceImpl.Builder();

      /* Required: issuingAirTrafficServicesUnit, meteorologicalWatchOffice, sequenceNumber */
      sigmetReferenceBuilder.setIssuingAirTrafficServicesUnit(airTrafficServiceUnit.build());
      sigmetReferenceBuilder.setMeteorologicalWatchOffice(meteorologicalWatchOffice.build());
      sigmetReferenceBuilder.setSequenceNumber(geowebSigmet.getCancelsSigmetSequenceId());

      PartialOrCompleteTimePeriod.Builder referredValidityTimeBuilder = PartialOrCompleteTimePeriod.builder()
      .setStartTime(PartialOrCompleteTimeInstant.of(ZonedDateTime.from(previousValidityStart)))
      .setEndTime(PartialOrCompleteTimeInstant.of(ZonedDateTime.from(previousValidityEnd)));

      sigmetReferenceBuilder = sigmetReferenceBuilder.setValidityPeriod(referredValidityTimeBuilder.build());
      sigmetBuilder.setCancelledReference(sigmetReferenceBuilder.build());

      /* Add VA MOV TO info if needed */
      if (geowebSigmet.getVaSigmetMoveToFIR() != null) {
        String movedToFirDesignator = geowebSigmet.getVaSigmetMoveToFIR();
        String movedToFirName = firStore.getFirName(geowebSigmet.getVaSigmetMoveToFIR());
        if (movedToFirName!=null) {
          VAInfoImpl.Builder volcanicAshInfo = new VAInfoImpl.Builder();
          UnitPropertyGroupImpl.Builder issuingAirTrafficServicesUnit = new UnitPropertyGroupImpl.Builder();
          issuingAirTrafficServicesUnit.setName(movedToFirName);
          issuingAirTrafficServicesUnit.setType(firStore.getFirType(movedToFirDesignator));
          issuingAirTrafficServicesUnit.setDesignator(movedToFirDesignator);
          volcanicAshInfo.setVolcanicAshMovedToFIR(issuingAirTrafficServicesUnit.build());
          sigmetBuilder.setVAInfo(volcanicAshInfo.build());
        } else {
          retval.addIssue(new ConversionIssue(ConversionIssue.Type.MISSING_DATA, "movedToFir to unknown FIR:"+movedToFirName));
          return retval;
        }
      }
      /* Return this Cancel Sigmet */
      retval.setConvertedMessage(sigmetBuilder.build());
      return retval;
    } else {
      sigmetBuilder = sigmetBuilder.setCancelMessage(false);
    }

    /* Set analysis geometry */
    PhenomenonGeometryWithHeightImpl.Builder geometry = new PhenomenonGeometryWithHeightImpl.Builder();

    /* Set Analysis type: whether it is an observation or a forecast */
    log.info("analysis:"+geowebSigmet.getIsObservationOrForecast());
    if (geowebSigmet.getIsObservationOrForecast() == ObservationOrForecastEnum.OBS) {
      geometry.setAnalysisType(SigmetAnalysisType.OBSERVATION);
    } else if (geowebSigmet.getIsObservationOrForecast() == ObservationOrForecastEnum.FCST){
      geometry.setAnalysisType(SigmetAnalysisType.FORECAST);
    }

    log.info("analysistime:"+geowebSigmet.getObservationOrForecastTime());
    if (geowebSigmet.getObservationOrForecastTime() != null) {
      geometry.setTime(PartialOrCompleteTimeInstant.of(ZonedDateTime.from(geowebSigmet.getObservationOrForecastTime())));
    }

    if (!((geowebSigmet.getStartGeometry()!=null)&&(geowebSigmet.getStartGeometryIntersect()!=null)) &&
        (geowebSigmet.getFirGeometry()!=null)) {
        retval.addIssue(new ConversionIssue(ConversionIssue.Type.MISSING_DATA, "SIGMET start area missing"));
        return retval;
    }

    /* Check that the geometry is (partially) contained, otherwise fail */
    if (geowebSigmet.getStartGeometry()!=null) {
      if (!SigmetAirmetUtils.isPartiallyContained(geowebSigmet.getStartGeometry(),geowebSigmet.getFirGeometry())) {
        retval.addIssue(new ConversionIssue(ConversionIssue.Type.LOGICAL, "The start position needs to be (partly) inside the FIR"));
        return retval;
      }
    }

    /* Prepare geometry conversion */
    TacOrGeoGeometry tacOrGeoGeometry = SigmetAirmetUtils.handleGeometries(geowebSigmet.getStartGeometry(),
            geowebSigmet.getStartGeometryIntersect(),
            geowebSigmet.getFirGeometry());
    geometry.setGeometry(tacOrGeoGeometry);

    geometry.setApproximateLocation(false);

    /* Set levels */
    if (geowebSigmet.getLevelInfoMode() != null) {
      switch (geowebSigmet.getLevelInfoMode()) {
        case AT:
          NumericMeasure nmLower = NumericMeasureImpl.of((double) geowebSigmet.getLevel().getValue(),
                  geowebSigmet.getLevel().getUnit().toString());
          geometry.setLowerLimit(nmLower);
          break;
        case ABV:
          nmLower = NumericMeasureImpl.of((double) geowebSigmet.getLevel().getValue(),
                  geowebSigmet.getLevel().getUnit().toString());
          geometry.setLowerLimit(nmLower);
          geometry.setLowerLimitOperator(AviationCodeListUser.RelationalOperator.ABOVE);
          break;
        case BETW:
          nmLower = NumericMeasureImpl.of((double) geowebSigmet.getLowerLevel().getValue(),
                  geowebSigmet.getLowerLevel().getUnit().toString());
          geometry.setLowerLimit(nmLower);
          NumericMeasure nmUpper = NumericMeasureImpl.of((double) geowebSigmet.getLevel().getValue(),
                  geowebSigmet.getLevel().getUnit().toString());
          geometry.setUpperLimit(nmUpper);
          break;
        case BETW_SFC:
          nmLower = NumericMeasureImpl.of(0.0, "FT"); //Special case for SFC: 0FT
          geometry.setLowerLimit(nmLower);
          nmUpper =  NumericMeasureImpl.of((double) geowebSigmet.getLevel().getValue(),
                  geowebSigmet.getLevel().getUnit().toString());
          geometry.setUpperLimit(nmUpper);
          break;
        case TOPS_BLW:
          nmLower = NumericMeasureImpl.of((double) geowebSigmet.getLowerLevel().getValue(),
                  geowebSigmet.getLowerLevel().getUnit().toString());
          geometry.setLowerLimit(nmLower);
          geometry.setLowerLimitOperator(AviationCodeListUser.RelationalOperator.BELOW);
          break;
        case TOPS:
          nmUpper = NumericMeasureImpl.of((double) geowebSigmet.getLevel().getValue(),
                  geowebSigmet.getLevel().getUnit().toString());
          geometry.setUpperLimit(nmUpper);
          break;
        case TOPS_ABV:
          nmUpper = NumericMeasureImpl.of((double) geowebSigmet.getLevel().getValue(),
                  geowebSigmet.getLevel().getUnit().toString());
          geometry.setUpperLimit(nmUpper);
          geometry.setUpperLimitOperator(AviationCodeListUser.RelationalOperator.ABOVE);
          break;
        default:
          break;
      }
    }

    /* Set change */
    if (geowebSigmet.getChange() != null) {
      switch (geowebSigmet.getChange()) {
      case NC:
        geometry.setIntensityChange(SigmetIntensityChange.NO_CHANGE);
        break;
      case INTSF:
        geometry.setIntensityChange(SigmetIntensityChange.INTENSIFYING);
        break;
      case WKN:
        geometry.setIntensityChange(SigmetIntensityChange.WEAKENING);
        break;
      }
    }

    /* Set MovementType */
    if (geowebSigmet.getMovementType() != null) {
      switch (geowebSigmet.getMovementType()) {
      case STATIONARY:
        geometry.setMovingDirection(Optional.empty());
        geometry.setMovingSpeed(Optional.empty());
        break;
      case MOVEMENT:
        if (geowebSigmet.getMovementUnit()!=null) {
          geometry.setMovingSpeed(NumericMeasureImpl.of(geowebSigmet.getMovementSpeed(), geowebSigmet.getMovementUnit().name()));
        }
        /* Convert enum to degrees by multiplying */
        if (geowebSigmet.getMovementDirection()!=null) {
          geometry.setMovingDirection(NumericMeasureImpl.of(geowebSigmet.getMovementDirection().ordinal()*22.5, "deg"));
        }
        break;
      case FORECAST_POSITION:
      case NO_VA_EXP:
        break;
      default:
        break;
      }
    }

    /* Set analysis geometry */
    sigmetBuilder.setAnalysisGeometries(Arrays.asList(geometry.build()));

    /* Set forecast geometry */
    if (geowebSigmet.getEndGeometry() != null) {
      PhenomenonGeometryImpl.Builder fpaPhenBuilder=new PhenomenonGeometryImpl.Builder();
      /* TODO: Check timestamp */
      fpaPhenBuilder.setTime(PartialOrCompleteTimeInstant.of(geowebSigmet.getValidDateEnd().atZoneSameInstant(ZoneId.of("UTC"))));

      /* Check that the geometry is (partially) contained, otherwise fail */
      if (!SigmetAirmetUtils.isPartiallyContained(geowebSigmet.getEndGeometry(),geowebSigmet.getFirGeometry())) {
        retval.addIssue(new ConversionIssue(ConversionIssue.Type.LOGICAL, "The end position needs to be (partly) inside the FIR"));
        return retval;
      }

      TacOrGeoGeometry tacOrGeoGeometry_forecast = SigmetAirmetUtils.handleGeometries(geowebSigmet.getEndGeometry(),
                geowebSigmet.getEndGeometryIntersect(),
                geowebSigmet.getFirGeometry());
      fpaPhenBuilder.setGeometry(tacOrGeoGeometry_forecast);
      fpaPhenBuilder.setApproximateLocation(false);
      sigmetBuilder.setForecastGeometries(Arrays.asList(fpaPhenBuilder.build()));
    }

    if (geowebSigmet.getMovementType()==MovementTypeEnum.NO_VA_EXP) {
      PhenomenonGeometryImpl.Builder fpaPhenBuilder=new PhenomenonGeometryImpl.Builder();
      /* TODO: Check timestamp */
      fpaPhenBuilder.setTime(PartialOrCompleteTimeInstant.of(geowebSigmet.getValidDateEnd().atZoneSameInstant(ZoneId.of("UTC"))));
      fpaPhenBuilder.setApproximateLocation(false);
      fpaPhenBuilder.setNoVolcanicAshExpected(true);
      sigmetBuilder.setForecastGeometries(Arrays.asList(fpaPhenBuilder.build()));
    }

    /* Set volcanic ash if needed */
    /* Volcanic sigmet properties */
    if (geowebSigmet.getVaSigmetVolcanoName()!=null|| geowebSigmet.getVaSigmetVolcanoCoordinates() !=null){

      VAInfoImpl.Builder volcanicAshInfo = new VAInfoImpl.Builder();

      VolcanoDescriptionImpl.Builder volcanoDescription = new VolcanoDescriptionImpl.Builder();
      if (geowebSigmet.getVaSigmetVolcanoName() != null) {
        volcanoDescription.setVolcanoName(geowebSigmet.getVaSigmetVolcanoName());
      }

      if (geowebSigmet.getVaSigmetVolcanoCoordinates() !=null) {
        if (geowebSigmet.getVaSigmetVolcanoCoordinates().getLatitude() !=null &&
          geowebSigmet.getVaSigmetVolcanoCoordinates().getLongitude() !=null ) {
            ElevatedPointImpl.Builder volcanoPosition = ElevatedPointImpl.builder();
            List<Double> coordinates = Arrays.asList(
              Double.valueOf(geowebSigmet.getVaSigmetVolcanoCoordinates().getLatitude().doubleValue()),
              Double.valueOf(geowebSigmet.getVaSigmetVolcanoCoordinates().getLongitude().doubleValue()));
            volcanoPosition.setCoordinates(coordinates);
            volcanoDescription.setVolcanoPosition(volcanoPosition.build());
          }
      }

      volcanicAshInfo.setVolcano(volcanoDescription.build());
    //   if (geowebSigmet.getMovementType() == MovementTypeEnum.NO_VA_EXP) {
    //     volcanicAshInfo.se
    //   }

      sigmetBuilder.setVAInfo(volcanicAshInfo.build());

    }

    retval.setConvertedMessage(sigmetBuilder.build());
    return retval;
  }

  private static boolean isShortTestSigmet(final GeoWebSigmet geowebSigmet) {
    return (geowebSigmet.getType() == TypeEnum.SHORT_TEST || geowebSigmet.getType() == TypeEnum.SHORT_VA_TEST)
            && geowebSigmet.getCancelsSigmetSequenceId() == null;
  }

}