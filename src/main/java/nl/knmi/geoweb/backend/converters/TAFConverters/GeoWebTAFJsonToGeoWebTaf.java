package nl.knmi.geoweb.backend.converters.TAFConverters;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import nl.knmi.geoweb.backend.product.GeoWebTAF.GeoWebTAF;

public class GeoWebTAFJsonToGeoWebTaf {
  public GeoWebTAF convert(String geowebTafJSON, ObjectMapper geowebTafObjectMapper) throws JsonMappingException, JsonProcessingException {
    return geowebTafObjectMapper.readValue(
      geowebTafJSON,
      GeoWebTAF.class
    );
  } 
  public String convert(GeoWebTAF geowebTaf, ObjectMapper geowebTafObjectMapper) throws JsonMappingException, JsonProcessingException {
    return geowebTafObjectMapper.writeValueAsString(
      geowebTaf
    );
  } 
}
