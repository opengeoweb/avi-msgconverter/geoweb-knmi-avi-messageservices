package nl.knmi.geoweb.backend.converters.TAFConverters;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fi.fmi.avi.converter.ConversionHints;
import fi.fmi.avi.converter.ConversionIssue;
import fi.fmi.avi.converter.ConversionResult;
import fi.fmi.avi.model.AviationCodeListUser;
import fi.fmi.avi.model.CloudLayer;
import fi.fmi.avi.model.PartialDateTime;
import fi.fmi.avi.model.PartialOrCompleteTimeInstant;
import fi.fmi.avi.model.PartialOrCompleteTimePeriod;
import fi.fmi.avi.model.Weather;
import fi.fmi.avi.model.AviationWeatherMessage.ReportStatus;
import fi.fmi.avi.model.immutable.AerodromeImpl;
import fi.fmi.avi.model.immutable.CloudForecastImpl;
import fi.fmi.avi.model.immutable.CloudLayerImpl;
import fi.fmi.avi.model.immutable.NumericMeasureImpl;
import fi.fmi.avi.model.immutable.SurfaceWindImpl;
import fi.fmi.avi.model.immutable.WeatherImpl;
import fi.fmi.avi.model.taf.TAF;
import fi.fmi.avi.model.taf.TAFBaseForecast;
import fi.fmi.avi.model.taf.TAFChangeForecast;
import fi.fmi.avi.model.taf.immutable.TAFBaseForecastImpl;
import fi.fmi.avi.model.taf.immutable.TAFChangeForecastImpl;
import fi.fmi.avi.model.taf.immutable.TAFImpl;
import lombok.extern.slf4j.Slf4j;
import nl.knmi.geoweb.backend.product.GeoWebTAF.GeoWebTAF;
import nl.knmi.geoweb.backend.product.GeoWebTAF.enums.ChangeEnum;
import nl.knmi.geoweb.backend.product.GeoWebTAF.enums.CloudAppendixEnum;
import nl.knmi.geoweb.backend.product.GeoWebTAF.enums.CloudTypeEnum;
import nl.knmi.geoweb.backend.product.GeoWebTAF.enums.MessageTypeEnum;
import nl.knmi.geoweb.backend.product.GeoWebTAF.enums.ProbabilityEnum;
import nl.knmi.geoweb.backend.product.GeoWebTAF.enums.StatusEnum;
import nl.knmi.geoweb.backend.product.GeoWebTAF.types.BaseForeCast;
import nl.knmi.geoweb.backend.product.GeoWebTAF.types.ChangeGroup;
import nl.knmi.geoweb.backend.product.GeoWebTAF.types.Cloud;
import nl.knmi.geoweb.backend.product.GeoWebTAF.types.Clouds;
import nl.knmi.geoweb.backend.product.GeoWebTAF.types.Visibility;
import nl.knmi.geoweb.backend.product.GeoWebTAF.types.Wind;
import nl.knmi.geoweb.backend.product.GeoWebTAF.types.WindDirection;
import nl.knmi.geoweb.backend.product.GeoWebTAF.types.WindSpeed;
@Slf4j
public class GeoWebTAFToFMITAF {
  public ConversionResult<TAF> convert(GeoWebTAF geowebTaf, ObjectMapper geowebTafObjectMapper, ConversionHints hints) throws JsonMappingException, JsonProcessingException {
    log.info("GeoWebTAFToFMITAF convert");
    ConversionResult<TAF> retval = new ConversionResult<>();
    TAFImpl.Builder tafBuilder = TAFImpl.builder();
    tafBuilder = tafBuilder.setTranslationTime(ZonedDateTime.now(ZoneId.of("Z")));


    AerodromeImpl.Builder aerodromeBuilder = AerodromeImpl.builder()
    .setDesignator(geowebTaf.getLocation());
    tafBuilder = tafBuilder.setAerodrome(aerodromeBuilder.build());
    StatusEnum tafStatus = geowebTaf.getStatus();
    if (tafStatus == null) {
      tafStatus = StatusEnum.NEW;
    }

    switch(geowebTaf.getMessageType()) {
      case AMD:
        tafBuilder = tafBuilder.setReportStatus(ReportStatus.AMENDMENT);
        break;
      case CNL:
        tafBuilder = tafBuilder.setReportStatus(ReportStatus.AMENDMENT);
        tafBuilder.setCancelMessage(true);
        break;
      case COR:
        tafBuilder = tafBuilder.setReportStatus(ReportStatus.CORRECTION);
        break;
      case ORG:
        tafBuilder = tafBuilder.setReportStatus(ReportStatus.NORMAL);
        break;
      default:
        throw new IllegalArgumentException("Unknown TAF message type");
    }

    tafBuilder = tafBuilder.setPermissibleUsage(AviationCodeListUser.PermissibleUsage.OPERATIONAL);

    ZonedDateTime completeStartTime = ZonedDateTime.from(geowebTaf.getValidDateStart());
    ZonedDateTime completeEndTime = ZonedDateTime.from(geowebTaf.getValidDateEnd());

    PartialOrCompleteTimePeriod.Builder validityTimeBuilder = PartialOrCompleteTimePeriod.builder();
    validityTimeBuilder = validityTimeBuilder.setStartTime(PartialOrCompleteTimeInstant.of(PartialDateTime.ofDayHour(completeStartTime, false), completeStartTime));
    validityTimeBuilder = validityTimeBuilder.setEndTime(PartialOrCompleteTimeInstant.of(PartialDateTime.ofDayHour(completeEndTime, true), completeEndTime));

    tafBuilder = tafBuilder.setValidityTime(validityTimeBuilder.build());

    if (geowebTaf.getIssueDate() != null) {
      tafBuilder = tafBuilder.setIssueTime(PartialOrCompleteTimeInstant.of(ZonedDateTime.from(geowebTaf.getIssueDate())));
    }

    /* Reference TAF is needed for these lifecycle statuses */
    if (geowebTaf.getMessageType() == MessageTypeEnum.CNL ||
    geowebTaf.getMessageType() == MessageTypeEnum.AMD ||
    geowebTaf.getMessageType() == MessageTypeEnum.COR){

      OffsetDateTime previousValidityStart = geowebTaf.getPreviousValidDateStart();
      OffsetDateTime previousValidityEnd = geowebTaf.getPreviousValidDateEnd();
      if (previousValidityStart == null || previousValidityEnd == null) {
        retval.addIssue(new ConversionIssue(ConversionIssue.Type.MISSING_DATA, "previousValidityStart or previousValidityEnd is missing"));
        return retval;
      }

      PartialOrCompleteTimePeriod.Builder referredValidityTimeBuilder = PartialOrCompleteTimePeriod.builder()
              .setStartTime(PartialOrCompleteTimeInstant.of(ZonedDateTime.from(previousValidityStart)))
              .setEndTime(PartialOrCompleteTimeInstant.of(PartialDateTime.ofDayHour(previousValidityEnd, true), completeEndTime));
      tafBuilder.setReferredReportValidPeriod(referredValidityTimeBuilder.build());
    }

    if (!geowebTaf.getMessageType().equals(MessageTypeEnum.CNL)) {
      retval.addIssue(updateBaseForecast(tafBuilder, geowebTaf, hints));
      retval.addIssue(updateChangeForecasts(tafBuilder, geowebTaf, hints));
    }

    retval.setConvertedMessage(tafBuilder.build());
    if (retval.getConversionIssues().size()>0) {
      retval.setStatus(ConversionResult.Status.FAIL);
    }else{
      retval.setStatus(ConversionResult.Status.SUCCESS);
    }
    return retval;
  }
  public GeoWebTAF convert(TAF fmiTAF, ObjectMapper geowebTafObjectMapper) throws JsonMappingException, JsonProcessingException {
    return null;
  }

    private List<ConversionIssue> updateBaseForecast(final TAFImpl.Builder tafBuilder, final GeoWebTAF input, ConversionHints hints) {
      List<ConversionIssue> retval = new ArrayList<>();
      final TAFBaseForecastImpl.Builder baseFctBuilder = TAFBaseForecastImpl.builder();
      if (input.getBaseForecast() != null) {
          retval.addAll(updateForecastSurfaceWind(baseFctBuilder, input, hints));
          retval.addAll(updateVisibility(baseFctBuilder, input, hints));

          boolean isCavOK = input.getBaseForecast().getCavOK() !=null && input.getBaseForecast().getCavOK().booleanValue();

          if (!isCavOK) {
              List<ConversionIssue> weather = updateWeather(baseFctBuilder, input, hints);
              if (weather!=null){
                retval.addAll(weather);
              }
              retval.addAll(updateTemperatures(baseFctBuilder, input, hints));
              retval.addAll(updateBaseForecastClouds(baseFctBuilder, input, hints));
          }else {
            baseFctBuilder.setCeilingAndVisibilityOk(true);
          }
      }

      TAFBaseForecast baseFct = baseFctBuilder.build();
      tafBuilder.setBaseForecast(baseFct);
      return retval;
  }
  private List<ConversionIssue> updateForecastSurfaceWind(final TAFBaseForecastImpl.Builder fct, final GeoWebTAF input, ConversionHints hints) {
    List<ConversionIssue> retval = new ArrayList<>();
    SurfaceWindImpl.Builder wind = SurfaceWindImpl.builder();
    BaseForeCast baseForecast = input.getBaseForecast();
    if (baseForecast == null) throw new IllegalArgumentException("No baseforecast");

    Wind baseForecastWind = baseForecast.getWind();
    if (baseForecastWind != null) {

      WindDirection windDirection = baseForecastWind.getDirection();
      if (windDirection!=null) {
        if (windDirection.getIsVRB() !=null && windDirection.getIsVRB()) {
            wind.setVariableDirection(true);
        } else if (windDirection.getDirection()!=null){
            wind.setMeanWindDirection(NumericMeasureImpl.of(windDirection.getDirection(), "deg"));
        } else {
          retval.add(new ConversionIssue(ConversionIssue.Type.MISSING_DATA, "Surface wind direction is missing: "));
        }
      } else {
        retval.add(new ConversionIssue(ConversionIssue.Type.MISSING_DATA, "Surface wind direction is missing: "));
      }

      String windSpeedUnit = baseForecastWind.getUnit();

      if ("KT".equalsIgnoreCase(windSpeedUnit)) {
          windSpeedUnit = "[kn_i]";
      } else if ("MPS".equalsIgnoreCase(windSpeedUnit)) {
          windSpeedUnit = "m/s";
      }

      WindSpeed meanSpeed = baseForecastWind.getSpeed();
      if (meanSpeed != null) {
        if (meanSpeed.getIsP99() !=null && meanSpeed.getIsP99()) {
          //TODO: Check P99!
          wind.setMeanWindSpeedOperator(AviationCodeListUser.RelationalOperator.ABOVE);
          wind.setMeanWindSpeed(NumericMeasureImpl.of(99, windSpeedUnit));
        }else {
          wind.setMeanWindSpeed(NumericMeasureImpl.of(meanSpeed.getSpeed(), windSpeedUnit));
        }

      } else {
          retval.add(new ConversionIssue(ConversionIssue.Type.MISSING_DATA, "Surface wind mean speed is missing: "));
      }

      WindSpeed gustSpeed = baseForecastWind.getGust();
      if (gustSpeed != null) {
        if (gustSpeed.getIsP99() !=null && gustSpeed.getIsP99()) {
          //TODO: Check P99!
          wind.setWindGustOperator(AviationCodeListUser.RelationalOperator.ABOVE);
          wind.setWindGust(NumericMeasureImpl.of(99, windSpeedUnit));
        }else {
          wind.setWindGust(NumericMeasureImpl.of(gustSpeed.getSpeed(), windSpeedUnit));
        }
      }
      fct.setSurfaceWind(wind.build());
    }

    return retval;
  }

  private List<ConversionIssue> updateVisibility(final TAFBaseForecastImpl.Builder fct, final GeoWebTAF input, ConversionHints hints) {
    List<ConversionIssue> retval = new ArrayList<>();
    if ((input.getBaseForecast() != null) && (input.getBaseForecast().getVisibility() != null)) {
        Integer dist = input.getBaseForecast().getVisibility().getRange();
        String unit = input.getBaseForecast().getVisibility().getUnit();
        if (unit == null) unit = "m";
        if (unit.equals("M")) unit = "m";
        if ((dist != null) && (unit != null)) {
            fct.setPrevailingVisibility(NumericMeasureImpl.of(dist, unit));
        } else {
            retval.add(new ConversionIssue(ConversionIssue.Type.MISSING_DATA, "Missing visibility value or unit: "));
        }
        if (dist >= 9999) {
            fct.setPrevailingVisibilityOperator(AviationCodeListUser.RelationalOperator.ABOVE);
        }
    }
    return retval;
  }

  void addToWeatherList ( List<Weather> weatherList, String geoWebWeather){
    if (geoWebWeather == null) return;
    WeatherImpl.Builder weather = WeatherImpl.builder();
    weather.setCode(geoWebWeather);
    weather.setDescription("Longtext for " + geoWebWeather);
    weatherList.add(weather.build());
  }

  private List<ConversionIssue> updateWeather(final TAFBaseForecastImpl.Builder fct, final GeoWebTAF input, ConversionHints hints) {
    List<ConversionIssue> retval = new ArrayList<>();
    List<Weather> weatherList = new ArrayList<>();
    if (input.getBaseForecast().getWeather() == null) {
      return null;
    }
    String weather1 = input.getBaseForecast().getWeather().getWeather1();
    if (weather1.equals("NSW")) {
      fct.setNoSignificantWeather(true);
    } else {
      addToWeatherList(weatherList, weather1);
      addToWeatherList(weatherList, input.getBaseForecast().getWeather().getWeather2());
      addToWeatherList(weatherList, input.getBaseForecast().getWeather().getWeather3());
    }
    if (!weatherList.isEmpty()) {
        fct.setForecastWeather(weatherList);
    } else {
        fct.setForecastWeather(Optional.empty());
    }
    return retval;
  }

  private List<ConversionIssue> updateTemperatures(final TAFBaseForecastImpl.Builder fct, final GeoWebTAF input, ConversionHints hints) {
    List<ConversionIssue> retval = new ArrayList<>();
    return retval;
  }

  private List<ConversionIssue> updateChangeForecasts(final TAFImpl.Builder fctBuilder, final GeoWebTAF input, final ConversionHints hints) {
    List<ConversionIssue> retval = new ArrayList<>();
    List<TAFChangeForecast> changeForecasts = new ArrayList<>();
    if (input.getChangeGroups() != null) {
      for (ChangeGroup ch : input.getChangeGroups()) {
        TAFChangeForecastImpl.Builder changeFct = TAFChangeForecastImpl.builder();
        ChangeEnum changeType = ch.getChange();
        if (changeType!=null) {
          switch (changeType) {
            case TEMPO:
                changeFct.setChangeIndicator(AviationCodeListUser.TAFChangeIndicator.TEMPORARY_FLUCTUATIONS);
                updateChangeForecastContents(changeFct, ch, hints);
                break;
            case BECMG:
                changeFct.setChangeIndicator(AviationCodeListUser.TAFChangeIndicator.BECOMING);
                updateChangeForecastContents(changeFct, ch, hints);
                break;
            case FM:
                changeFct.setChangeIndicator(AviationCodeListUser.TAFChangeIndicator.FROM);
                updateChangeForecastContents(changeFct, ch, hints);
                //The end time of the baseforecast is used as the end time of the FROM changeforecast (as IBLSOFT does for example)
                changeFct.getPeriodOfChangeBuilder().setEndTime(fctBuilder.getValidityTime().get().getEndTime().get());
                break;
            // TODO: At and UNTIL
            // case "AT":
            // case "UNTIL":
            //     retval.add(new ConversionIssue(ConversionIssue.Type.SYNTAX, "Change group " + ch.getChangeType() + " is not allowed in TAF"));
            //     break;
            default:
              throw new IllegalArgumentException("updateChangeForecasts: Not implemented this ChangeGroup Change Type");
          }
        }
        ProbabilityEnum probType = ch.getProbability();
        if (probType != null) {
          switch(probType) {
            case PROB30:
              switch((changeType !=null) ? changeType : ChangeEnum.BECMG){
                case TEMPO:
                  changeFct.setChangeIndicator(AviationCodeListUser.TAFChangeIndicator.PROBABILITY_30_TEMPORARY_FLUCTUATIONS);
                  break;
                case FM:
                  retval.add(new ConversionIssue(ConversionIssue.Type.SYNTAX, "updateChangeForecasts: Combination of PROB30 and FM is not allowed."));
                  return retval;
                default:
                  changeFct.setChangeIndicator(AviationCodeListUser.TAFChangeIndicator.PROBABILITY_30);
              }
              updateChangeForecastContents(changeFct, ch, hints);
              break;
            case PROB40:
            switch((changeType !=null) ? changeType : ChangeEnum.BECMG){
              case TEMPO:
                changeFct.setChangeIndicator(AviationCodeListUser.TAFChangeIndicator.PROBABILITY_40_TEMPORARY_FLUCTUATIONS);
                break;
              case FM:
                retval.add(new ConversionIssue(ConversionIssue.Type.SYNTAX, "updateChangeForecasts: Combination of PROB40 and FM is not allowed."));
                return retval;
              default:
                changeFct.setChangeIndicator(AviationCodeListUser.TAFChangeIndicator.PROBABILITY_40);
            }
              updateChangeForecastContents(changeFct, ch, hints);
              break;
            default:
              throw new IllegalArgumentException("updateChangeForecasts: Not implemented this ChangeGroup Change Type");
          }
        }
        changeForecasts.add(changeFct.build());
      }
    }
    if (!changeForecasts.isEmpty()) {
        fctBuilder.setChangeForecasts(changeForecasts);
    } else {
        fctBuilder.setChangeForecasts(Optional.empty());
    }
    return retval;
  }

  private List<ConversionIssue> updateChangeForecastContents(final TAFChangeForecastImpl.Builder fct, final ChangeGroup input, final ConversionHints hints) {
    List<ConversionIssue> retval = new ArrayList<>();

    if (fct.getChangeIndicator() != AviationCodeListUser.TAFChangeIndicator.FROM) {
        if ((input.getValid()!=null)&&(input.getValid().getStart()!=null)&&(input.getValid().getEnd()!=null)) {
            PartialOrCompleteTimePeriod.Builder periodOfChangeBuilder = PartialOrCompleteTimePeriod.builder();

            ZonedDateTime completeStartTime = ZonedDateTime.from(input.getValid().getStart());
            ZonedDateTime completeEndTime = ZonedDateTime.from(input.getValid().getEnd());

            periodOfChangeBuilder.setStartTime(PartialOrCompleteTimeInstant.of(PartialDateTime.ofDayHour(completeStartTime, false), completeStartTime));
            periodOfChangeBuilder.setEndTime(PartialOrCompleteTimeInstant.of(PartialDateTime.ofDayHour(completeEndTime, true), completeEndTime));
            fct.setPeriodOfChange(periodOfChangeBuilder.build());
        }
    } else {
        if ((input.getValid()!=null)&&(input.getValid().getStart()!=null)) {
            PartialOrCompleteTimePeriod.Builder periodOfChangeBuilder = PartialOrCompleteTimePeriod.builder();
            periodOfChangeBuilder.setStartTime(PartialOrCompleteTimeInstant.of(ZonedDateTime.from(input.getValid().getStart())));
            fct.setPeriodOfChange(periodOfChangeBuilder.build());
        }
    }

    if ((input.getCavOK() != null) && input.getCavOK()) {
        fct.setCeilingAndVisibilityOk(input.getCavOK());
        retval.addAll(updateChangeForecastSurfaceWind(fct, input, hints));
    } else if ((input.getCavOK() == null) || (!input.getCavOK())) {
        fct.setCeilingAndVisibilityOk(false);
        retval.addAll(updateChangeForecastSurfaceWind(fct, input, hints));
        retval.addAll(updateChangeVisibility(fct, input, hints));
        retval.addAll(updateChangeWeather(fct, input, hints));
        retval.addAll(updateChangeClouds(fct, input, hints));
    }

    return retval;
  }

  private List<ConversionIssue> updateChangeForecastSurfaceWind(final TAFChangeForecastImpl.Builder fct, final ChangeGroup input, ConversionHints hints) {
    List<ConversionIssue> retval = new ArrayList<>();
    SurfaceWindImpl.Builder wind = SurfaceWindImpl.builder();

    Wind src = input.getWind();
    if (src != null) {
        WindDirection windDirection = src.getDirection();
        if (windDirection!=null) {
          if (windDirection.getIsVRB() !=null && windDirection.getIsVRB()) {
              wind.setVariableDirection(true);
          } else if (windDirection.getDirection()!=null){
              wind.setMeanWindDirection(NumericMeasureImpl.of(windDirection.getDirection(), "deg"));
          } else {
            retval.add(new ConversionIssue(ConversionIssue.Type.MISSING_DATA, "Surface wind direction is missing: "));
          }
        } else {
          retval.add(new ConversionIssue(ConversionIssue.Type.MISSING_DATA, "Surface wind direction is missing: "));
        }
        String windSpeedUnit = src.getUnit();
        if ("KT".equalsIgnoreCase(windSpeedUnit)) {
            windSpeedUnit = "[kn_i]";
        } else {
            if ("MPS".equalsIgnoreCase(windSpeedUnit)) {
                windSpeedUnit = "m/s";
            }
        }

        WindSpeed meanSpeed = src.getSpeed();
        if (meanSpeed != null) {
          if (meanSpeed.getIsP99() !=null && meanSpeed.getIsP99()){
            wind.setMeanWindSpeedOperator(AviationCodeListUser.RelationalOperator.ABOVE);
            wind.setMeanWindSpeed(NumericMeasureImpl.of(99, windSpeedUnit));
          }else{
            wind.setMeanWindSpeed(NumericMeasureImpl.of(meanSpeed.getSpeed(), windSpeedUnit));
          }
        } else {
            retval.add(new ConversionIssue(ConversionIssue.Type.MISSING_DATA, "Surface wind mean speed is missing: "));
        }

        WindSpeed gustSpeed = src.getGust();
        if (gustSpeed != null) {
          if (gustSpeed.getIsP99() !=null && gustSpeed.getIsP99()){
            //TODO: Check P99!
            wind.setWindGustOperator(AviationCodeListUser.RelationalOperator.ABOVE);
            wind.setWindGust(NumericMeasureImpl.of(99, windSpeedUnit));
          }else{
            wind.setWindGust(NumericMeasureImpl.of(gustSpeed.getSpeed(), windSpeedUnit));
          }
        }
        fct.setSurfaceWind(wind.build());
    } else {
    }

    return retval;
  }

  private List<ConversionIssue> updateChangeVisibility(final TAFChangeForecastImpl.Builder fct, final ChangeGroup input, ConversionHints hints) {
    List<ConversionIssue> retval = new ArrayList<>();
    Visibility src = input.getVisibility();
    Integer dist = null;
    String unit = null;
    if (src != null) {
        dist = src.getRange();
        unit = src.getUnit();
    }
    if (unit == null) unit = "m";
    if (unit.equals("M")) unit = "m";
    if ((dist != null) && (unit != null)) {
        fct.setPrevailingVisibility(NumericMeasureImpl.of(dist, unit));
        if (dist >= 9999) {
            fct.setPrevailingVisibilityOperator(AviationCodeListUser.RelationalOperator.ABOVE);
        }
    } else {
        retval.add(new ConversionIssue(ConversionIssue.Type.MISSING_DATA, "Missing visibility value or unit: "));
        fct.setPrevailingVisibility(Optional.empty());
    }
    return retval;
  }

  private List<ConversionIssue> updateChangeWeather(final TAFChangeForecastImpl.Builder fct, final ChangeGroup input, ConversionHints hints) {
    List<ConversionIssue> retval = new ArrayList<>();
    List<Weather> weatherList = new ArrayList<>();
    if (input.getWeather() != null) {
      String weather1 = input.getWeather().getWeather1();

      if (weather1.equals("NSW")) {
        fct.setNoSignificantWeather(true);
      } else {
          addToWeatherList(weatherList, weather1);
          addToWeatherList(weatherList, input.getWeather().getWeather2());
          addToWeatherList(weatherList, input.getWeather().getWeather3());
      }
    }
    if (!weatherList.isEmpty()) {
        fct.setForecastWeather(weatherList);
    } else {
        fct.setForecastWeather(Optional.empty());
    }
    return retval;
  }

  private void updateBaseForecastCloud(final Cloud cldType, List<CloudLayer> layers, CloudForecastImpl.Builder cloud, ConversionHints hints) {
    if (cldType == null) return;
    CloudTypeEnum cover = cldType.getCoverage();
    CloudAppendixEnum mod = cldType.getType();
    Integer height = cldType.getHeight();
    String unit = "[ft_i]";


    if ((cldType.getIsNSC() != null) && cldType.getIsNSC()) {
        cloud.setNoSignificantCloud(cldType.getIsNSC());
    } else {
        CloudLayerImpl.Builder layer = CloudLayerImpl.builder();
        if (CloudTypeEnum.FEW.equals(cover)) {
            layer.setAmount(AviationCodeListUser.CloudAmount.FEW);
        } else if (CloudTypeEnum.SCT.equals(cover)) {
            layer.setAmount(AviationCodeListUser.CloudAmount.SCT);
        } else if (CloudTypeEnum.BKN.equals(cover)) {
            layer.setAmount(AviationCodeListUser.CloudAmount.BKN);
        } else if (CloudTypeEnum.OVC.equals(cover)) {
            layer.setAmount(AviationCodeListUser.CloudAmount.OVC);
        } else if (CloudTypeEnum.SKC.equals(cover)) {
            layer.setAmount(AviationCodeListUser.CloudAmount.SKC);
        }
        if (CloudAppendixEnum.TCU.equals(mod)) {
            layer.setCloudType(AviationCodeListUser.CloudType.TCU);
        } else if (CloudAppendixEnum.CB.equals(mod)) {
            layer.setCloudType(AviationCodeListUser.CloudType.CB);
        }
        layer.setBase(NumericMeasureImpl.of(height * 100, unit));
        layers.add(layer.build());
    }
  }

  private List<ConversionIssue> updateBaseForecastClouds(final TAFBaseForecastImpl.Builder fct, final GeoWebTAF input, ConversionHints hints) {
    List<ConversionIssue> retval = new ArrayList<>();
    CloudForecastImpl.Builder cloud = CloudForecastImpl.builder();
    List<CloudLayer> layers = new ArrayList<>();
    if (input.getBaseForecast().getCloud() != null) {
      Integer vv = input.getBaseForecast().getCloud().getCloud1().getVerticalVisibility();
      if (vv !=null) {
        String unit = "[ft_i]";
        cloud.setVerticalVisibility(NumericMeasureImpl.of(vv * 100, unit));
      } else{
        updateBaseForecastCloud(input.getBaseForecast().getCloud().getCloud1(), layers, cloud, hints);
        updateBaseForecastCloud(input.getBaseForecast().getCloud().getCloud2(), layers, cloud, hints);
        updateBaseForecastCloud(input.getBaseForecast().getCloud().getCloud3(), layers, cloud, hints);
        updateBaseForecastCloud(input.getBaseForecast().getCloud().getCloud4(), layers, cloud, hints);
      }

    }
    if (!layers.isEmpty()) {
        cloud.setLayers(layers);
    }
    fct.setCloud(cloud.build());
    return retval;
  }


  private void updateChangeCloud(final Cloud cldType, List<CloudLayer> layers, CloudForecastImpl.Builder cloud, ConversionHints hints) {
    if (cldType == null) return;
    CloudTypeEnum cover = cldType.getCoverage();
    CloudAppendixEnum mod = cldType.getType();
    Integer height = cldType.getHeight();
    String unit = "[ft_i]";


    if ((cldType.getIsNSC() != null) && cldType.getIsNSC()) {
        cloud.setNoSignificantCloud(cldType.getIsNSC());
    } else {
        CloudLayerImpl.Builder layer = CloudLayerImpl.builder();
        if (CloudTypeEnum.FEW.equals(cover)) {
            layer.setAmount(AviationCodeListUser.CloudAmount.FEW);
        } else if (CloudTypeEnum.SCT.equals(cover)) {
            layer.setAmount(AviationCodeListUser.CloudAmount.SCT);
        } else if (CloudTypeEnum.BKN.equals(cover)) {
            layer.setAmount(AviationCodeListUser.CloudAmount.BKN);
        } else if (CloudTypeEnum.OVC.equals(cover)) {
            layer.setAmount(AviationCodeListUser.CloudAmount.OVC);
        } else if (CloudTypeEnum.SKC.equals(cover)) {
            layer.setAmount(AviationCodeListUser.CloudAmount.SKC);
        }
        if (CloudAppendixEnum.TCU.equals(mod)) {
            layer.setCloudType(AviationCodeListUser.CloudType.TCU);
        } else if (CloudAppendixEnum.CB.equals(mod)) {
            layer.setCloudType(AviationCodeListUser.CloudType.CB);
        }
        layer.setBase(NumericMeasureImpl.of(height * 100, unit));
        layers.add(layer.build());
    }
  }
  private List<ConversionIssue> updateChangeClouds(final TAFChangeForecast.Builder fct, final ChangeGroup input, ConversionHints hints) {
    List<ConversionIssue> retval = new ArrayList<>();
    CloudForecastImpl.Builder cloud = CloudForecastImpl.builder();
    List<CloudLayer> layers = new ArrayList<>();
    Clouds src = input.getCloud();

    if (src != null) {
        Integer vv = src.getCloud1().getVerticalVisibility();
        if (vv !=null) {
            String unit = "[ft_i]";
            cloud.setVerticalVisibility(NumericMeasureImpl.of(vv * 100, unit));
        } else{
          updateChangeCloud(input.getCloud().getCloud1(), layers, cloud, hints);
          updateChangeCloud(input.getCloud().getCloud2(), layers, cloud, hints);
          updateChangeCloud(input.getCloud().getCloud3(), layers, cloud, hints);
          updateChangeCloud(input.getCloud().getCloud4(), layers, cloud, hints);
        }
    }

    if (layers.size()>0) {
        cloud.setLayers(layers);
    }
    fct.setCloud(cloud.build());
    return retval;
  }
}
