package nl.knmi.geoweb.backend.converters.TAFConverters;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import fi.fmi.avi.converter.ConversionHints;
import nl.knmi.geoweb.backend.aviation.Tools;
import nl.knmi.geoweb.backend.product.GeoWebTAF.GeoWebTAF;

public class TafUtils {
    /**
   * Loads a json from the resources and converts it to a GeoWeb TAF Pojo
   * @param resource
   * @return
   * @throws IOException
   */
  public static GeoWebTAF getGeoWebTafFromResource(String resource, ObjectMapper geowebTafObjectMapper) throws IOException {
    String json = Tools.readResource(resource);
    if (json == null) {
      throw new IOException("Unable to load resource " + resource);

    }
    return (new GeoWebTAFJsonToGeoWebTaf()).convert(json, geowebTafObjectMapper);
  }

  public static ConversionHints getTokenizerParsingHints() {
    Map<ConversionHints.Key,Object> hints = new HashMap<>();
    hints.put(ConversionHints.KEY_VALIDTIME_FORMAT,
    ConversionHints.VALUE_VALIDTIME_FORMAT_PREFER_LONG);
    hints.put(ConversionHints.KEY_TAF_REFERENCE_POLICY,
    ConversionHints.VALUE_TAF_REFERENCE_POLICY_USE_OWN_VALID_TIME_ONLY);
    
    return new ConversionHints(hints);
  }
}
