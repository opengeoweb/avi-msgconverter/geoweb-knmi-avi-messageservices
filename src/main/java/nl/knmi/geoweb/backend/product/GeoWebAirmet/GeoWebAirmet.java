package nl.knmi.geoweb.backend.product.GeoWebAirmet;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.time.OffsetDateTime;
import lombok.Getter;
import lombok.Setter;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.ChangeEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.LevelInfoModeEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.MovementDirectionEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.MovementTypeEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.MovementUnitEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.ObservationOrForecastEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.PhenomenaEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.StatusEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.TypeEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.WindUnitEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.VisibilityCauseEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.VisibilityUnitEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.CloudLevelInfoModeEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.types.LevelType;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.types.CloudLevelType;

import org.geojson.FeatureCollection;

@JsonPropertyOrder(alphabetic = true)
@JsonInclude(Include.NON_NULL)
@Getter
@Setter
public class GeoWebAirmet {

  private PhenomenaEnum phenomenon;
  private ChangeEnum change;
  private String locationIndicatorATSR;
  private String locationIndicatorATSU;
  private String locationIndicatorMWO;
  private String firName;
  private ObservationOrForecastEnum isObservationOrForecast;
  private LevelInfoModeEnum levelInfoMode;
  private LevelType level;
  private LevelType lowerLevel;
  private TypeEnum type;

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private OffsetDateTime observationOrForecastTime;

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private OffsetDateTime validDateEnd;

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private OffsetDateTime validDateStart;

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private OffsetDateTime issueDate;

  private String uuid;
  private String sequence;
  private String cancelsAirmetSequenceId;
  private int movementSpeed;
  private MovementUnitEnum movementUnit;
  private MovementTypeEnum movementType;
  private MovementDirectionEnum movementDirection;
  private StatusEnum status;
  private FeatureCollection startGeometry;
  private FeatureCollection startGeometryIntersect;
  private FeatureCollection firGeometry;

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private OffsetDateTime validDateStartOfAirmetToCancel;

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private OffsetDateTime validDateEndOfAirmetToCancel;

  /* Only for phenomenon surface wind */
  private int windSpeed;
  private WindUnitEnum windUnit;
  private int windDirection;

  /* Only for phenomenon surface visibility */
  private int visibilityValue;
  private VisibilityCauseEnum visibilityCause;
  private VisibilityUnitEnum  VisibilityUnit;

  /* Only for Cloud phenomena */
  private CloudLevelInfoModeEnum cloudLevelInfoMode;
  private CloudLevelType cloudLevel;
  private CloudLevelType cloudLowerLevel;
}
