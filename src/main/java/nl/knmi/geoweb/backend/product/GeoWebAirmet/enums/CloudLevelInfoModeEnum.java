package nl.knmi.geoweb.backend.product.GeoWebAirmet.enums;

public enum CloudLevelInfoModeEnum {
    BETW,
    BETW_SFC,
    BETW_SFC_ABV,
    BETW_ABV,
}
