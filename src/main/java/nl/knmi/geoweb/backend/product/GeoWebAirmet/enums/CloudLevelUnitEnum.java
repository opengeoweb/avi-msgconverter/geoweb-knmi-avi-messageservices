package nl.knmi.geoweb.backend.product.GeoWebAirmet.enums;

public enum CloudLevelUnitEnum {
  FT,
  M,
}
