package nl.knmi.geoweb.backend.product.GeoWebAirmet.enums;

public enum LevelUnitEnum {
  FT,
  FL,
  M,
}
