package nl.knmi.geoweb.backend.product.GeoWebAirmet.enums;

public enum MovementDirectionEnum {
  N,
  NNE,
  NE,
  ENE,
  E,
  ESE,
  SE,
  SSE,
  S,
  SSW,
  SW,
  WSW,
  W,
  WNW,
  NW,
  NNW,
}
