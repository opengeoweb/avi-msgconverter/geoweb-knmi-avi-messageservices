package nl.knmi.geoweb.backend.product.GeoWebAirmet.enums;

public enum MovementTypeEnum {
  STATIONARY,
  MOVEMENT,
}
