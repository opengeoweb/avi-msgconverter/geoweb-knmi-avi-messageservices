package nl.knmi.geoweb.backend.product.GeoWebAirmet.enums;

public enum MovementUnitEnum {
  KT,
  KMH
}
