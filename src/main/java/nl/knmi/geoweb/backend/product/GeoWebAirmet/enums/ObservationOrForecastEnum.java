package nl.knmi.geoweb.backend.product.GeoWebAirmet.enums;

public enum ObservationOrForecastEnum {
  OBS,
  FCST,
}
