package nl.knmi.geoweb.backend.product.GeoWebAirmet.enums;

public enum PhenomenaEnum {
  BKN_CLD, 
  OVC_CLD,
  FRQ_CB,
  FRQ_TCU,
  ISOL_CB,
  ISOL_TCU,
  ISOL_TS,
  ISOL_TSGR,
  MOD_ICE,
  MOD_MTW,
  MOD_TURB, 
  MT_OBSC,
  OCNL_CB,
  OCNL_TS,
  OCNL_TSGR,
  OCNL_TCU,
  SFC_VIS,
  SFC_WIND 
}
