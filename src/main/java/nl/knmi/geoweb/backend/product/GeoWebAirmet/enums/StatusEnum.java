package nl.knmi.geoweb.backend.product.GeoWebAirmet.enums;

public enum StatusEnum {
  DRAFT,
  PUBLISHED,
  CANCELLED,
  EXPIRED,
}
