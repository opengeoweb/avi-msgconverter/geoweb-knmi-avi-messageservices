package nl.knmi.geoweb.backend.product.GeoWebAirmet.enums;

public enum TypeEnum {
  NORMAL,
  TEST,
  EXERCISE,
}
