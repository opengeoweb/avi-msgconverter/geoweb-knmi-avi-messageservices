package nl.knmi.geoweb.backend.product.GeoWebAirmet.enums;

public enum VisibilityCauseEnum {
    DZ,
    DU,
    PO,
    DS,
    FG,
    FC,
    GR,
    HZ,
    PL,
    BR,
    RA,
    SA,
    SS,
    FU,
    SN,
    SG,
    GS,
    SQ,
    VA
}
