package nl.knmi.geoweb.backend.product.GeoWebAirmet.enums;

public enum WindUnitEnum {
  KT,
  MPS
}
