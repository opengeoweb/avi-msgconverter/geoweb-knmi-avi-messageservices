package nl.knmi.geoweb.backend.product.GeoWebAirmet.om;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsonorg.JsonOrgModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

public class CreateAirmetObjectMapper {

  private static final String DATEFORMAT_ISO8601 = "yyyy-MM-dd'TT'HH:mm:ss'Y'";

  public static ObjectMapper getOm() {
    ObjectMapper om = new ObjectMapper();
    om.registerModule(new Jdk8Module());
    om.registerModule(new JavaTimeModule());
    om.registerModule(new JsonOrgModule());
    om.setDateFormat(new SimpleDateFormat(DATEFORMAT_ISO8601));
    om.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    SimpleModule simpleModule = new SimpleModule();
    simpleModule.addSerializer(
      OffsetDateTime.class,
      new JsonSerializer<OffsetDateTime>() {
        @Override
        public void serialize(
          OffsetDateTime offsetDateTime,
          JsonGenerator jsonGenerator,
          SerializerProvider serializerProvider
        )
          throws IOException {
          String formattedDate = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(
            offsetDateTime
          );
          jsonGenerator.writeString(formattedDate);
        }
      }
    );
    om.registerModule(simpleModule);
    om.setTimeZone(TimeZone.getTimeZone("UTC"));
    om.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
    om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    om.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    om.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
    om.configure(SerializationFeature.INDENT_OUTPUT, true);
    return om;
  }
}
