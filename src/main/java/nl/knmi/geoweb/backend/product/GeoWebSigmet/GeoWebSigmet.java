package nl.knmi.geoweb.backend.product.GeoWebSigmet;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.time.OffsetDateTime;
import lombok.Getter;
import lombok.Setter;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.enums.ChangeEnum;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.enums.LevelInfoModeEnum;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.enums.MovementDirectionEnum;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.enums.MovementTypeEnum;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.enums.MovementUnitEnum;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.enums.ObservationOrForecastEnum;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.enums.PhenomenaEnum;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.enums.StatusEnum;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.enums.TypeEnum;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.types.Coordinate;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.types.LevelType;
import org.geojson.FeatureCollection;

@JsonPropertyOrder(alphabetic = true)
@JsonInclude(Include.NON_NULL)
@Getter
@Setter
public class GeoWebSigmet {

  private PhenomenaEnum phenomenon;
  private ChangeEnum change;
  private String locationIndicatorATSR;
  private String locationIndicatorATSU;
  private String locationIndicatorMWO;
  private String firName;
  private ObservationOrForecastEnum isObservationOrForecast;
  private LevelInfoModeEnum levelInfoMode;
  private LevelType level;
  private LevelType lowerLevel;
  private TypeEnum type;

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private OffsetDateTime observationOrForecastTime;

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private OffsetDateTime validDateEnd;

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private OffsetDateTime validDateStart;

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private OffsetDateTime issueDate;

  private String uuid;
  private String sequence;
  private String cancelsSigmetSequenceId;
  private int movementSpeed;
  private MovementUnitEnum movementUnit;
  private MovementTypeEnum movementType;
  private MovementDirectionEnum movementDirection;
  private StatusEnum status;
  private FeatureCollection endGeometryIntersect;
  private FeatureCollection endGeometry;
  private FeatureCollection startGeometry;
  private FeatureCollection startGeometryIntersect;
  private FeatureCollection firGeometry;

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private OffsetDateTime validDateStartOfSigmetToCancel;

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private OffsetDateTime validDateEndOfSigmetToCancel;

  /* VA Sigmet */
  private String vaSigmetVolcanoName;
  private Coordinate vaSigmetVolcanoCoordinates;
  private String vaSigmetMoveToFIR;

}
