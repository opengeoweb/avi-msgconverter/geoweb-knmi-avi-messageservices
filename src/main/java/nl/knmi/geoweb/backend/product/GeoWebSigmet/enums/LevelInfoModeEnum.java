package nl.knmi.geoweb.backend.product.GeoWebSigmet.enums;

public enum LevelInfoModeEnum {
  AT,
  BETW,
  BETW_SFC,
  TOPS,
  TOPS_ABV,
  TOPS_BLW,
  ABV,
}
