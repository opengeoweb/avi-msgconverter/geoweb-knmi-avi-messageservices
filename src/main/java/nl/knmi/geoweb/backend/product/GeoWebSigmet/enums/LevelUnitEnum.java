package nl.knmi.geoweb.backend.product.GeoWebSigmet.enums;

public enum LevelUnitEnum {
  FT,
  FL,
  M,
}
