package nl.knmi.geoweb.backend.product.GeoWebSigmet.enums;

public enum MovementDirectionEnum {
  N,
  NNE,
  NE,
  ENE,
  E,
  ESE,
  SE,
  SSE,
  S,
  SSW,
  SW,
  WSW,
  W,
  WNW,
  NW,
  NNW,
}
