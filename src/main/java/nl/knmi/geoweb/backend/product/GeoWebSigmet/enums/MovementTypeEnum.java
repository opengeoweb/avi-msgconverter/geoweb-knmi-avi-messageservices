package nl.knmi.geoweb.backend.product.GeoWebSigmet.enums;

public enum MovementTypeEnum {
  STATIONARY,
  MOVEMENT,
  FORECAST_POSITION,
  NO_VA_EXP,
}
