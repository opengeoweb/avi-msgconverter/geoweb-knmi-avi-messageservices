package nl.knmi.geoweb.backend.product.GeoWebSigmet.enums;

public enum MovementUnitEnum {
  KT,
  KMH
}
