package nl.knmi.geoweb.backend.product.GeoWebSigmet.enums;

public enum ObservationOrForecastEnum {
  OBS,
  FCST,
}
