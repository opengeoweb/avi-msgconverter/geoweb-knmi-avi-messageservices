package nl.knmi.geoweb.backend.product.GeoWebSigmet.enums;

public enum PhenomenaEnum {
  EMBD_TS,
  EMBD_TSGR,
  FRQ_TS,
  FRQ_TSGR,
  HVY_DS,
  HVY_SS,
  OBSC_TS,
  OBSC_TSGR,
  RDOACT_CLD,
  SEV_ICE,
  SEV_ICE_FRZ_RN,
  SEV_MTW,
  SEV_TURB,
  SQL_TS,
  SQL_TSGR,
  TC,
  VA_CLD,
}
