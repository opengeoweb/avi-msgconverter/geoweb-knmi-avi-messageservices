package nl.knmi.geoweb.backend.product.GeoWebSigmet.enums;

public enum StatusEnum {
  DRAFT,
  PUBLISHED,
  CANCELLED,
  EXPIRED,
}
