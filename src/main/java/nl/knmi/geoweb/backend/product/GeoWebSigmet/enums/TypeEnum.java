package nl.knmi.geoweb.backend.product.GeoWebSigmet.enums;

public enum TypeEnum {
  NORMAL,
  TEST,
  EXERCISE,
  SHORT_TEST,
  SHORT_VA_TEST
}
