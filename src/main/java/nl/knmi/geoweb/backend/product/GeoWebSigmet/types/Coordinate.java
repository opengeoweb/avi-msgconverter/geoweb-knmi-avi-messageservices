package nl.knmi.geoweb.backend.product.GeoWebSigmet.types;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Coordinate {
    private Float latitude;
    private Float longitude;
}
