package nl.knmi.geoweb.backend.product.GeoWebSigmet.types;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.enums.LevelUnitEnum;

@JsonPropertyOrder(alphabetic = true)
@JsonInclude(Include.NON_NULL)
@Getter
@Setter
public class LevelType {

  int value;
  LevelUnitEnum unit;
}
