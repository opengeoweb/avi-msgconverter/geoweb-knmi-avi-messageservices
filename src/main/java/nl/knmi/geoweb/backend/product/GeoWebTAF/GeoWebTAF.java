package nl.knmi.geoweb.backend.product.GeoWebTAF;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.time.OffsetDateTime;
import lombok.Getter;
import lombok.Setter;
import nl.knmi.geoweb.backend.product.GeoWebTAF.enums.MessageTypeEnum;
import nl.knmi.geoweb.backend.product.GeoWebTAF.enums.StatusEnum;
import nl.knmi.geoweb.backend.product.GeoWebTAF.enums.TypeEnum;
import nl.knmi.geoweb.backend.product.GeoWebTAF.types.BaseForeCast;
import nl.knmi.geoweb.backend.product.GeoWebTAF.types.ChangeGroup;

@JsonPropertyOrder(alphabetic = true)
@JsonInclude(Include.NON_NULL)
@Getter
@Setter
public class GeoWebTAF {
  private String uuid;

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private OffsetDateTime issueDate;

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private OffsetDateTime baseTime;

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private OffsetDateTime validDateStart;

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private OffsetDateTime validDateEnd;

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private OffsetDateTime previousValidDateStart;

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private OffsetDateTime previousValidDateEnd;


  private String location;

  private StatusEnum status;

  private TypeEnum type;

  private MessageTypeEnum messageType;

  private BaseForeCast baseForecast;

  private ChangeGroup changeGroups[];

}
