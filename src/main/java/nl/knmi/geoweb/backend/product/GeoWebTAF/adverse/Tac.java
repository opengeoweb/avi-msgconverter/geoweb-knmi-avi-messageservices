package nl.knmi.geoweb.backend.product.GeoWebTAF.adverse;

import java.util.ArrayList;
import java.util.List;

import fi.fmi.avi.converter.tac.lexer.Lexeme;
import fi.fmi.avi.converter.tac.lexer.LexemeIdentity;
import fi.fmi.avi.converter.tac.lexer.LexemeSequence;

public class Tac extends ArrayList<TacLine> {
    public Tac(LexemeSequence sequence) {
        super();
        List<Lexeme> lexemeList = new ArrayList<>();
        for (LexemeSequence seq : sequence.splitBy(LexemeIdentity.TAF_FORECAST_CHANGE_INDICATOR)) {
            lexemeList.addAll(seq.getLexemes());
            if (seq.getLexemes().size()>=3) {
                this.add(new TacLine(lexemeList));
                lexemeList.clear();
            }
        }
    }
}
