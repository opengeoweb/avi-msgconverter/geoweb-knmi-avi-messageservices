package nl.knmi.geoweb.backend.product.GeoWebTAF.adverse;

import java.util.ArrayList;
import java.util.List;

import fi.fmi.avi.converter.tac.lexer.Lexeme;
import fi.fmi.avi.converter.tac.lexer.LexemeSequence;

public class TacLine extends ArrayList<TacValue> {
    public TacLine(LexemeSequence seq) {
        super();
        for (Lexeme lexeme : seq.getLexemes()) {
            if (lexeme.getIdentity().name().equals("WHITE_SPACE")) {
                continue;
            }
            this.add(new TacValue(lexeme));
        }
    }

    public TacLine(List<Lexeme>seq) {
        super();
        for (Lexeme lexeme : seq) {
            if (lexeme.getIdentity().name().equals("WHITE_SPACE")) {
                continue;
            }
            this.add(new TacValue(lexeme));
        }
    }

}
