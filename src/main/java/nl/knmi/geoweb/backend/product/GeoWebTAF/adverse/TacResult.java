package nl.knmi.geoweb.backend.product.GeoWebTAF.adverse;

public class TacResult {
  public Tac tac;
  public TacErrors errors;

  public TacResult() {
    super();
    errors = new TacErrors();
  }

  public TacResult setTac(Tac tac) {
    this.tac = tac;
    return this;
  }

  public TacResult addError(String error) {
    this.errors.add(error);
    return this;
  }
}
