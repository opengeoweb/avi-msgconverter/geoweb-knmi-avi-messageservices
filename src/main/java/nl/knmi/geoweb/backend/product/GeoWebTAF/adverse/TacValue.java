package nl.knmi.geoweb.backend.product.GeoWebTAF.adverse;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fi.fmi.avi.converter.tac.lexer.Lexeme;

public class TacValue extends HashMap<String, String> {
    public TacValue(Lexeme lexeme) {
        super();

        boolean adverse = false;
        switch (lexeme.getIdentity().name()) {
            case "SURFACE_WIND":
                if (isAdverseWind(lexeme))
                    adverse = true;
                break;
            case "HORIZONTAL_VISIBILITY":
                if (isAdverseVisibility(lexeme))
                    adverse = true;
                break;
            case "WEATHER":
                if (isAdverseWeather(lexeme))
                    adverse = true;
                break;
            case "CLOUD":
                if (isAdverseCloud(lexeme))
                    adverse = true;
                break;
        }

        this.put("text", lexeme.getTACToken());
        if (adverse)
            this.put("type", "adverse");
    }

    private boolean isAdverseWind(Lexeme lexeme) {
        // only consider values if unit is KT
        Pattern p = Pattern.compile("^(VRB|[0-9]{3})(P?[0-9]{2})(GP?[0-9]{2})?KT$");
        Matcher m = p.matcher(lexeme.getTACToken());

        if (!m.find()) {
            return false;
        }

        // wind speed >= 20 KT
        if (m.group(2).startsWith("P") || m.group(2).matches("[2-9][0-9]$")) {
            return true;
        }

        // wind gusts >= 20 KT
        if (m.group(3) == null) {
            return false;
        }
        if (m.group(3).startsWith("GP") || m.group(3).matches("[2-9][0-9]$")) {
            return true;
        }

        return false;
    }

    private boolean isAdverseVisibility(Lexeme lexeme) {
        String pattern = "^(1000|0[0-9][0,5]0)$";
        if (lexeme.getTACToken().matches(pattern)) {
            return true;
        }
        return false;
    }

    private boolean isAdverseWeather(Lexeme lexeme) {
        String pattern = "^([A-Z]{2}){0,4}(TS|FG|FZ|SN|PL|FC|SQ|SS|DS|WS|GS|GR)([A-Z]{2}){0,4}$";
        if (lexeme.getTACToken().startsWith("+") || lexeme.getTACToken().matches(pattern)) {
            return true;
        }
        return false;
    }

    private boolean isAdverseCloud(Lexeme lexeme) {
        String[] patterns = { "^BKN00[0-9]$", "^BKN//$", "^OVC00[0-9]$", "^OVC//$", "^VV00[0-3]$", "^VV//$" };
        for (String pattern : patterns) {
            if (lexeme.getTACToken().matches(pattern)) {
                return true;
            }
        }
        return false;
    }
}
