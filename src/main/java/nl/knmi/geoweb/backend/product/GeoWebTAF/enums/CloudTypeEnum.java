package nl.knmi.geoweb.backend.product.GeoWebTAF.enums;

public enum CloudTypeEnum {
  FEW, 
  SCT, 
  BKN,
  OVC,
  SKC;
}
