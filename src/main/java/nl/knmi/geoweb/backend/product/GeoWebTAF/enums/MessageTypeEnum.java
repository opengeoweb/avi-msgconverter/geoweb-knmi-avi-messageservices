package nl.knmi.geoweb.backend.product.GeoWebTAF.enums;

public enum MessageTypeEnum {
  ORG,
  AMD,
  COR,
  CNL,
}
