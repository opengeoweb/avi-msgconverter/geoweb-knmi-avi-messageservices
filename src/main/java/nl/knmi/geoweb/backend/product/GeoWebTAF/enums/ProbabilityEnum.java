package nl.knmi.geoweb.backend.product.GeoWebTAF.enums;

public enum ProbabilityEnum {
  PROB30, 
  PROB40
}
