package nl.knmi.geoweb.backend.product.GeoWebTAF.enums;

public enum StatusEnum {
  NEW,
  DRAFT,
  PUBLISHED,
  CORRECTED,
  AMENDED,
  CANCELLED,
  EXPIRED,
  DRAFT_AMENDED,
  DRAFT_CORRECTED,
}
