package nl.knmi.geoweb.backend.product.GeoWebTAF.enums;

public enum TypeEnum {
  NORMAL,
  TEST,
  EXERCISE,
}
