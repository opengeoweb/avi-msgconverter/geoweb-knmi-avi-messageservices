package nl.knmi.geoweb.backend.product.GeoWebTAF.types;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;

@JsonPropertyOrder(alphabetic = true)
// @JsonInclude(Include.NON_NULL)
@Getter
@Setter
public class BaseForeCast {
  private Valid valid;
  private Visibility visibility;

  private Clouds cloud;

  private Weather weather;

  private Wind wind;

  private Boolean cavOK;

}
