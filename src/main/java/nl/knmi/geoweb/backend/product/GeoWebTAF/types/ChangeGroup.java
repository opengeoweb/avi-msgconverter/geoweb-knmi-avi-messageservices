package nl.knmi.geoweb.backend.product.GeoWebTAF.types;

import nl.knmi.geoweb.backend.product.GeoWebTAF.enums.ChangeEnum;
import nl.knmi.geoweb.backend.product.GeoWebTAF.enums.ProbabilityEnum;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;

@JsonPropertyOrder(alphabetic = true)
@JsonInclude(Include.NON_NULL)
@Getter
@Setter
public class ChangeGroup extends BaseForeCast{
  private ChangeEnum change;

  private ProbabilityEnum probability;


}
