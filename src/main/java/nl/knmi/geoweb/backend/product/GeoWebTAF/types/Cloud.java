package nl.knmi.geoweb.backend.product.GeoWebTAF.types;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;
import nl.knmi.geoweb.backend.product.GeoWebTAF.enums.CloudAppendixEnum;
import nl.knmi.geoweb.backend.product.GeoWebTAF.enums.CloudTypeEnum;

@JsonPropertyOrder(alphabetic = true)
@JsonInclude(Include.NON_NULL)
@Getter
@Setter
public class Cloud  {
  public Cloud() {
    this.isNSC = null;
  } 

  public Cloud(String cld) {
    if ("NSC".equalsIgnoreCase(cld)) {
        isNSC = true;
    }
  }

  public Cloud(Integer verticalVisibility) {
    this.verticalVisibility = verticalVisibility;
  }

  public Cloud(Cloud cld) {
    this.isNSC=cld.getIsNSC();
    this.coverage=cld.getCoverage();
    this.height=cld.getHeight();
    this.type=cld.getType();
  }

  private CloudTypeEnum coverage;
  private Integer height;
  private CloudAppendixEnum type;
  @JsonInclude(JsonInclude.Include.NON_NULL)
  Boolean isNSC = null;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Integer verticalVisibility;
}
