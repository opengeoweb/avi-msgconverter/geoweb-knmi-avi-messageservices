package nl.knmi.geoweb.backend.product.GeoWebTAF.types;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import lombok.Getter;
import lombok.Setter;

@JsonPropertyOrder(alphabetic = true)
@JsonInclude(Include.NON_NULL)
@Getter
@Setter
public class CloudSerializer extends StdSerializer<Cloud> {
  public CloudSerializer() {
    this(null);
} 
  protected CloudSerializer(Class<Cloud> vc) {
		super(vc);
  }
  
	private static final long serialVersionUID = 1L;

  @Override
  public void serialize(Cloud value, JsonGenerator gen, SerializerProvider provider) throws IOException {
    /* Cloud can be NSC or a Cloud object */
    if (value.getIsNSC() != null && value.getIsNSC()) {
			gen.writeString("NSC");
		} else {
			gen.writeObject(value);
		}    
  }
  
}
