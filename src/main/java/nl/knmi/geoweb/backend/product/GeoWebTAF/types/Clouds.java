package nl.knmi.geoweb.backend.product.GeoWebTAF.types;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;

@JsonPropertyOrder(alphabetic = true)
@JsonInclude(Include.NON_NULL)
@Getter
@Setter
public class Clouds {
  private Cloud cloud1;
  private Cloud cloud2;
  private Cloud cloud3;
  private Cloud cloud4;
}
