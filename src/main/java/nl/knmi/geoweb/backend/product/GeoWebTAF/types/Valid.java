package nl.knmi.geoweb.backend.product.GeoWebTAF.types;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.time.OffsetDateTime;
import lombok.Getter;
import lombok.Setter;

@JsonPropertyOrder(alphabetic = true)
@JsonInclude(Include.NON_NULL)
@Getter
@Setter
public class Valid {
  
  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private OffsetDateTime start;

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private OffsetDateTime end;
}
