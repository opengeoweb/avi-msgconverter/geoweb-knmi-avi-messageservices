package nl.knmi.geoweb.backend.product.GeoWebTAF.types;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;

@JsonPropertyOrder(alphabetic = true)
@JsonInclude(Include.NON_NULL)
@Getter
@Setter
public class Weather {
  private String weather1;
  private String weather2;
  private String weather3;
}
