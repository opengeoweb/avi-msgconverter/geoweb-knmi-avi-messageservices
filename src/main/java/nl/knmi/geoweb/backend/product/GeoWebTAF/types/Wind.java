package nl.knmi.geoweb.backend.product.GeoWebTAF.types;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;

@JsonPropertyOrder(alphabetic = true)
@JsonInclude(Include.NON_NULL)
@Getter
@Setter
public class Wind {
  /*   "wind": { "direction": 180, "speed": 10, "unit": "KT" }, */

  private WindDirection direction;

  private WindSpeed speed;

  private String unit;

  private WindSpeed gust;

}
