package nl.knmi.geoweb.backend.product.GeoWebTAF.types;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;

@JsonSerialize(using = WindDirectionSerializer.class)
@JsonPropertyOrder(alphabetic = true)
@JsonInclude(Include.NON_NULL)
@Getter
@Setter
public class WindDirection {
  public WindDirection() {
    this.isVRB = null;
  } 

  public WindDirection(String vrb) {
    if ("VRB".equalsIgnoreCase(vrb)) {
      isVRB = true;
    }
  }

  public WindDirection(Integer direction) {
    this.direction = direction;
  }

  public WindDirection(WindDirection windDirection) {
    this.isVRB=windDirection.getIsVRB();
    this.direction=windDirection.getDirection();
  }
  
  private Integer direction;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  Boolean isVRB = null;
}
