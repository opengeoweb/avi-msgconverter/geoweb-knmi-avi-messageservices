package nl.knmi.geoweb.backend.product.GeoWebTAF.types;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import lombok.Getter;
import lombok.Setter;

@JsonPropertyOrder(alphabetic = true)
@JsonInclude(Include.NON_NULL)
@Getter
@Setter
public class WindDirectionSerializer extends StdSerializer<WindDirection> {
  public WindDirectionSerializer() {
    this(null);
} 
  protected WindDirectionSerializer(Class<WindDirection> vc) {
		super(vc);
  }
  
	private static final long serialVersionUID = 1L;

  @Override
  public void serialize(WindDirection value, JsonGenerator gen, SerializerProvider provider) throws IOException {
    /* WindDirection can be VRB or a integer */
    if (value.getIsVRB() != null && value.getIsVRB()) {
			gen.writeString("VRB");
		} else {
      gen.writeNumber(value.getDirection());
		}    
  }
  
}
