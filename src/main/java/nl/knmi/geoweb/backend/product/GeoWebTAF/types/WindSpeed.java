package nl.knmi.geoweb.backend.product.GeoWebTAF.types;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;

@JsonSerialize(using = WindSpeedSerializer.class)
@JsonPropertyOrder(alphabetic = true)
@JsonInclude(Include.NON_NULL)
@Getter
@Setter
public class WindSpeed {
  public WindSpeed() {
    this.isP99 = null;
  } 

  public WindSpeed(String p99) {
    if ("P99".equalsIgnoreCase(p99)) {
      isP99 = true;
    }
  }

  public WindSpeed(Integer speed) {
    this.speed = speed;
  }

  public WindSpeed(WindSpeed windSpeed) {
    this.isP99=windSpeed.getIsP99();
    this.speed=windSpeed.getSpeed();
  }
  
  private Integer speed;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  Boolean isP99 = null;
}
