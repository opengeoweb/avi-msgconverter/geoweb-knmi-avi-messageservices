package nl.knmi.geoweb.backend.product.GeoWebTAF.types;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import lombok.Getter;
import lombok.Setter;

@JsonPropertyOrder(alphabetic = true)
@JsonInclude(Include.NON_NULL)
@Getter
@Setter
public class WindSpeedSerializer extends StdSerializer<WindSpeed> {
  public WindSpeedSerializer() {
    this(null);
} 
  protected WindSpeedSerializer(Class<WindSpeed> vc) {
		super(vc);
  }
  
	private static final long serialVersionUID = 1L;

  @Override
  public void serialize(WindSpeed value, JsonGenerator gen, SerializerProvider provider) throws IOException {
    /* WindSpeed can be VRB or a integer */
    if (value.getIsP99() != null && value.getIsP99()) {
			gen.writeString("P99");
		} else {
      gen.writeNumber(value.getSpeed());
		}    
  }
  
}
