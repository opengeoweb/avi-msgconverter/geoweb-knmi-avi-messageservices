package nl.knmi.geoweb.backend.product.sigmetairmet;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import nl.knmi.geoweb.backend.utils.GeoUtilsKnmi;
import nl.knmi.geoweb.backend.utils.IntersectionLimitUtil;

import org.geojson.Feature;
import org.geojson.FeatureCollection;
import org.geojson.GeoJsonObject;
import org.geojson.LngLatAlt;
import org.geojson.Point;
import org.geojson.Polygon;
import org.geojson.MultiPolygon;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.PrecisionModel;
import org.locationtech.jts.geom.impl.CoordinateArraySequenceFactory;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.geojson.GeoJsonReader;
import org.locationtech.jts.io.geojson.GeoJsonWriter;

import fi.fmi.avi.model.TacOrGeoGeometry;
import fi.fmi.avi.model.immutable.TacGeometryImpl;
import fi.fmi.avi.model.immutable.TacOrGeoGeometryImpl;


@Slf4j
public class SigmetAirmetUtils {

  private static String START = "start";
  private static String INTERSECTION = "intersection";

  public static String convertLat(double lat) {
    String latDM = "";
    if (lat < 0) {
      latDM = "S";
      lat = Math.abs(lat);
    } else {
      latDM = "N";
    }

    int degreesLat = (int) Math.floor(lat);
    double fracPartLat = lat - degreesLat;
    int minutesLat = (int) Math.round(fracPartLat * 60.0);

    if (minutesLat==60) {
        degreesLat++;
        minutesLat=0;
    }
    latDM += String.format("%02d", degreesLat);
    latDM += String.format("%02d", minutesLat);
    return latDM;
  }

  public static String convertLon(double lon) {
    String lonDM = "";
    if (lon < 0) {
      lonDM = "W";
      lon = Math.abs(lon);
    } else {
      lonDM = "E";
    }
    int degreesLon = (int) Math.floor(lon);

    double fracPartLon = lon - degreesLon;
    int minutesLon = (int) Math.round(fracPartLon * 60.0);
    if (minutesLon==60) {
      degreesLon++;
      minutesLon=0;
    }
    lonDM += String.format("%03d", degreesLon);
    lonDM += String.format("%02d", minutesLon);
    return lonDM;
  }

  public static String pointToDMSString(LngLatAlt lnglat) {
    double lon = lnglat.getLongitude();
    double lat = lnglat.getLatitude();

    return convertLat(lat) + " " + convertLon(lon);
  }

  private static String pointToDMSString(Coordinate coord) {
    double lon = coord.getOrdinate(Coordinate.X);
    double lat = coord.getOrdinate(Coordinate.Y);

    return convertLat(lat) + " " + convertLon(lon);
  }

  private static String latlonToDMS(Coordinate[] coords) {
    Arrays.stream(coords);
    return Arrays
      .stream(coords)
      .map(coord -> pointToDMSString(coord))
      .collect(Collectors.joining(" - "));
  }

  private static String latlonToDMS(List<LngLatAlt> coords) {
    return coords
      .stream()
      .map(lnglat -> pointToDMSString(lnglat))
      .collect(Collectors.joining(" - "));
  }

  public static GeoJsonObject findStartGeometry(GeoJsonObject geojson) {
    FeatureCollection fc = (FeatureCollection) geojson;
    for (Feature f : fc.getFeatures()) {
      if (
        (f.getProperty("featureFunction") != null) &&
        f.getProperty("featureFunction").equals(START)
      ) {
        return f;
      }
    }
    return null;
  }

  public static GeoJsonObject extractSingleStartGeometry(
    GeoJsonObject geojson
  ) {
    FeatureCollection fc = (FeatureCollection) geojson;
    for (Feature f : fc.getFeatures()) {
      if (
        (f.getProperty("featureFunction") != null) &&
        f.getProperty("featureFunction").equals(START)
      ) {
        for (Feature f2 : fc.getFeatures()) {
          if (
            (f2.getProperty("featureFunction") != null) &&
            f2.getProperty("featureFunction").equals(INTERSECTION) &&
            f.getId().equals(f2.getProperty("relatesTo"))
          ) {
            return f2;
          }
        }
        return f;
      }
    }
    return null;
  }

  /* Checks if poly1 is contained within poly2  */
  public static boolean isPartiallyContained(FeatureCollection poly1, FeatureCollection poly2) {
    GeometryFactory gf = new GeometryFactory(
      new PrecisionModel(PrecisionModel.FLOATING)
    );
    GeoJsonReader reader = new GeoJsonReader(gf);
    try {
      ObjectMapper om = new ObjectMapper();
      Geometry geom1 = reader.read(om.writeValueAsString(poly1.getFeatures().get(0).getGeometry()));
      Geometry geom2 = reader.read(om.writeValueAsString(poly2.getFeatures().get(0).getGeometry()));
      return geom2.intersects(geom1);
    } catch (ParseException | JsonProcessingException e) {
      log.error(e.toString());
      return false;
    }
  }

  public static String geometriesToTac(FeatureCollection poly, FeatureCollection fir, String selectionType, FeatureCollection intersection) {
    return SigmetAirmetUtils.featureToTAC(poly.getFeatures().get(0), fir.getFeatures().get(0), selectionType, intersection.getFeatures().get(0));
  }

  public static TacOrGeoGeometry handleGeometries(FeatureCollection drawn, FeatureCollection intersected, FeatureCollection fir) {
    TacOrGeoGeometryImpl.Builder tacOrGeoGeometryBuilder = TacOrGeoGeometryImpl.builder();
    if (drawn.getFeatures().get(0).getProperties().containsKey("selectionType")) {
      String tacText;
      String selectionType = drawn.getFeatures().get(0).getProperties().get("selectionType").toString();
      switch (selectionType) {
      case "fir":
        // Add TAC text for entire fir (with firType)
        tacOrGeoGeometryBuilder.setEntireArea(true);
        TacGeometryImpl.Builder tacGeometry = TacGeometryImpl.builder();
        tacGeometry.setTacContent("ENTIRE FIR"); //TODO FirType
        TacOrGeoGeometry tacOrGeoGeometryFIR = TacOrGeoGeometryImpl.of(GeoUtilsKnmi.jsonFeatureCollection2FmiAviGeometry(intersected));
        tacOrGeoGeometryBuilder.setGeoGeometry(tacOrGeoGeometryFIR.getGeoGeometry());
        tacOrGeoGeometryBuilder.setTacGeometry(tacGeometry.build());
        break;
      case "box":
      case "poly":
        TacOrGeoGeometry tacOrGeoGeometry;
        
        // Determine the intersection polygon limit
        int intersectionPolygonLimit = IntersectionLimitUtil.getIntersectionPolygonLimit();        
        // Check if intersection is multipolygon - if so, use drawn geometry
        if (intersected.getFeatures().get(0).getGeometry() instanceof MultiPolygon) {
            tacText = geometriesToTac(drawn, fir, selectionType, drawn);
            tacOrGeoGeometry = TacOrGeoGeometryImpl.of(GeoUtilsKnmi.jsonFeatureCollection2FmiAviGeometry(drawn));
        } else {
            Polygon intersectedPolygon = ((Polygon) intersected.getFeatures().get(0).getGeometry());
            // For box or if intersection has more points than the defined limit, use the drawn geometry
            if (selectionType.equals("box") || (intersectedPolygon.getCoordinates().size() > 0 
                    && intersectedPolygon.getCoordinates().get(0).size() > intersectionPolygonLimit)) {
                tacText = geometriesToTac(drawn, fir, selectionType, intersected);
                tacOrGeoGeometry = TacOrGeoGeometryImpl.of(GeoUtilsKnmi.jsonFeatureCollection2FmiAviGeometry(drawn));
            } else {
                tacText = geometriesToTac(intersected, fir, selectionType, intersected);
                tacOrGeoGeometry = TacOrGeoGeometryImpl.of(GeoUtilsKnmi.jsonFeatureCollection2FmiAviGeometry(intersected));
            }
        }
        tacGeometry = TacGeometryImpl.builder();
        tacGeometry.setTacContent(tacText);
        tacOrGeoGeometryBuilder.setGeoGeometry(tacOrGeoGeometry.getGeoGeometry());
        tacOrGeoGeometryBuilder.setTacGeometry(tacGeometry.build());
        break;
      case "point":
      case "circle":
        TacOrGeoGeometry tacOrGeoGeometryPoint = TacOrGeoGeometryImpl.of(GeoUtilsKnmi.jsonFeatureCollection2FmiAviGeometry(intersected));
        tacOrGeoGeometryBuilder.setGeoGeometry(tacOrGeoGeometryPoint.getGeoGeometry());
        break;
        default:
      }
    }
    return tacOrGeoGeometryBuilder.build();
  }

  public static String featureToTAC(Feature f, Feature FIR, String selectionType, Feature intersection) {
    List<LngLatAlt> coords;
    /* Empty text if feature or geometry is missing */
    if (
      (f == null) ||
      (
        (f.getGeometry() == null) &&
        (!f.getProperty("selectionType").toString().toLowerCase().equals("fir") || !selectionType.toLowerCase().equals("fir"))
      )
    ) return "";
    // Obtain selection type
    String usedSelectionType = "";
    if(selectionType.equals("")){
      usedSelectionType = f.getProperty("selectionType").toString().toLowerCase();
    }else{
      usedSelectionType = selectionType;
    }
    switch (usedSelectionType) {
      case "poly":
        // This assumes that one feature contains one set of coordinates
        /* The points of a polygon should be provided in a counterclockwise order, and the end point should
        be a repeat of the start point. */
        Polygon poly = ((Polygon) (f.getGeometry()));
        if (poly.getCoordinates().size() > 0) {
          coords = poly.getCoordinates().get(0);
          Collections.reverse(coords);
          return "WI " + latlonToDMS(coords);
        } else {
          return "NOCOORDS"; //TODO: Check what to do in case there are no coords
        }
      case "fir":
        return "ENTIRE FIR";
      case "point":
        Point p = (Point) f.getGeometry();
        return pointToDMSString(p.getCoordinates());
      case "box":
        // A box is drawn which can mean multiple things whether how many intersections
        // there are.
        // If one line segment intersects, the phenomenon happens in the area opposite
        // of the line intersection
        // e.g. if the south border of the box intersects, the phenomenon happens north
        // of this line.
        // If there are multiple intersections -- we assume two currently -- the
        // phenomenon happens in the quadrant
        // opposite of the intersection lines.
        // E.g. the south and west border of the box intersect, the phenomenon happens
        // north of the south intersection line and east of the west intersection line
        GeometryFactory gf = new GeometryFactory(
          new PrecisionModel(PrecisionModel.FLOATING)
        );
        GeoJsonReader reader = new GeoJsonReader(gf);

        if (FIR == null || FIR.getGeometry() == null) {
          log.warn("FIR is null!!");
          return "";
        }
        try {
          ObjectMapper om = new ObjectMapper();
          String FIRs = om.writeValueAsString(FIR.getGeometry()); // FIR as String

          try {
            Geometry drawnGeometry = reader.read(
              om.writeValueAsString(f.getGeometry())
            );

            Geometry geom_fir = reader.read(FIRs);

            // Sort box's coordinates
            Envelope env = drawnGeometry.getEnvelopeInternal();
            double minX = env.getMinX();
            double maxX = env.getMaxX();
            double minY = env.getMinY();
            double maxY = env.getMaxY();

            if ((minX == maxX) || (minY == maxY)) return " POINT "; // Box is one point!!

            // Find intersections with box's sides
            CoordinateArraySequenceFactory caf = CoordinateArraySequenceFactory.instance();
            boolean[] boxSidesIntersecting = new boolean[4];
            int boxSidesIntersectingCount = 0;

            // Sort the rectangle points counterclockwise, starting at lower left
            Coordinate[] drawnCoords = new Coordinate[5];
            for (int i = 0; i < 4; i++) {
              if (drawnGeometry.getCoordinates()[i].x == minX) {
                if (drawnGeometry.getCoordinates()[i].y == minY) {
                  drawnCoords[0] = drawnGeometry.getCoordinates()[i];
                } else {
                  drawnCoords[3] = drawnGeometry.getCoordinates()[i];
                }
              } else {
                if (drawnGeometry.getCoordinates()[i].y == minY) {
                  drawnCoords[1] = drawnGeometry.getCoordinates()[i];
                } else {
                  drawnCoords[2] = drawnGeometry.getCoordinates()[i];
                }
              }
            }
            drawnCoords[4] = drawnCoords[0]; // Copy first point to last
            log.debug(
              "drawnCoords: " +
              drawnCoords[0] +
              " " +
              drawnCoords[1] +
              " " +
              drawnCoords[2] +
              " " +
              drawnCoords[3] +
              " " +
              drawnCoords[4]
            );

            for (int i = 0; i < 4; i++) {
              LineString side = new LineString(
                caf.create(Arrays.copyOfRange(drawnCoords, i, i + 2)),
                gf
              );
              if (geom_fir == null) return " ERR (geom_fir) ";
              if (side.intersects(geom_fir)) {
                boxSidesIntersecting[i] = true;
                boxSidesIntersectingCount++;
              } else {
                boxSidesIntersecting[i] = false;
              }
            }

            log.debug(
              "Intersecting box on " + boxSidesIntersectingCount + " sides"
            );
            if (boxSidesIntersectingCount == 1) {
              if (boxSidesIntersecting[0]) {
                // N of
                return String.format("N OF %s", convertLat(minY));
              } else if (boxSidesIntersecting[1]) {
                // W of
                return String.format("W OF %s", convertLon(maxX));
              } else if (boxSidesIntersecting[2]) {
                // S of
                return String.format("S OF %s", convertLat(maxY));
              } else if (boxSidesIntersecting[3]) {
                // E of
                return String.format("E OF %s", convertLon(minX));
              }
            } else if (boxSidesIntersectingCount == 2) {
              if (boxSidesIntersecting[0] && boxSidesIntersecting[1]) {
                // N of and W of
                return String.format(
                  "N OF %s AND W OF %s",
                  convertLat(minY),
                  convertLon(maxX)
                );
              } else if (boxSidesIntersecting[1] && boxSidesIntersecting[2]) {
                // S of and W of
                return String.format(
                  "S OF %s AND W OF %s",
                  convertLat(maxY),
                  convertLon(maxX)
                );
              } else if (boxSidesIntersecting[2] && boxSidesIntersecting[3]) {
                // S of and E of
                return String.format(
                  "S OF %s AND E OF %s",
                  convertLat(maxY),
                  convertLon(minX)
                );
              } else if (boxSidesIntersecting[3] && boxSidesIntersecting[0]) {
                // N of and E of
                return String.format(
                  "N OF %s AND E OF %s",
                  convertLat(minY),
                  convertLon(minX)
                );
              } else if (boxSidesIntersecting[0] && boxSidesIntersecting[2]) {
                // N of and S of
                return String.format(
                  "N OF %s AND S OF %s",
                  convertLat(minY),
                  convertLat(maxY)
                );
              } else if (boxSidesIntersecting[1] && boxSidesIntersecting[3]) {
                // E of and W of
                return String.format(
                  "E OF %s AND W OF %s",
                  convertLon(minX),
                  convertLon(maxX)
                );
              }
            }

            // Intersect the box with the FIR, use passed intersection if available
            Geometry boxIntersection;
            if(intersection == null){
              boxIntersection = drawnGeometry.intersection(
                geom_fir
              );
            }else{
              boxIntersection = reader.read(
                om.writeValueAsString(intersection.getGeometry())
              );
            }
            if (boxIntersection.equalsTopo(geom_fir)) {
              return "ENTIRE FIR";
            }

            // This assumes that one feature contains one set of coordinates
            /* The points of the box should be provided in a counterclockwise order, and the end point should
            be a repeat of the start point. */
            Coordinate[] intersected = boxIntersection.getCoordinates();
            
            // Determine the intersection polygon limit
            int intersectionPolygonLimit = IntersectionLimitUtil.getIntersectionPolygonLimit();

            // Extract the number of intersection points
            int intersectedPointsCount = intersected.length > 0 ? intersected.length : 0;

            // If more than intersection points limit, use the drawn geometry
            if (intersectedPointsCount > intersectionPolygonLimit) {
              log.warn("More than " + intersectionPolygonLimit + " points in intersection!");
              Coordinate[] drawn = drawnGeometry.getCoordinates();
              List<Coordinate> drawnList = Arrays.asList(drawn);
              Collections.reverse(drawnList);
              return "WI " + latlonToDMS(drawnList.stream().toArray(Coordinate[]::new));
            }

            // Else use the intersection geometry
            List<Coordinate> intersectedList = Arrays.asList(intersected);
            Collections.reverse(intersectedList);
            return "WI " + latlonToDMS( intersectedList.stream().toArray(Coordinate[]::new));
          } catch (ParseException pe) {
            // log.error(pe.getMessage());
          }
        } catch (JsonProcessingException e) {
          log.error(e.getMessage());
        }
        return " ERR ";
      default:
        return "";
    }
  }
}
