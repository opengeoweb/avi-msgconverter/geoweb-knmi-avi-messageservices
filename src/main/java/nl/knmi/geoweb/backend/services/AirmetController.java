package nl.knmi.geoweb.backend.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.geojson.FeatureCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fi.fmi.avi.converter.AviMessageConverter;
import fi.fmi.avi.converter.ConversionHints;
import fi.fmi.avi.converter.ConversionIssue;
import fi.fmi.avi.converter.ConversionResult;
import fi.fmi.avi.converter.ConversionSpecification;
import fi.fmi.avi.converter.iwxxm.conf.IWXXMConverter;
import fi.fmi.avi.converter.tac.conf.TACConverter;
import fi.fmi.avi.model.sigmet.AIRMET;
import lombok.extern.slf4j.Slf4j;
import nl.knmi.geoweb.backend.ApplicationConfig;
import nl.knmi.geoweb.backend.aviation.FIRStore;
import nl.knmi.geoweb.backend.converters.AirmetConverters.GeoWebAirmetToFMIAirmet;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.GeoWebAirmet;

@Slf4j
@RestController
@CrossOrigin
@Import(ApplicationConfig.class)
public class AirmetController {
  @Autowired
  @Qualifier("geowebAviMessageConverter")
  private AviMessageConverter geowebAviMessageConverter;

  @Autowired
  private FIRStore firStore;

  @Autowired
  @Qualifier("airmetObjectMapper")
  private ObjectMapper airmetObjectMapper;

  @Autowired
  @Qualifier("geowebAirmetObjectMapper")
  private ObjectMapper geowebAirmetObjectMapper;

  public ConversionHints getTokenizerParsingHints() {
    ConversionHints hints = new ConversionHints();
    hints.put(ConversionHints.KEY_VALIDTIME_FORMAT,
    ConversionHints.VALUE_VALIDTIME_FORMAT_PREFER_LONG);
    hints.put(ConversionHints.KEY_COORDINATE_MINUTES, ConversionHints.VALUE_COORDINATE_MINUTES_INCLUDE_ZERO);
  return hints;
}

  @RequestMapping(
    value = "/getairmettac",
    method = RequestMethod.POST,
    produces = { MediaType.TEXT_PLAIN_VALUE },
    consumes = MediaType.APPLICATION_JSON_VALUE
  )
  public ResponseEntity<String> getAirmetTac(@RequestBody JsonNode body) {
    if (firStore == null) {
      return ResponseEntity.ok("Error FIR store is null");
    }
    String geowebAirmetAsString = body.toString();
    try {
      GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
        geowebAirmetAsString,
        GeoWebAirmet.class
      );

      if (geowebAirmet.getFirGeometry()==null) {
        // Add FIR from firStore
        FeatureCollection collection = new FeatureCollection();
        collection.add(firStore.lookup(geowebAirmet.getFirName(), true));
        geowebAirmet.setFirGeometry(collection);
      }

      /* Convert the GeoWeb AIRMET Pojo to the FMI AIRMET Pojo */
      ConversionResult<AIRMET> conversionResultfmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);

      /* If GeoWeb JSON to FMI Model conversion fails, give at least a hint why */
      if (ConversionResult.Status.SUCCESS != conversionResultfmiAirmet.getStatus()) {
        String errors = "Unable to translate GeoWeb JSON to FMI Model\n";
        for (ConversionIssue issue:conversionResultfmiAirmet.getConversionIssues()) {
          errors += issue + "\n";
          log.warn(issue.toString());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
      }


      AIRMET fmiAirmet = conversionResultfmiAirmet.getConvertedMessage().get();
      /* Now convert to TAC */
      final ConversionSpecification<AIRMET, String> airmetToTacSpec = TACConverter.AIRMET_POJO_TO_TAC;
      final ConversionResult<String> resultAsTac =
          (ConversionResult<String>)geowebAviMessageConverter
            .convertMessage(fmiAirmet, airmetToTacSpec, getTokenizerParsingHints());

      /* Assure TAC conversion is successful */
      if (ConversionResult.Status.SUCCESS != resultAsTac.getStatus()) {
        String errors = "Unable to translate FMI model to TAC\n";
        for (ConversionIssue issue:resultAsTac.getConversionIssues()) {
          errors += issue + "\n";
          log.warn(issue.toString());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
      } else {
        return ResponseEntity.ok(resultAsTac.getConvertedMessage().get().replaceAll("\\r", ""));
      }
    } catch (JsonMappingException e) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    } catch (JsonProcessingException e) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }
  }

  public ResponseEntity<String> getAirmetIWXXM(String geowebAirmetAsString, ConversionSpecification<AIRMET, String> airmetToIWXXMSpec) {
    if (firStore == null) {
      return ResponseEntity.ok("Error FIR store is null");
    }
    try {
      GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
        geowebAirmetAsString,
        GeoWebAirmet.class
      );
      if (geowebAirmet.getFirGeometry()==null) {
        // Add FIR from firStore
        FeatureCollection collection = new FeatureCollection();
        collection.add(firStore.lookup(geowebAirmet.getFirName(), true));
        geowebAirmet.setFirGeometry(collection);
      }

      /* Convert the GeoWeb Airmet Pojo to the FMI Airmet Pojo */
      ConversionResult<AIRMET> conversionResultfmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);

      /* If GeoWeb JSON to FMI Model conversion fails, give at least a hint why */
      if (ConversionResult.Status.SUCCESS != conversionResultfmiAirmet.getStatus()) {
        String errors = "Unable to translate GeoWeb JSON to FMI Model\n";
        for (ConversionIssue issue:conversionResultfmiAirmet.getConversionIssues()) {
          errors += issue + "\n";
          log.warn(issue.toString());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
      }


      AIRMET fmiAirmet = conversionResultfmiAirmet.getConvertedMessage().get();
      /* Now convert to IWXXM */
      final ConversionResult<String> resultAsIWXXMString =
          (ConversionResult<String>)geowebAviMessageConverter
            .convertMessage(fmiAirmet, airmetToIWXXMSpec, getTokenizerParsingHints());

      /* Ensure IWXXM conversion is successful */
      if (ConversionResult.Status.SUCCESS != resultAsIWXXMString.getStatus()) {
        String errors = "Unable to translate FMI model to IWXXM\n";
        for (ConversionIssue issue:resultAsIWXXMString.getConversionIssues()) {
          errors += issue + "\n";
          log.warn(issue.toString());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
      } else {
        return ResponseEntity.ok(resultAsIWXXMString.getConvertedMessage().get().replaceAll("\\r", ""));
      }
    } catch (JsonMappingException e) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    } catch (JsonProcessingException e) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }
  }

  @RequestMapping(
    value = "/getairmetiwxxm21",
    method = RequestMethod.POST,
    produces = { MediaType.APPLICATION_XML_VALUE },
    consumes = MediaType.APPLICATION_JSON_VALUE
  )

  public ResponseEntity<String> getAirmetIWXXM2_1(@RequestBody JsonNode body) {
    return getAirmetIWXXM(body.toString(), IWXXMConverter.AIRMET_POJO_TO_IWXXM21_STRING);
  }

  @RequestMapping(
    value = "/getairmetiwxxm30",
    method = RequestMethod.POST,
    produces = { MediaType.APPLICATION_XML_VALUE },
    consumes = MediaType.APPLICATION_JSON_VALUE
  )

  public ResponseEntity<String> getAirmetIWXXM3_0(@RequestBody JsonNode body) {
    return getAirmetIWXXM(body.toString(), IWXXMConverter.AIRMET_POJO_TO_IWXXM30_STRING);
  }

@RequestMapping(
    value = "/getairmetiwxxm2023_1",
    method = RequestMethod.POST,
    produces = { MediaType.APPLICATION_XML_VALUE },
    consumes = MediaType.APPLICATION_JSON_VALUE
  )

  public ResponseEntity<String> getAirmetIWXXM2023_1(@RequestBody JsonNode body) {
    return getAirmetIWXXM(body.toString(), IWXXMConverter.AIRMET_POJO_TO_IWXXM2023_1_STRING);
  }

}
