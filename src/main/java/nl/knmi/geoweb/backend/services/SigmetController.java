package nl.knmi.geoweb.backend.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;

import org.geojson.FeatureCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fi.fmi.avi.converter.AviMessageConverter;
import fi.fmi.avi.converter.ConversionHints;
import fi.fmi.avi.converter.ConversionIssue;
import fi.fmi.avi.converter.ConversionResult;
import fi.fmi.avi.converter.ConversionSpecification;
import fi.fmi.avi.converter.iwxxm.conf.IWXXMConverter;
import fi.fmi.avi.converter.tac.conf.TACConverter;
import fi.fmi.avi.model.sigmet.SIGMET;
import lombok.extern.slf4j.Slf4j;
import nl.knmi.geoweb.backend.ApplicationConfig;
import nl.knmi.geoweb.backend.aviation.FIRStore;
import nl.knmi.geoweb.backend.converters.SigmetConverters.GeoWebSigmetToFMISigmet;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.GeoWebSigmet;

@Slf4j
@RestController
@CrossOrigin
@Import(ApplicationConfig.class)
public class SigmetController {

  @Autowired
  @Qualifier("sigmetObjectMapper")
  private ObjectMapper sigmetObjectMapper;

  @Autowired
  @Qualifier("geowebAviMessageConverter")
  private AviMessageConverter geowebAviMessageConverter;

  @Autowired
  private FIRStore firStore;

  @Autowired
  @Qualifier("geowebSigmetObjectMapper")
  private ObjectMapper geowebSigmetObjectMapper;

  @RequestMapping(value = "/", method = RequestMethod.GET)
  public String index() {
    return "Greetings from Spring SigmetController!!! Please post a Sigmet JSON with contentype application/json to this service, and you will be presented a TAC !\n";
  }

  @RequestMapping(value = "/healthcheck", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
  public Map<String, String> healthcheck() {
    Map<String, String> response = new HashMap<>();
    response.put("status", "OK");
    response.put("service", "avi-messageservices");
    return response;
  }

  public ConversionHints getTokenizerParsingHints() {
      ConversionHints hints = new ConversionHints();
      hints.put(ConversionHints.KEY_VALIDTIME_FORMAT,
      ConversionHints.VALUE_VALIDTIME_FORMAT_PREFER_LONG);
      hints.put(ConversionHints.KEY_COORDINATE_MINUTES, ConversionHints.VALUE_COORDINATE_MINUTES_INCLUDE_ZERO);
    return hints;
  }

  @RequestMapping(
    value = "/getsigmettac",
    method = RequestMethod.POST,
    produces = { MediaType.TEXT_PLAIN_VALUE },
    consumes = MediaType.APPLICATION_JSON_VALUE
  )
  public ResponseEntity<String> getSigmetTac2(@RequestBody JsonNode body) {
    if (firStore == null) {
      return ResponseEntity.ok("Error FIR store is null");
    }
    String geowebSigmetAsString = body.toString();
    try {
      GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
        geowebSigmetAsString,
        GeoWebSigmet.class
      );
      if (geowebSigmet.getFirGeometry()==null) {
        // Add FIR from firStore
        FeatureCollection collection = new FeatureCollection();
        collection.add(firStore.lookup(geowebSigmet.getFirName(), true));
        geowebSigmet.setFirGeometry(collection);
      }

      /* Convert the GeoWeb SIGMET Pojo to the FMI SIGMET Pojo */
      ConversionResult<SIGMET> conversionResultfmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);

      /* If GeoWeb JSON to FMI Model conversion fails, give at least a hint why */
      if (ConversionResult.Status.SUCCESS != conversionResultfmiSigmet.getStatus()) {
        String errors = "Unable to translate GeoWeb JSON to FMI Model\n";
        for (ConversionIssue issue:conversionResultfmiSigmet.getConversionIssues()) {
          errors += issue + "\n";
          log.warn(issue.toString());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
      }


      SIGMET fmiSigmet = conversionResultfmiSigmet.getConvertedMessage().get();
      /* Now convert to TAC */
      final ConversionSpecification<SIGMET, String> sigmetToTacSpec = TACConverter.SIGMET_POJO_TO_TAC;
      final ConversionResult<String> resultAsTac =
          (ConversionResult<String>)geowebAviMessageConverter
            .convertMessage(fmiSigmet, sigmetToTacSpec, getTokenizerParsingHints());

      /* Ensure TAC conversion is successful */
      if (ConversionResult.Status.SUCCESS != resultAsTac.getStatus()) {
        String errors = "Unable to translate FMI model to TAC\n";
        for (ConversionIssue issue:resultAsTac.getConversionIssues()) {
          errors += issue + "\n";
          log.warn(issue.toString());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
      } else {
        return ResponseEntity.ok(resultAsTac.getConvertedMessage().get().replaceAll("\\r", ""));
      }
    } catch (JsonMappingException e) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    } catch (JsonProcessingException e) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }
  }

  public ResponseEntity<String> getSigmetIWXXM(String geowebSigmetAsString, ConversionSpecification<SIGMET, String> sigmetToIWXXMSpec) {
    if (firStore == null) {
      return ResponseEntity.ok("Error FIR store is null");
    }
    try {
      GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
        geowebSigmetAsString,
        GeoWebSigmet.class
      );
      if (geowebSigmet.getFirGeometry()==null) {
        // Add FIR from firStore
        FeatureCollection collection = new FeatureCollection();
        collection.add(firStore.lookup(geowebSigmet.getFirName(), true));
        geowebSigmet.setFirGeometry(collection);
      }

      /* Convert the GeoWeb SIGMET Pojo to the FMI SIGMET Pojo */
      ConversionResult<SIGMET> conversionResultfmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);

      /* If GeoWeb JSON to FMI Model conversion fails, give at least a hint why */
      if (ConversionResult.Status.SUCCESS != conversionResultfmiSigmet.getStatus()) {
        String errors = "Unable to translate GeoWeb JSON to FMI Model\n";
        for (ConversionIssue issue:conversionResultfmiSigmet.getConversionIssues()) {
          errors += issue + "\n";
          log.warn(issue.toString());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
      }


      SIGMET fmiSigmet = conversionResultfmiSigmet.getConvertedMessage().get();
      /* Now convert to IWXXM */
      final ConversionResult<String> resultAsIWXXMString =
          (ConversionResult<String>)geowebAviMessageConverter
            .convertMessage(fmiSigmet, sigmetToIWXXMSpec, getTokenizerParsingHints());

      /* Ensure IWXXM conversion is successful */
      if (ConversionResult.Status.SUCCESS != resultAsIWXXMString.getStatus()) {
        String errors = "Unable to translate FMI model to IWXXM\n";
        for (ConversionIssue issue:resultAsIWXXMString.getConversionIssues()) {
          errors += issue + "\n";
          log.warn(issue.toString());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
      } else {
        return ResponseEntity.ok(resultAsIWXXMString.getConvertedMessage().get().replaceAll("\\r", ""));
      }
    } catch (JsonMappingException e) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    } catch (JsonProcessingException e) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }
  }

  @RequestMapping(
    value = "/getsigmetiwxxm21",
    method = RequestMethod.POST,
    produces = { MediaType.APPLICATION_XML_VALUE },
    consumes = MediaType.APPLICATION_JSON_VALUE
  )
  public ResponseEntity<String> getSigmetIWXXM2_1(@RequestBody JsonNode body) {
    return getSigmetIWXXM(body.toString(), IWXXMConverter.SIGMET_POJO_TO_IWXXM21_STRING);
  }

  @RequestMapping(
    value = "/getsigmetiwxxm30",
    method = RequestMethod.POST,
    produces = { MediaType.APPLICATION_XML_VALUE },
    consumes = MediaType.APPLICATION_JSON_VALUE
  )
  public ResponseEntity<String> getSigmetIWXXM3_0(@RequestBody JsonNode body) {
    return getSigmetIWXXM(body.toString(), IWXXMConverter.SIGMET_POJO_TO_IWXXM30_STRING);
  }

  @RequestMapping(
    value = "/getsigmetiwxxm2023_1",
    method = RequestMethod.POST,
    produces = { MediaType.APPLICATION_XML_VALUE },
    consumes = MediaType.APPLICATION_JSON_VALUE
  )
  public ResponseEntity<String> getSigmetIWXXM2023_1(@RequestBody JsonNode body) {
    return getSigmetIWXXM(body.toString(), IWXXMConverter.SIGMET_POJO_TO_IWXXM2023_1_STRING);
  }

}
