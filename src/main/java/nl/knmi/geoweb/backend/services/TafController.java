package nl.knmi.geoweb.backend.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fi.fmi.avi.converter.AviMessageConverter;
import fi.fmi.avi.converter.ConversionHints;
import fi.fmi.avi.converter.ConversionIssue;
import fi.fmi.avi.converter.ConversionResult;
import fi.fmi.avi.converter.ConversionSpecification;
import fi.fmi.avi.converter.iwxxm.conf.IWXXMConverter;
import fi.fmi.avi.converter.tac.conf.TACConverter;
import fi.fmi.avi.converter.tac.lexer.LexemeSequence;
import fi.fmi.avi.converter.tac.lexer.SerializingException;
import fi.fmi.avi.converter.tac.taf.TAFTACSerializer;
import fi.fmi.avi.model.immutable.AerodromeImpl;
import fi.fmi.avi.model.immutable.CoordinateReferenceSystemImpl;
import fi.fmi.avi.model.immutable.ElevatedPointImpl;
import fi.fmi.avi.model.taf.TAF;
import fi.fmi.avi.model.taf.immutable.TAFImpl;
import fi.fmi.avi.model.taf.immutable.TAFReferenceImpl;
import lombok.extern.slf4j.Slf4j;
import nl.knmi.geoweb.backend.ApplicationConfig;
import nl.knmi.geoweb.backend.aviation.AirportInfo;
import nl.knmi.geoweb.backend.converters.TAFConverters.GeoWebTAFJsonToGeoWebTaf;
import nl.knmi.geoweb.backend.converters.TAFConverters.GeoWebTAFToFMITAF;
import nl.knmi.geoweb.backend.product.GeoWebTAF.GeoWebTAF;
import nl.knmi.geoweb.backend.product.GeoWebTAF.adverse.Tac;
import nl.knmi.geoweb.backend.product.GeoWebTAF.adverse.TacResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@CrossOrigin
@Import(ApplicationConfig.class)
public class TafController {

  @Autowired
  @Qualifier("tafObjectMapper")
  private ObjectMapper tafObjectMapper;

  @Autowired
  @Qualifier("geowebAviMessageConverter")
  private AviMessageConverter geowebAviMessageConverter;

  @Autowired
  @Qualifier("tafTACSerializer")
  private TAFTACSerializer tafTACSerializer;

  @Autowired
  private nl.knmi.geoweb.backend.aviation.AirportStore airportStore;

  @Autowired
  @Qualifier("geowebTafObjectMapper")
  private ObjectMapper geowebTafObjectMapper;

  public ConversionHints getTokenizerParsingHints() {
    Map<ConversionHints.Key,Object> hints = new HashMap<>();
    hints.put(ConversionHints.KEY_VALIDTIME_FORMAT,
    ConversionHints.VALUE_VALIDTIME_FORMAT_PREFER_LONG);
    hints.put(ConversionHints.KEY_TAF_REFERENCE_POLICY,
    ConversionHints.VALUE_TAF_REFERENCE_POLICY_USE_OWN_VALID_TIME_ONLY);

    return new ConversionHints(hints);
  }

  @RequestMapping(
    value = "/gettaftac",
    method = RequestMethod.POST,
    produces = { MediaType.TEXT_PLAIN_VALUE, MediaType.ALL_VALUE },
    consumes = MediaType.APPLICATION_JSON_VALUE
  )
  public ResponseEntity<String> getText(@RequestBody JsonNode body) {
    if (airportStore == null) {
      log.warn("Error Airport store is null");
      return ResponseEntity.badRequest().body("Error Airport store is null");
    }
    try {
      ConversionResult<TAF> conversionResultfmiTaf = convertToTaf(body.toString());

      /* If GeoWeb JSON to FMI Model conversion fails, give at least a hint why */
      if (ConversionResult.Status.SUCCESS != conversionResultfmiTaf.getStatus()) {
        String errors = "Unable to translate GeoWeb JSON to FMI Model\n";
        for (ConversionIssue issue:conversionResultfmiTaf.getConversionIssues()) {
          errors += issue + "\n";
        }
        log.info(errors);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
      }

      TAF fmiTaf = conversionResultfmiTaf.getConvertedMessage().get();

      /* Now convert to TAC */
      final ConversionSpecification<TAF, String> tafToTacSpec = TACConverter.TAF_POJO_TO_TAC;
      final ConversionResult<String> resultAsTac =
          (ConversionResult<String>)geowebAviMessageConverter
            .convertMessage(fmiTaf, tafToTacSpec, getTokenizerParsingHints());

      /* Assure TAC conversion is successful */
      if (ConversionResult.Status.SUCCESS != resultAsTac.getStatus()) {
        String errors = "Unable to translate FMI model to TAC\n";
        for (ConversionIssue issue:resultAsTac.getConversionIssues()) {
          errors += issue + "\n";
        }
        log.info(errors);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
      } else {
        return ResponseEntity.ok(resultAsTac.getConvertedMessage().get().replaceAll("\\r", ""));
      }
    } catch (JsonMappingException e) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    } catch (JsonProcessingException e) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }
  }

  @RequestMapping(
    value = "/gettaftac",
    method = RequestMethod.POST,
    produces = MediaType.APPLICATION_JSON_VALUE,
    consumes = MediaType.APPLICATION_JSON_VALUE
  )
  public ResponseEntity<TacResult> getJson(@RequestBody JsonNode body) {
    TacResult tacResult = new TacResult();
    if (airportStore == null) {
      tacResult.addError("Error Airport store is null");
      return ResponseEntity.badRequest().body(tacResult);
    }

    ConversionResult<TAF> conversionResultfmiTaf;
    try {
      conversionResultfmiTaf = convertToTaf(body.toString());
    } catch (JsonProcessingException e) {
      tacResult.addError(e.getMessage());
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(tacResult);
    }

    /* If GeoWeb JSON to FMI Model conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != conversionResultfmiTaf.getStatus()) {
      tacResult.addError("Unable to translate GeoWeb JSON to FMI Model");
      for (ConversionIssue issue : conversionResultfmiTaf.getConversionIssues()) {
        tacResult.addError(issue.toString());
        log.debug(issue.getMessage());
      }
      log.info(String.join("\n", tacResult.errors));
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(tacResult);
    }

    TAF fmiTaf = conversionResultfmiTaf.getConvertedMessage().get();

    try {
      LexemeSequence sequence = this.tafTACSerializer.tokenizeMessage(fmiTaf, getTokenizerParsingHints());
      Tac tac = new Tac(sequence);
      tacResult.setTac(tac);
    } catch (SerializingException e) {
        log.info(e.getMessage());
        tacResult.addError(e.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(tacResult);
    }

    return ResponseEntity.ok(tacResult);
  }

  private ConversionResult<TAF> convertToTaf(String geoWebTafAsString)
      throws JsonMappingException, JsonProcessingException {
    GeoWebTAF geoWebTaf = (new GeoWebTAFJsonToGeoWebTaf())
        .convert(geoWebTafAsString, geowebTafObjectMapper);

    /* Convert the GeoWeb TAF Pojo to the FMI TAF Pojo */
    return (new GeoWebTAFToFMITAF()).convert(geoWebTaf, geowebTafObjectMapper, getTokenizerParsingHints());
  }

  private TAF fixCancelTafForIwxxm30(TAF taf) {
    TAFImpl.Builder newTafBuilder = TAFImpl.Builder.from(taf);
    if (taf.isCancelMessage()) {
      newTafBuilder.clearValidityTime();
    }
    return newTafBuilder.build();
  }

  private TAF addAirportInfo(TAF taf, AirportInfo airportInfo) {
    TAFImpl.Builder newTafBuilder = TAFImpl.Builder.from(taf);
    AerodromeImpl.Builder ad = AerodromeImpl.Builder.from(taf.getAerodrome());

    ElevatedPointImpl.Builder refPoint = ElevatedPointImpl.builder()
            .setCrs(CoordinateReferenceSystemImpl.wgs84())
            .addCoordinates(new double[] { airportInfo.getGeoLocation().getLon(),
                    airportInfo.getGeoLocation().getLat() })
            .setElevationValue(airportInfo.getFieldElevation()).setElevationUom("m");
    ad.setReferencePoint(refPoint.build());

    ad.setFieldElevationValue(airportInfo.getFieldElevation());
    ad.setLocationIndicatorICAO(airportInfo.getICAOName());
    ad.setName(airportInfo.getName());
    ad.setDesignator(airportInfo.getICAOName());
    newTafBuilder.setAerodrome(ad.build());

    if (taf.getReferredReport().isPresent()) {
      AerodromeImpl.Builder referredAd = AerodromeImpl.Builder
              .from(taf.getReferredReport().get().getAerodrome());
      referredAd.setLocationIndicatorICAO(airportInfo.getICAOName());
      referredAd.setReferencePoint(refPoint.build());
      referredAd.setDesignator(airportInfo.getICAOName());
      referredAd.setName(airportInfo.getName());
      referredAd.setFieldElevationValue(airportInfo.getFieldElevation());
      TAFReferenceImpl.Builder tafReference = TAFReferenceImpl.Builder
            .from(taf.getReferredReport().get());
      tafReference.setAerodrome(referredAd.build());
      newTafBuilder.setReferredReport(tafReference.build());

    }
    return newTafBuilder.build();
  }

  public ResponseEntity<String> getTafIWXXM(String geowebTafAsString, ConversionSpecification<TAF, String> tafToIWXXMSpec, boolean addAirportInfo) {
    if (addAirportInfo) {
      if (airportStore == null) {
        log.warn("Error Airport store is null");
        return ResponseEntity.badRequest().body("Error Airport store is null");
      }
    }
    try {
      GeoWebTAF geowebTaf = geowebTafObjectMapper.readValue(
        geowebTafAsString,
        GeoWebTAF.class
      );
      /* Convert the GeoWeb TAF Pojo to the FMI TAF Pojo */
      ConversionResult<TAF> conversionResultfmiTaf = (new GeoWebTAFToFMITAF())
          .convert(geowebTaf, geowebTafObjectMapper, getTokenizerParsingHints());

      /* If GeoWeb JSON to FMI Model conversion fails, give at least a hint why */
      if (ConversionResult.Status.SUCCESS != conversionResultfmiTaf.getStatus()) {
        String errors = "Unable to translate GeoWeb JSON to FMI Model\n";
        for (ConversionIssue issue:conversionResultfmiTaf.getConversionIssues()) {
          errors += issue + "\n";
        }
        log.info(errors.toString());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
      }

      TAF fmiTaf = conversionResultfmiTaf.getConvertedMessage().get();

      if (addAirportInfo) {
        AirportInfo airportInfo = airportStore.lookup(geowebTaf.getLocation());
        if (airportInfo==null) {
          log.warn("Unknown airport "+geowebTaf.getLocation());
          return ResponseEntity.badRequest().body("Unknown airport "+geowebTaf.getLocation());
        }
        fmiTaf = addAirportInfo(fmiTaf, airportInfo);
      } else {
        fmiTaf = fixCancelTafForIwxxm30(fmiTaf);
      }

      /* Now convert to IWXXM */
      final ConversionResult<String> resultAsIWXXMString =
          (ConversionResult<String>)geowebAviMessageConverter
            .convertMessage(fmiTaf, tafToIWXXMSpec, getTokenizerParsingHints());

      /* Ensure IWXXM conversion is successful */
      if (ConversionResult.Status.SUCCESS != resultAsIWXXMString.getStatus()) {
        String errors = "Unable to translate FMI model to IWXXM\n";
        for (ConversionIssue issue:resultAsIWXXMString.getConversionIssues()) {
          errors += issue + "\n";
          log.warn(issue.toString());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
      } else {
        return ResponseEntity.ok(resultAsIWXXMString.getConvertedMessage().get().replaceAll("\\r", ""));
      }
    } catch (JsonMappingException e) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    } catch (JsonProcessingException e) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }
  }

  @RequestMapping(
    value = "/gettafiwxxm21",
    method = RequestMethod.POST,
    produces = { MediaType.APPLICATION_XML_VALUE },
    consumes = MediaType.APPLICATION_JSON_VALUE
  )
  public ResponseEntity<String> getTafIWXXM2_1(@RequestBody JsonNode body) {
    return getTafIWXXM(body.toString(), IWXXMConverter.TAF_POJO_TO_IWXXM21_STRING, true);
  }

  @RequestMapping(
    value = "/gettafiwxxm30",
    method = RequestMethod.POST,
    produces = { MediaType.APPLICATION_XML_VALUE },
    consumes = MediaType.APPLICATION_JSON_VALUE
  )
  public ResponseEntity<String> getTafIWXXM3_0(@RequestBody JsonNode body) {
    return getTafIWXXM(body.toString(), IWXXMConverter.TAF_POJO_TO_IWXXM30_STRING, false);
  }
}
