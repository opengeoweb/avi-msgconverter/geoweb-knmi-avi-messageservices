package nl.knmi.geoweb.backend.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fi.fmi.avi.model.immutable.CircleByCenterPointImpl;
import fi.fmi.avi.model.immutable.MultiPolygonGeometryImpl;
import fi.fmi.avi.model.immutable.NumericMeasureImpl;
import fi.fmi.avi.model.immutable.PointGeometryImpl;
import fi.fmi.avi.model.immutable.PolygonGeometryImpl;
import lombok.extern.slf4j.Slf4j;

import org.geojson.Feature;
import org.geojson.FeatureCollection;
import org.geojson.LngLatAlt;
import org.geojson.Point;
import org.geojson.Polygon;


public class GeoUtilsKnmi {

	/* TODO: Implement circle */
	public static fi.fmi.avi.model.Geometry jsonFeatureCollection2FmiAviGeometry(FeatureCollection f) {
		if (f.getFeatures().size() == 1) {
			if (f.getFeatures().get(0).getGeometry() instanceof Polygon) {
				PolygonGeometryImpl.Builder builder = PolygonGeometryImpl.builder();
				Polygon polygon = (Polygon)f.getFeatures().get(0).getGeometry();
				if (polygon.getCoordinates().size()>0) {
					List<LngLatAlt> lls = ((Polygon)f.getFeatures().get(0).getGeometry()).getExteriorRing();
					List<Double> extPoints=new ArrayList<>();
					for (LngLatAlt ll: lls) {
						extPoints.add(ll.getLatitude());
						extPoints.add(ll.getLongitude());
					}
					builder.addAllExteriorRingPositions(extPoints);
					return builder.build();
				} else {
					List<Double> extPoints=new ArrayList<>();
					builder.addAllExteriorRingPositions(extPoints);
					return builder.build();
				}
			} else if (f.getFeatures().get(0).getGeometry() instanceof Point) {
                Feature pointFeature = f.getFeatures().get(0);
                if (pointFeature.getProperty("radius")==null) {
                    PointGeometryImpl.Builder builder= PointGeometryImpl.builder();
                    LngLatAlt ll = ((Point)f.getFeatures().get(0).getGeometry()).getCoordinates();
                    List<Double> pts = Arrays.asList(ll.getLatitude(), ll.getLongitude());
                    builder.addAllCoordinates(pts);
                    return builder.build();
                }
                // Property radius denotes this is a CircleByCenterPoint with radius and radiusUnit
                int radius = pointFeature.getProperty("radius");
                String radiusUnit = pointFeature.getProperty("radiusUnit");
                if (radiusUnit==null) {
                    radiusUnit="KM";
                } else {
                    if (!radiusUnit.equals("KM") && !radiusUnit.equals("NM")) {
                        radiusUnit="KM";
                    }
                }
                CircleByCenterPointImpl.Builder builder = CircleByCenterPointImpl.builder();
                LngLatAlt ll = ((Point)f.getFeatures().get(0).getGeometry()).getCoordinates();
                List<Double> pts = Arrays.asList(ll.getLatitude(), ll.getLongitude());
                builder.addAllCenterPointCoordinates(pts);
                builder.setRadius(NumericMeasureImpl.of(radius, radiusUnit));
                return builder.build();
			}
			return null;
		} else {
			MultiPolygonGeometryImpl.Builder builder = MultiPolygonGeometryImpl.builder();
			for (Feature feat: f.getFeatures()) {
				if (feat.getGeometry() instanceof Polygon) {
					List<LngLatAlt> lls = ((Polygon)feat.getGeometry()).getExteriorRing();
					List<Double> extPoints=new ArrayList<>();
					for (LngLatAlt ll: lls) {
							extPoints.add(ll.getLatitude());
							extPoints.add(ll.getLongitude());
					}
					builder.addExteriorRingPositions(extPoints);
				}
			}
			return builder.build();
		}
	}

}
