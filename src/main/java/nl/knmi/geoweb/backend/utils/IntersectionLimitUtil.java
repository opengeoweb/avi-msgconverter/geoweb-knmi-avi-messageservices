package nl.knmi.geoweb.backend.utils;

public class IntersectionLimitUtil {
    private static final int INTERSECTION_POLYGON_LIMIT;

    static {
        String limitEnv = System.getenv("INTERSECTION_POLYGON_LIMIT");
        if (limitEnv != null && !limitEnv.isEmpty()) {
            INTERSECTION_POLYGON_LIMIT = Integer.parseInt(limitEnv);
        } else {
            INTERSECTION_POLYGON_LIMIT = 7;
        }
    }

    public static int getIntersectionPolygonLimit() {
        return INTERSECTION_POLYGON_LIMIT;
    }
}