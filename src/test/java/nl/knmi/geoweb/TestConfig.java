package nl.knmi.geoweb;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsonorg.JsonOrgModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Primary;

import fi.fmi.avi.converter.AviMessageConverter;
import fi.fmi.avi.converter.AviMessageSpecificConverter;
import fi.fmi.avi.converter.iwxxm.conf.IWXXMConverter;
import fi.fmi.avi.converter.json.TAFJSONParser;
import fi.fmi.avi.converter.json.conf.JSONConverter;
import fi.fmi.avi.converter.tac.conf.TACConverter;
import fi.fmi.avi.model.sigmet.AIRMET;
import fi.fmi.avi.model.sigmet.SIGMET;
import fi.fmi.avi.model.taf.TAF;
import fi.fmi.avi.model.taf.TAFBulletin;
import nl.knmi.geoweb.backend.aviation.FIRStore;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.om.CreateSigmetObjectMapper;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.om.CreateAirmetObjectMapper;
import nl.knmi.geoweb.backend.product.GeoWebTAF.om.CreateTAFObjectMapper;
@ComponentScan("fi.fmi.avi.converter.tac")
@ComponentScan("fi.fmi.avi.converter.iwxxm")
public class TestConfig {

  private static final String DATEFORMAT_ISO8601 = "yyyy-MM-dd'TT'HH:mm:ss'Y'";

  private static ObjectMapper objectMapper;

  public TestConfig() {
    ObjectMapper om = new ObjectMapper();
    om.registerModule(new Jdk8Module());
    om.registerModule(new JavaTimeModule());
    om.registerModule(new JsonOrgModule());
    om.setDateFormat(new SimpleDateFormat(DATEFORMAT_ISO8601));
    om.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    SimpleModule simpleModule = new SimpleModule();
    simpleModule.addSerializer(
      OffsetDateTime.class,
      new JsonSerializer<OffsetDateTime>() {
        @Override
        public void serialize(
          OffsetDateTime offsetDateTime,
          JsonGenerator jsonGenerator,
          SerializerProvider serializerProvider
        )
          throws IOException {
          String formattedDate = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(
            offsetDateTime
          );
          jsonGenerator.writeString(formattedDate);
        }
      }
    );
    om.registerModule(simpleModule);
    om.setTimeZone(TimeZone.getTimeZone("UTC"));
    om.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
    om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    om.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    om.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
    objectMapper = om;
  }

  @Bean("sigmetObjectMapper")
  public static ObjectMapper getSigmetObjectMapperBean() {
    return objectMapper;
  }

  @Bean("airmetObjectMapper")
  public static ObjectMapper getAirmetObjectMapperBean() {
    return objectMapper;
  }

  @Bean("tafObjectMapper")
  public static ObjectMapper getTafObjectMapperBean() {
    return objectMapper;
  }

  @Bean("geoWebObjectMapper")
  public static ObjectMapper getGeoWebObjectMapperBean() {
    return objectMapper;
  }

  @Bean("objectMapper")
  @Primary
  public static ObjectMapper getObjectMapperBean() {
    ObjectMapper om = new ObjectMapper();
    om.registerModule(new JavaTimeModule());
    om.setTimeZone(TimeZone.getTimeZone("UTC"));
    return om;
  }



  @Bean
  public FIRStore getFirStore() {
    FIRStore firStore = new FIRStore();
    return firStore;
  }

  @Bean("geowebSigmetObjectMapper")
  public ObjectMapper geowebSigmetObjectMapper() {
    return CreateSigmetObjectMapper.getOm();
  }

  @Bean("geowebAirmetObjectMapper")
  public ObjectMapper geowebAirmetObjectMapper() {
    return CreateAirmetObjectMapper.getOm();
  }

  @Bean("geowebTafObjectMapper")
  public ObjectMapper getGeowebTafObjectMapperBean() {
    ObjectMapper om = CreateTAFObjectMapper.getOm();
    return om;
  }

  @Autowired
  @Qualifier("tafTACSerializer")
  private AviMessageSpecificConverter<TAF, String> geowebTafTACSerializer;

  @Autowired
  private AviMessageSpecificConverter<TAFBulletin, String> tafBulletinTACSerializer;

  @Autowired
  @Qualifier("sigmetTACSerializer")
  private AviMessageSpecificConverter<SIGMET, String> geowebSigmetTACSerializer;

  @Autowired
  @Qualifier("airmetTACSerializer")
  private AviMessageSpecificConverter<AIRMET, String> geowebAirmetTACSerializer;

  @Bean(name = "geowebTafJSONParser")
  public AviMessageSpecificConverter<String, TAF> geowebTafJSONParser() {
    return new TAFJSONParser();
  }

  @Autowired
  @Qualifier("geowebTafJSONParser")
  private AviMessageSpecificConverter<String, TAF> geowebTafJSONParser;

  @Autowired
  @Qualifier("sigmetIWXXMStringSerializer")
	private AviMessageSpecificConverter<SIGMET, String> sigmetIWXXMStringSerializer;

  @Autowired
  @Qualifier("airmetIWXXMStringSerializer")
	private AviMessageSpecificConverter<AIRMET, String> airmetIWXXMStringSerializer;

  @Autowired
  @Qualifier("sigmetIWXXM30StringSerializer")
	private AviMessageSpecificConverter<SIGMET, String> sigmetIWXXM30StringSerializer;

  @Autowired
  @Qualifier("airmetIWXXM30StringSerializer")
	private AviMessageSpecificConverter<AIRMET, String> airmetIWXXM30StringSerializer;

  @Autowired
  @Qualifier("sigmetIWXXM20231StringSerializer")
	private AviMessageSpecificConverter<SIGMET, String> sigmetIWXXM20231StringSerializer;

  @Autowired
  @Qualifier("airmetIWXXM20231StringSerializer")
	private AviMessageSpecificConverter<AIRMET, String> airmetIWXXM20231StringSerializer;

    @Autowired
    @Qualifier("tafIWXXMStringSerializer")
      private AviMessageSpecificConverter<TAF, String> tafIWXXMStringSerializer;

    @Autowired
    @Qualifier("tafIWXXM30StringSerializer")
      private AviMessageSpecificConverter<TAF, String> tafIWXXM30StringSerializer;

  @Bean("geowebAviMessageConverter")
  public AviMessageConverter geowebAviMessageConverter() {
    final AviMessageConverter p = new AviMessageConverter();
    p.setMessageSpecificConverter(TACConverter.TAF_POJO_TO_TAC, geowebTafTACSerializer);
    p.setMessageSpecificConverter(TACConverter.TAF_BULLETIN_POJO_TO_TAC, tafBulletinTACSerializer);
    p.setMessageSpecificConverter(TACConverter.SIGMET_POJO_TO_TAC, geowebSigmetTACSerializer);
    p.setMessageSpecificConverter(TACConverter.AIRMET_POJO_TO_TAC, geowebAirmetTACSerializer);
    p.setMessageSpecificConverter(JSONConverter.JSON_STRING_TO_TAF_POJO, geowebTafJSONParser);
    p.setMessageSpecificConverter(IWXXMConverter.SIGMET_POJO_TO_IWXXM21_STRING, sigmetIWXXMStringSerializer);
    p.setMessageSpecificConverter(IWXXMConverter.AIRMET_POJO_TO_IWXXM21_STRING, airmetIWXXMStringSerializer);
    p.setMessageSpecificConverter(IWXXMConverter.SIGMET_POJO_TO_IWXXM30_STRING, sigmetIWXXM30StringSerializer);
    p.setMessageSpecificConverter(IWXXMConverter.AIRMET_POJO_TO_IWXXM30_STRING, airmetIWXXM30StringSerializer);
    p.setMessageSpecificConverter(IWXXMConverter.SIGMET_POJO_TO_IWXXM2023_1_STRING, sigmetIWXXM20231StringSerializer);
    p.setMessageSpecificConverter(IWXXMConverter.AIRMET_POJO_TO_IWXXM2023_1_STRING, airmetIWXXM20231StringSerializer);
    p.setMessageSpecificConverter(IWXXMConverter.TAF_POJO_TO_IWXXM21_STRING, tafIWXXMStringSerializer);
    p.setMessageSpecificConverter(IWXXMConverter.TAF_POJO_TO_IWXXM30_STRING, tafIWXXM30StringSerializer);
    return p;
  }
}
