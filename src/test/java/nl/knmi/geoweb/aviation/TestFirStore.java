package nl.knmi.geoweb.aviation;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.geojson.Feature;
import org.geojson.MultiPolygon;
import org.geojson.Polygon;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import nl.knmi.geoweb.TestConfig;
import nl.knmi.geoweb.backend.aviation.FIRStore;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { TestConfig.class })
public class TestFirStore {

    @Autowired
    FIRStore firStore;

    @Test
    public void testGetFirName() {
      assertEquals("AMSTERDAM", firStore.getFirName("EHAA"));
      assertEquals(null, firStore.getFirName("YUDD"));
    }

    @Test
    public void testGetFirType() {
        assertEquals("FIR", firStore.getFirType("EHAA"));
        assertEquals(null, firStore.getFirType("YUDD"));
    }

    @Test
    public void testGetFirFeature() {
        Feature fEhaa = firStore.lookup("EHAA", false);
        assertNotNull(fEhaa);
        assertTrue(((String)fEhaa.getProperty("FIRname")).contains("AMSTERDAM"));
        assertEquals(Polygon.class, fEhaa.getGeometry().getClass());
        assertEquals(18, ((Polygon)fEhaa.getGeometry()).getCoordinates().get(0).size());

        Feature fEhaa_delegated =  firStore.lookup("EHAA", true);
        assertNotNull(fEhaa_delegated);
        assertEquals(MultiPolygon.class, fEhaa_delegated.getGeometry().getClass());
        assertEquals(18, ((MultiPolygon)fEhaa_delegated.getGeometry()).getCoordinates().get(0).get(0).size());
        assertEquals(12, ((MultiPolygon)fEhaa_delegated.getGeometry()).getCoordinates().get(1).get(0).size());

        Feature fAMSTERDAM = firStore.lookup("AMSTERDAM", false);
        assertNull(fAMSTERDAM);
        fAMSTERDAM = firStore.lookup("AMSTERDAM FIR", false);
        assertEquals("EHAA", fAMSTERDAM.getProperty("ICAOCODE"));
    }
}
