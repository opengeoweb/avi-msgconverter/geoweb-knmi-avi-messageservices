package nl.knmi.geoweb.backend.product.GeoWebAirmet;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.knmi.geoweb.backend.ApplicationConfig;
import nl.knmi.geoweb.backend.aviation.Tools;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.ChangeEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.CloudLevelInfoModeEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.CloudLevelUnitEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.LevelInfoModeEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.LevelUnitEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.MovementDirectionEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.MovementTypeEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.MovementUnitEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.ObservationOrForecastEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.PhenomenaEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.StatusEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.TypeEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.VisibilityCauseEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.VisibilityUnitEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.enums.WindUnitEnum;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.om.CreateAirmetObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Import(ApplicationConfig.class)
public class TestGeoWebAirmet {

  @Autowired
  @Qualifier("airmetObjectMapper")
  private ObjectMapper airmetObjectMapper;

  @Test
  public void readDraftCloudGeoWebAirmetAndCheckProps() throws Exception {
    ObjectMapper geowebAirmetObjectMapper = CreateAirmetObjectMapper.getOm();
    String airmetJson = Tools.readResource(
      "geowebairmets/draftcloud.json"
    );

    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );

    assertEquals(geowebAirmet.getLocationIndicatorATSR(), "EHAA");
    assertEquals(geowebAirmet.getLocationIndicatorATSU(), "EHAA");
    assertEquals(geowebAirmet.getLocationIndicatorMWO(), "EHDB");
    assertEquals(geowebAirmet.getFirName(), "AMSTERDAM FIR");
    assertEquals(geowebAirmet.getChange(), ChangeEnum.INTSF);
    assertEquals(geowebAirmet.getLevel(), null);
    assertEquals(geowebAirmet.getWindSpeed(), 0);
    assertEquals(geowebAirmet.getVisibilityCause(), null);

    assertEquals(geowebAirmet.getCloudLevel().getUnit(), CloudLevelUnitEnum.FT);
    assertEquals(geowebAirmet.getCloudLevel().getValue(), 1000);
    assertEquals(geowebAirmet.getCloudLowerLevel().getUnit(), CloudLevelUnitEnum.FT);
    assertEquals(geowebAirmet.getCloudLowerLevel().getValue(), 100);
    assertEquals(geowebAirmet.getCloudLevelInfoMode(), CloudLevelInfoModeEnum.BETW_ABV);

    assertEquals(
      geowebAirmet.getIsObservationOrForecast(),
      ObservationOrForecastEnum.OBS
    );

    assertEquals(geowebAirmet.getPhenomenon(), PhenomenaEnum.BKN_CLD);
    assertEquals(geowebAirmet.getType(), TypeEnum.TEST);
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getObservationOrForecastTime()),
      "null"
    );
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getIssueDate()),
      "\"2020-11-09T13:13:26Z\""
    );
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getValidDateStart()),
      "\"2020-11-09T13:15:00Z\""
    );
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getValidDateEnd()),
      "\"2020-11-09T14:59:29Z\""
    );
    assertEquals(
      geowebAirmet.getUuid(),
      "someuniqueidfordraft"
    );
    assertEquals(geowebAirmet.getSequence(), "3");
    assertEquals(
      geowebAirmet.getMovementType(),
      MovementTypeEnum.MOVEMENT
    );
    assertEquals(
      geowebAirmet.getMovementDirection(),
      MovementDirectionEnum.NNE
    );
    assertEquals(
      geowebAirmet.getMovementUnit(),
      MovementUnitEnum.KMH
    );
    assertEquals(
      geowebAirmet.getMovementSpeed(),
      3
    );
    assertEquals(geowebAirmet.getStatus(), StatusEnum.DRAFT);
  }


  @Test
  public void readPublishedGeoWebAirmetAndCheckProps() throws Exception {
    ObjectMapper geowebAirmetObjectMapper = CreateAirmetObjectMapper.getOm();
    String airmetJson = Tools.readResource(
      "geowebairmets/published.json"
    );

    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );

    assertEquals(geowebAirmet.getLocationIndicatorATSR(), "EHAA");
    assertEquals(geowebAirmet.getLocationIndicatorATSU(), "EHAA");
    assertEquals(geowebAirmet.getLocationIndicatorMWO(), "EHDB");
    assertEquals(geowebAirmet.getFirName(), "AMSTERDAM FIR");
    assertEquals(geowebAirmet.getChange(), ChangeEnum.WKN);
    assertEquals(geowebAirmet.getLevel().getUnit(), LevelUnitEnum.FT);
    assertEquals(geowebAirmet.getLevel().getValue(), 1000);
    assertEquals(geowebAirmet.getCloudLevelInfoMode(), null);
    assertEquals(geowebAirmet.getWindSpeed(), 0);
    assertEquals(geowebAirmet.getVisibilityCause(), null);

    assertEquals(
      geowebAirmet.getIsObservationOrForecast(),
      ObservationOrForecastEnum.FCST
    );
    assertEquals(geowebAirmet.getLevelInfoMode(), LevelInfoModeEnum.ABV);
    assertEquals(geowebAirmet.getPhenomenon(), PhenomenaEnum.ISOL_TSGR);
    assertEquals(geowebAirmet.getType(), TypeEnum.TEST);
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getObservationOrForecastTime()),
      "\"2020-11-09T13:15:00Z\""
    );
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getIssueDate()),
      "\"2020-11-09T13:13:26Z\""
    );
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getValidDateStart()),
      "\"2020-11-09T13:15:00Z\""
    );
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getValidDateEnd()),
      "\"2020-11-09T14:59:29Z\""
    );
    assertEquals(
      geowebAirmet.getUuid(),
      "someuniqueidnumber2"
    );
    assertEquals(geowebAirmet.getSequence(), "2");
    assertEquals(
      geowebAirmet.getMovementType(),
      MovementTypeEnum.STATIONARY
    );
    assertEquals(geowebAirmet.getStatus(), StatusEnum.PUBLISHED);
  }

  @Test
  public void readPublishedSurfaceVisGeoWebAirmetAndCheckProps() throws Exception {
    ObjectMapper geowebAirmetObjectMapper = CreateAirmetObjectMapper.getOm();
    String airmetJson = Tools.readResource(
      "geowebairmets/publishedsurfacevis.json"
    );

    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );

    assertEquals(geowebAirmet.getLocationIndicatorATSR(), "EHAA");
    assertEquals(geowebAirmet.getLocationIndicatorATSU(), "EHAA");
    assertEquals(geowebAirmet.getLocationIndicatorMWO(), "EHDB");
    assertEquals(geowebAirmet.getFirName(), "AMSTERDAM FIR");
    assertEquals(geowebAirmet.getChange(), ChangeEnum.INTSF);
    assertEquals(geowebAirmet.getLevel(), null);
    assertEquals(geowebAirmet.getCloudLevelInfoMode(), null);
    assertEquals(geowebAirmet.getWindSpeed(), 0);

    assertEquals(geowebAirmet.getVisibilityCause(), VisibilityCauseEnum.DZ);
    assertEquals(geowebAirmet.getVisibilityValue(), 1000);
    assertEquals(geowebAirmet.getVisibilityUnit(), VisibilityUnitEnum.M);

    assertEquals(
      geowebAirmet.getIsObservationOrForecast(),
      ObservationOrForecastEnum.OBS
    );
    assertEquals(geowebAirmet.getPhenomenon(), PhenomenaEnum.SFC_VIS);
    assertEquals(geowebAirmet.getType(), TypeEnum.TEST);
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getObservationOrForecastTime()),
      "\"2020-11-09T12:00:00Z\""
    );
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getIssueDate()),
      "\"2020-09-17T12:00:00Z\""
    );
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getValidDateStart()),
      "\"2020-11-09T13:15:00Z\""
    );
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getValidDateEnd()),
      "\"2020-11-09T14:59:29Z\""
    );
    assertEquals(
      geowebAirmet.getUuid(),
      "someuniqueidprescibedbyBE2"
    );
    assertEquals(geowebAirmet.getSequence(), "12");
    assertEquals(
      geowebAirmet.getMovementType(),
      MovementTypeEnum.STATIONARY
    );
    assertEquals(geowebAirmet.getStatus(), StatusEnum.PUBLISHED);
  }

  @Test
  public void readCancelGeoWebAirmetAndCheckProps() throws Exception {
    ObjectMapper geowebAirmetObjectMapper = CreateAirmetObjectMapper.getOm();
    String airmetJson = Tools.readResource(
      "geowebairmets/cancel.json"
    );

    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );

    assertEquals(geowebAirmet.getLocationIndicatorATSR(), "EHAA");
    assertEquals(geowebAirmet.getLocationIndicatorATSU(), "EHAA");
    assertEquals(geowebAirmet.getLocationIndicatorMWO(), "EHDB");
    assertEquals(geowebAirmet.getFirName(), "AMSTERDAM FIR");
    assertEquals(geowebAirmet.getCloudLevelInfoMode(), null);
    assertEquals(geowebAirmet.getWindSpeed(), 0);
    assertEquals(geowebAirmet.getVisibilityCause(), null);

    assertEquals(geowebAirmet.getCancelsAirmetSequenceId(), "113");
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getValidDateStartOfAirmetToCancel()),
      "\"2020-09-17T13:00:00Z\""
    );
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getValidDateEndOfAirmetToCancel()),
      "\"2020-09-17T17:00:00Z\""
    );

    assertEquals(geowebAirmet.getType(), TypeEnum.TEST);
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getIssueDate()),
      "\"2020-09-17T14:30:00Z\""
    );
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getValidDateStart()),
      "\"2020-09-17T14:30:00Z\""
    );
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getValidDateEnd()),
      "\"2020-09-17T17:00:00Z\""
    );
    assertEquals(
      geowebAirmet.getUuid(),
      "uniqueidcancel"
    );
    assertEquals(geowebAirmet.getSequence(), "4");
    assertEquals(geowebAirmet.getStatus(), StatusEnum.PUBLISHED);
  }

  @Test
  public void readCancelledSurfaceWindGeoWebAirmetAndCheckProps() throws Exception {
    ObjectMapper geowebAirmetObjectMapper = CreateAirmetObjectMapper.getOm();
    String airmetJson = Tools.readResource(
      "geowebairmets/cancelled.json"
    );

    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );

    assertEquals(geowebAirmet.getLocationIndicatorATSR(), "EHAA");
    assertEquals(geowebAirmet.getLocationIndicatorATSU(), "EHAA");
    assertEquals(geowebAirmet.getLocationIndicatorMWO(), "EHDB");
    assertEquals(geowebAirmet.getFirName(), "AMSTERDAM FIR");
    assertEquals(geowebAirmet.getChange(), ChangeEnum.WKN);
    assertEquals(geowebAirmet.getLevel(), null);
    assertEquals(geowebAirmet.getCloudLevelInfoMode(), null);

    assertEquals(geowebAirmet.getWindSpeed(), 120);
    assertEquals(geowebAirmet.getWindDirection(), 220);
    assertEquals(geowebAirmet.getWindUnit(), WindUnitEnum.KT);

    assertEquals(
      geowebAirmet.getIsObservationOrForecast(),
      ObservationOrForecastEnum.OBS
    );
    assertEquals(geowebAirmet.getPhenomenon(), PhenomenaEnum.SFC_WIND);
    assertEquals(geowebAirmet.getType(), TypeEnum.TEST);
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getObservationOrForecastTime()),
      "\"2020-09-17T12:00:00Z\""
    );
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getIssueDate()),
      "\"2020-09-17T12:00:00Z\""
    );
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getValidDateStart()),
      "\"2020-09-17T13:00:00Z\""
    );
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getValidDateEnd()),
      "\"2020-09-17T17:00:00Z\""
    );
    assertEquals(
      geowebAirmet.getUuid(),
      "uniqueid1223443"
    );
    assertEquals(geowebAirmet.getSequence(), "113");
    assertEquals(
      geowebAirmet.getMovementType(),
      MovementTypeEnum.MOVEMENT
    );
    assertEquals(
      geowebAirmet.getMovementDirection(),
      MovementDirectionEnum.NNE
    );
    assertEquals(geowebAirmet.getMovementSpeed(), 3.0, 0.001);
    assertEquals(
      geowebAirmet.getMovementUnit(),
      MovementUnitEnum.KMH
    );
    assertEquals(geowebAirmet.getStatus(), StatusEnum.CANCELLED);
  }

  @Test
  public void readExpiredGeoWebAirmetAndCheckProps() throws Exception {
    ObjectMapper geowebAirmetObjectMapper = CreateAirmetObjectMapper.getOm();
    String airmetJson = Tools.readResource(
      "geowebairmets/expired.json"
    );

    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );

    assertEquals(geowebAirmet.getLocationIndicatorATSR(), "EHAA");
    assertEquals(geowebAirmet.getLocationIndicatorATSU(), "EHAA");
    assertEquals(geowebAirmet.getLocationIndicatorMWO(), "EHDB");
    assertEquals(geowebAirmet.getFirName(), "AMSTERDAM FIR");
    assertEquals(geowebAirmet.getChange(), ChangeEnum.WKN);
    assertEquals(geowebAirmet.getLevel().getUnit(), LevelUnitEnum.FL);
    assertEquals(geowebAirmet.getLevel().getValue(), 300);
    assertEquals(geowebAirmet.getCloudLevelInfoMode(), null);
    assertEquals(geowebAirmet.getWindSpeed(), 0);
    assertEquals(geowebAirmet.getVisibilityCause(), null);

    assertEquals(
      geowebAirmet.getIsObservationOrForecast(),
      ObservationOrForecastEnum.OBS
    );
    assertEquals(geowebAirmet.getPhenomenon(), PhenomenaEnum.ISOL_CB);
    assertEquals(geowebAirmet.getType(), TypeEnum.TEST);
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getObservationOrForecastTime()),
      "\"2020-09-17T12:00:00Z\""
    );
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getIssueDate()),
      "\"2020-09-18T12:00:00Z\""
    );
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getValidDateStart()),
      "\"2020-09-18T23:00:00Z\""
    );
    assertEquals(
      CreateAirmetObjectMapper
        .getOm()
        .writeValueAsString(geowebAirmet.getValidDateEnd()),
      "\"2020-09-19T02:00:00Z\""
    );
    assertEquals(
      geowebAirmet.getUuid(),
      "9474040237904"
    );
    assertEquals(geowebAirmet.getSequence(), "1");
    assertEquals(
      geowebAirmet.getMovementType(),
      MovementTypeEnum.STATIONARY
    );
    assertEquals(geowebAirmet.getStatus(), StatusEnum.EXPIRED);
  }

}
