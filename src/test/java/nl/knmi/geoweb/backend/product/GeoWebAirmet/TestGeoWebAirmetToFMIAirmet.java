package nl.knmi.geoweb.backend.product.GeoWebAirmet;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Arrays;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import fi.fmi.avi.converter.ConversionResult;
import fi.fmi.avi.model.PolygonGeometry;
import fi.fmi.avi.model.PointGeometry;
import fi.fmi.avi.model.sigmet.AIRMET;
import nl.knmi.geoweb.backend.ApplicationConfig;
import nl.knmi.geoweb.backend.aviation.Tools;
import nl.knmi.geoweb.backend.converters.AirmetConverters.GeoWebAirmetToFMIAirmet;

@RunWith(SpringRunner.class)
@SpringBootTest
@Import(ApplicationConfig.class)
public class TestGeoWebAirmetToFMIAirmet {

  @Autowired
  @Qualifier("airmetObjectMapper")
  private ObjectMapper airmetObjectMapper;

  @Test
  public void readCancelAirgmet() throws Exception {
    /* Read json and tac, convert json to tac, results should be equal */
    String airmetJson = Tools.readResource("./geowebairmets/cancel.json");

    ObjectMapper geowebAirmetObjectMapper = airmetObjectMapper;
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
       airmetJson,
       GeoWebAirmet.class
     );
    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
    assertEquals(ConversionResult.Status.SUCCESS, fmiAirmet.getStatus());
  }

  @Test
  public void readCancelledAirmet() throws Exception {
    /* Read json and tac, convert json to tac, results should be equal */
    String airmetJson = Tools.readResource("./geowebairmets/cancelled.json");

    ObjectMapper geowebAirmetObjectMapper = airmetObjectMapper;
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
       airmetJson,
       GeoWebAirmet.class
     );
    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
    assertEquals(ConversionResult.Status.SUCCESS, fmiAirmet.getStatus());
  }

  @Test
  public void readPublishedAirmet() throws Exception {
    /* Read json and tac, convert json to tac, results should be equal */
    String airmetJson = Tools.readResource("./geowebairmets/published.json");

    ObjectMapper geowebAirmetObjectMapper = airmetObjectMapper;
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
       airmetJson,
       GeoWebAirmet.class
     );
    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
    assertEquals(ConversionResult.Status.SUCCESS, fmiAirmet.getStatus());
  }

  @Test
  public void readPublishedSurfaceVISAirmet() throws Exception {
    /* Read json and tac, convert json to tac, results should be equal */
    String airmetJson = Tools.readResource("./geowebairmets/publishedsurfacevis.json");

    ObjectMapper geowebAirmetObjectMapper = airmetObjectMapper;
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
       airmetJson,
       GeoWebAirmet.class
     );
    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
    assertEquals(ConversionResult.Status.SUCCESS, fmiAirmet.getStatus());
  }

  // Drawn:
  // [52.752303811296045, 2.381969736260152, 51.56384439662024, 0.6207187263244109, 51.636777020700315, 2.96905340623873, 52.752303811296045, 2.381969736260152]
  // N5245 E00223 - N5138 E00258 - N5134 E00037 - N5245 E00223
  // Intersection:
  // [52.274632333864965, 2.6333605219412513, 51.60946036704992, 2.0894980945086408, 51.636777, 2.969053, 52.274632333864965, 2.6333605219412513]
  // N5216 E00238 - N5138 E00258 - N5136 E00205 - N5216 E00238
  @Test
  public void read3IntersectionGeometryAirmet() throws Exception {
    /* The intersection polygon should be used in the TAC */
    String airmetJson = Tools.readResource("./geowebairmets/airmet-3-coordinates-geometry.json");

    ObjectMapper geowebAirmetObjectMapper = airmetObjectMapper;
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );

    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
    assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.SUCCESS);
    assertEquals(fmiAirmet.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getTacGeometry().get().getTacContent(), "WI N5216 E00238 - N5138 E00258 - N5137 E00205 - N5216 E00238");
    List<Double> intersectCoords = Arrays.asList(52.274632333864965, 2.6333605219412513, 51.636777, 2.969053, 51.60946036704992, 2.0894980945086408, 52.274632333864965, 2.6333605219412513);
    assertEquals(((PolygonGeometry) (fmiAirmet.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getGeoGeometry().get())).getExteriorRingPositions(),intersectCoords);
  }

  // Drawn:
  // [1.8972202640043532, 52.00225609676087, 5.51192036851197, 49.11837323675346, 8.4778281465695, 51.86891924321323, 1.8972202640043532, 52.00225609676087]
  // N5200 E00154 - N5152 E00829 - N4907 E00531 - N5200 E00154
  // Intersection:
  // [2.4022909006635103, 51.99202219809572, 2.2079578858334536, 51.7543428380748, 2.5980337339471675, 51.44313219622742, 3.370001, 51.369722, 3.362223, 51.320002, 3.36389, 51.313608, 3.373613, 51.309999, 3.952501, 51.214441, 4.397501, 51.452776, 5.078611, 51.391665, 5.848333, 51.139444, 5.651667, 50.824717, 6.011797, 50.757273, 5.934168, 51.036386, 6.222223, 51.361666, 5.94639, 51.811663, 6.405001, 51.830828, 6.5285677285655845, 51.90841512510155, 2.4022909006635103, 51.99202219809572]
  @Test
  public void readMore6IntersectionGeometryAirmet() throws Exception {
    /* The drawn polygon should be used in the TAC */
    String airmetJson = Tools.readResource("./geowebairmets/airmet-more-6-coordinates-geometry.json");

    ObjectMapper geowebAirmetObjectMapper = airmetObjectMapper;
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );


     ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
     assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.SUCCESS);
     assertEquals(fmiAirmet.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getTacGeometry().get().getTacContent(), "WI N5200 E00154 - N5152 E00829 - N4907 E00531 - N5200 E00154");
     List<Double> intersectCoords = Arrays.asList(52.00225609676087, 1.8972202640043532, 51.86891924321323, 8.4778281465695, 49.11837323675346, 5.51192036851197, 52.00225609676087, 1.8972202640043532);
     assertEquals(((PolygonGeometry) (fmiAirmet.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getGeoGeometry().get())).getExteriorRingPositions(),intersectCoords);
  }

  @Test
  public void readPointIntersectionGeometryAirmet() throws Exception {
    /* The drawn polygon should be used in the TAC */
    String airmetJson = Tools.readResource("./geowebairmets/airmet-points-geometry.json");

    ObjectMapper geowebAirmetObjectMapper = airmetObjectMapper;
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );

    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
    assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.SUCCESS);
    List<Double> intersectCoords = Arrays.asList(53.444948, 3.518629);
    assertEquals(((PointGeometry) (fmiAirmet.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getGeoGeometry().get())).getCoordinates(),intersectCoords);
  }

  @Test
  public void readFIRGeometryAirmet() throws Exception {
    /* The drawn polygon should be used in the TAC */
    String airmetJson = Tools.readResource("./geowebairmets/airmet-FIR-geometry.json");

    ObjectMapper geowebAirmetObjectMapper = airmetObjectMapper;
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );

    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
     assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.SUCCESS);
     assertEquals(fmiAirmet.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getTacGeometry().get().getTacContent(), "ENTIRE FIR");
     List<Double> intersectCoords = Arrays.asList(55.332644, 4.331914, 55.764314, 3.368817, 54.379261, 2.761908, 52.913554, 3.15576, 51.500002, 2.000002, 51.369722, 3.370001, 51.320002, 3.362223, 51.313608, 3.36389, 51.309999, 3.373613, 51.214441, 3.952501, 51.452776, 4.397501, 51.391665, 5.078611, 51.139444, 5.848333, 50.824717, 5.651667, 50.757273, 6.011797, 51.036386, 5.934168, 51.361666, 6.222223, 51.811663, 5.94639, 51.830828, 6.405001, 52.237764, 7.053095, 52.268885, 7.031389, 52.346109, 7.063612, 52.385828, 7.065557, 52.888887, 7.133055, 52.898244, 7.14218, 53.3, 7.191667, 53.666667, 6.5, 55.000002, 6.500002, 55.0, 5.0, 55.332644, 4.331914);
     assertEquals(((PolygonGeometry) (fmiAirmet.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getGeoGeometry().get())).getExteriorRingPositions(),intersectCoords);
  }

  @Test
  public void readBoxOneSideGeometryAirmet() throws Exception {
    /* The drawn polygon should be used in the TAC */
    String airmetJson = Tools.readResource("./geowebairmets/airmet-box-one-side-geometry.json");

    ObjectMapper geowebAirmetObjectMapper = airmetObjectMapper;
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );

    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
     assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.SUCCESS);
     assertEquals(fmiAirmet.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getTacGeometry().get().getTacContent(), "N OF N5506");
     List<Double> intersectCoords = Arrays.asList(55.102156117325436, 0.8555521943158428, 55.102156117325436, 8.282160619544873,  56.358231580380156, 8.282160619544873, 56.358231580380156, 0.8555521943158428, 55.102156117325436, 0.8555521943158428);
     assertEquals(((PolygonGeometry) (fmiAirmet.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getGeoGeometry().get())).getExteriorRingPositions(),intersectCoords);
  }

  @Test
  public void readBoxTwoSideGeometryAirmet() throws Exception {
    /* The drawn polygon should be used in the TAC */
    String airmetJson = Tools.readResource("./geowebairmets/airmet-box-two-side-geometry.json");

    ObjectMapper geowebAirmetObjectMapper = airmetObjectMapper;
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );

    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
     assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.SUCCESS);
     assertEquals("N OF N5400 AND E OF E00510", fmiAirmet.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getTacGeometry().get().getTacContent());
     List<Double> intersectCoords = Arrays.asList(53.99557237183801, 5.170617168658404, 53.99557237183801, 8.810535922525597, 55.503182552990275, 8.810535922525597, 55.503182552990275, 5.170617168658404, 53.99557237183801, 5.170617168658404);
     assertEquals(((PolygonGeometry) (fmiAirmet.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getGeoGeometry().get())).getExteriorRingPositions(),intersectCoords);
  }

  @Test
  public void readBoxTwoSideGeometryTwoRoundsAirmet() throws Exception {
    /* The drawn polygon should be used in the TAC */
    String airmetJson = Tools.readResource("./geowebairmets/airmet-box-two-side-geometry-two-rounds.json");

    ObjectMapper geowebAirmetObjectMapper = airmetObjectMapper;
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );

    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
     assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.SUCCESS);
     assertEquals("N OF N5400 AND E OF E00600", fmiAirmet.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getTacGeometry().get().getTacContent());
     List<Double> intersectCoords = Arrays.asList(53.99557237183801, 5.997, 53.99557237183801, 8.810535922525597, 55.503182552990275, 8.810535922525597, 55.503182552990275, 5.997, 53.99557237183801, 5.997);
     assertEquals(((PolygonGeometry) (fmiAirmet.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getGeoGeometry().get())).getExteriorRingPositions(),intersectCoords);
  }

  @Test
  public void readBoxThreeSideGeometryAirmet() throws Exception {
    /* The intersection polygon should be used in the TAC */
    String airmetJson = Tools.readResource("./geowebairmets/airmet-box-three-side-geometry.json");

    ObjectMapper geowebAirmetObjectMapper = airmetObjectMapper;
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );

    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
     assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.SUCCESS);
     assertEquals(fmiAirmet.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getTacGeometry().get().getTacContent(), "WI N5320 E00421 - N5519 E00421 - N5500 E00500 - N5500 E00615 - N5320 E00615 - N5320 E00421");
     List<Double> intersectCoords = Arrays.asList(53.33467608564436, 4.348700030688394, 53.33467608564436, 6.256721958118777, 55.569626607203254, 6.256721958118777, 55.569626607203254, 4.348700030688394, 53.33467608564436, 4.348700030688394);
     assertEquals(((PolygonGeometry) (fmiAirmet.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getGeoGeometry().get())).getExteriorRingPositions(),intersectCoords);
  }

  @Test
  public void readBoxThreeSide8IntersectionsGeometryAirmet() throws Exception {
    /* The drawn polygon should be used in the TAC */
    String airmetJson = Tools.readResource("./geowebairmets/airmet-box-three-side-eight-intersections-geometry.json");

    ObjectMapper geowebAirmetObjectMapper = airmetObjectMapper;
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );

    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
     assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.SUCCESS);
     assertEquals(fmiAirmet.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getTacGeometry().get().getTacContent(), "WI N5112 E00149 - N5226 E00149 - N5226 E00629 - N5112 E00629 - N5112 E00149");
     List<Double> intersectCoords = Arrays.asList(51.19742239025377, 1.8242402497805006, 51.19742239025377, 6.491555426110208, 52.431323302349035, 6.491555426110208, 52.431323302349035, 1.8242402497805006, 51.19742239025377, 1.8242402497805006);
     assertEquals(((PolygonGeometry) (fmiAirmet.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getGeoGeometry().get())).getExteriorRingPositions(),intersectCoords);
  }

  @Test
  public void readBoxMultipleIntersectionsGeometrySigmet() throws Exception {
    /* The drawn box should be used in the TAC */
    String airmetJson = Tools.readResource("./geowebairmets/airmet-box-multiple-intersections-geometry.json");

    ObjectMapper geowebAirmetObjectMapper = airmetObjectMapper;
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );

    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
     assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.SUCCESS);
     assertEquals(fmiAirmet.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getTacGeometry().get().getTacContent(), "S OF N5120");
     List<Double> intersectCoords = Arrays.asList(50.549110289194275, 2.96905340623873, 50.549110289194275, 6.873159811596286, 51.32600382365317, 6.873159811596286, 51.32600382365317, 2.96905340623873, 50.549110289194275, 2.96905340623873);
     assertEquals(((PolygonGeometry) (fmiAirmet.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getGeoGeometry().get())).getExteriorRingPositions(),intersectCoords);
  }

  @Test
  public void readBoxMultipleIntersectionsMultipleSidesGeometrySigmet() throws Exception {
    /* The drawn box should be used in the TAC */
    String airmetJson = Tools.readResource("./geowebairmets/airmet-box-multiple-intersections-mulitple-sides-geometry.json");

    ObjectMapper geowebAirmetObjectMapper = airmetObjectMapper;
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );

    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
     assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.SUCCESS);
     assertEquals(fmiAirmet.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getTacGeometry().get().getTacContent(), "WI N5012 E00656 - N5012 E00605 - N5333 E00605 - N5333 E00656 - N5012 E00656");
     List<Double> intersectCoords = Arrays.asList(50.19338677117774, 6.931868178594142, 53.54450043024857,6.931868178594142, 53.54450043024857, 6.0805968571252, 50.19338677117774,  6.0805968571252, 50.19338677117774, 6.931868178594142);
     assertEquals(((PolygonGeometry) (fmiAirmet.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getGeoGeometry().get())).getExteriorRingPositions(),intersectCoords);
  }
}

