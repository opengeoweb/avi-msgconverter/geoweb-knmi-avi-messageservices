package nl.knmi.geoweb.backend.product.GeoWebAirmet;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.geojson.FeatureCollection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fi.fmi.avi.converter.AviMessageConverter;
import fi.fmi.avi.converter.ConversionHints;
import fi.fmi.avi.converter.ConversionIssue;
import fi.fmi.avi.converter.ConversionResult;
import fi.fmi.avi.converter.ConversionSpecification;
import fi.fmi.avi.converter.iwxxm.conf.IWXXMConverter;
import fi.fmi.avi.model.sigmet.AIRMET;
import lombok.extern.slf4j.Slf4j;
import nl.knmi.geoweb.TestConfig;
import nl.knmi.geoweb.backend.aviation.FIRStore;
import nl.knmi.geoweb.backend.aviation.Tools;
import nl.knmi.geoweb.backend.converters.AirmetConverters.GeoWebAirmetToFMIAirmet;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.om.CreateAirmetObjectMapper;

import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.diff.Diff;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { TestConfig.class })
public class TestGeoWebAirmetToFMIIWXXM20231 {

  @Autowired
  @Qualifier("geowebAviMessageConverter")
  private AviMessageConverter geowebAviMessageConverter;

  @Autowired
  private FIRStore firStore;

    /**
   * Compare two IWXXM documents and check if they are equal, leaving out unique id's.
   * @param expected
   * @param result
   * @return
   */
  static public Boolean iwxxmHasDifferences (String expected, String result) {
    System.out.println("IWXXM: "+result);
    Diff xmlDiff = DiffBuilder.compare(expected)
    .withTest(result)
    .withAttributeFilter(attr -> {
      return !(attr.getName().equals("gml:id") || attr.getName().equals("xlink:href"));
    }).withNodeFilter(node -> {
      return !(node.getNodeName().equals("gml:timePosition"));
    })
    .ignoreComments()
    .ignoreWhitespace()
    .build();
    if (xmlDiff.hasDifferences()) {
      log.error(xmlDiff.toString());
    }
    return xmlDiff.hasDifferences();
  }

  @Test
  public void testGeoWebAirmetToIXWWM20231() throws Exception {
    String airmetJson = Tools.readResource("./geowebairmets/published.json");
    ObjectMapper geowebAirmetObjectMapper = CreateAirmetObjectMapper.getOm();
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );
    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
    assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IXWWM2023_1 */
    final ConversionSpecification<AIRMET, String> airmetToIWXXMSpec = IWXXMConverter.AIRMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIWXXM2023_1 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiAirmet.getConvertedMessage().get(), airmetToIWXXMSpec, ConversionHints.AIRMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM2023_1.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM2023_1.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Ensure IWXXM conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM2023_1.getStatus());
    String airmetIWXXM2023_1 = Tools.readResource("./geowebairmets/published-FMI.IWXXM20231");

    assertFalse(iwxxmHasDifferences(airmetIWXXM2023_1, resultAsIWXXM2023_1.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebAirmetToIWXXM20231TestMovement() throws Exception {
    String airmetJson = Tools.readResource("./geowebairmets/testingmovement.json");
    ObjectMapper geowebAirmetObjectMapper = CreateAirmetObjectMapper.getOm();
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );
    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
    assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM2023_1 */
    final ConversionSpecification<AIRMET, String> airmetToIWXXMSpec = IWXXMConverter.AIRMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIWXXM20231 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiAirmet.getConvertedMessage().get(), airmetToIWXXMSpec, ConversionHints.AIRMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM20231.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM20231.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Ensure IWXXM conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM20231.getStatus());

    String airmetIWXXM20231 = Tools.readResource("./geowebairmets/testingmovement.IWXXM20231");

    assertFalse(iwxxmHasDifferences(airmetIWXXM20231, resultAsIWXXM20231.getConvertedMessage().get()));

  }


  @Test
  public void testGeoWebAirmetToIWXXM2023_1TestCancel() throws Exception {
    String airmetJson = Tools.readResource("./geowebairmets/cancel.json");
    ObjectMapper geowebAirmetObjectMapper = CreateAirmetObjectMapper.getOm();
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );
    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
    assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM2023_1 */
    final ConversionSpecification<AIRMET, String> airmetToIWXXMSpec = IWXXMConverter.AIRMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIWXXM2023_1 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiAirmet.getConvertedMessage().get(), airmetToIWXXMSpec, ConversionHints.AIRMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM2023_1.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM2023_1.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Ensure IWXXM conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM2023_1.getStatus());

    String airmetIWXXM2023_1 = Tools.readResource("./geowebairmets/cancel.IWXXM20231");

    assertFalse(iwxxmHasDifferences(airmetIWXXM2023_1, resultAsIWXXM2023_1.getConvertedMessage().get()));

  }

  @Test
  public void testGeoWebAirmetToIWXXM2023_1TestExpired() throws Exception {
    String airmetJson = Tools.readResource("./geowebairmets/expired.json");
    ObjectMapper geowebAirmetObjectMapper = CreateAirmetObjectMapper.getOm();
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );
    if (geowebAirmet.getFirGeometry()==null) {
      // Add FIR from firStore
      FeatureCollection collection = new FeatureCollection();
      collection.add(firStore.lookup(geowebAirmet.getFirName(), true));
      geowebAirmet.setFirGeometry(collection);
    }
    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
    assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM2023_1 */
    final ConversionSpecification<AIRMET, String> airmetToIWXXMSpec = IWXXMConverter.AIRMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIWXXM2023_1 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiAirmet.getConvertedMessage().get(), airmetToIWXXMSpec, ConversionHints.AIRMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM2023_1.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM2023_1.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Ensure IWXXM conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM2023_1.getStatus());

    String airmetIWXXM2023_1 = Tools.readResource("./geowebairmets/expired.IWXXM20231");

    assertFalse(iwxxmHasDifferences(airmetIWXXM2023_1, resultAsIWXXM2023_1.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebAirmetToIWXXM2023_1TestPublishedSurfaceVisibility() throws Exception {
    String airmetJson = Tools.readResource("./geowebairmets/publishedsurfacevis.json");
    ObjectMapper geowebAirmetObjectMapper = CreateAirmetObjectMapper.getOm();
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );
    if (geowebAirmet.getFirGeometry()==null) {
      // Add FIR from firStore
      FeatureCollection collection = new FeatureCollection();
      collection.add(firStore.lookup(geowebAirmet.getFirName(), true));
      geowebAirmet.setFirGeometry(collection);
    }
    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
    assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM2023_1 */
    final ConversionSpecification<AIRMET, String> airmetToIWXXMSpec = IWXXMConverter.AIRMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIWXXM2023_1 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiAirmet.getConvertedMessage().get(), airmetToIWXXMSpec, ConversionHints.AIRMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM2023_1.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM2023_1.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Ensure IWXXM conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM2023_1.getStatus());

    String airmetIWXXM2023_1 = Tools.readResource("./geowebairmets/publishedsurfacevis.IWXXM20231");

    assertFalse(iwxxmHasDifferences(airmetIWXXM2023_1, resultAsIWXXM2023_1.getConvertedMessage().get()));
  }


  @Test
  public void testGeoWebAirmetToIWXXM2023_1TestPublishedAirmetBoxMultipleIntersectionsMulitpleSidesGeometry() throws Exception {
    String airmetJson = Tools.readResource("./geowebairmets/airmet-box-multiple-intersections-mulitple-sides-geometry.json");
    ObjectMapper geowebAirmetObjectMapper = CreateAirmetObjectMapper.getOm();
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );
    if (geowebAirmet.getFirGeometry()==null) {
      // Add FIR from firStore
      FeatureCollection collection = new FeatureCollection();
      collection.add(firStore.lookup(geowebAirmet.getFirName(), true));
      geowebAirmet.setFirGeometry(collection);
    }
    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
    assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM2023_1 */
    final ConversionSpecification<AIRMET, String> airmetToIWXXMSpec = IWXXMConverter.AIRMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIWXXM2023_1 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiAirmet.getConvertedMessage().get(), airmetToIWXXMSpec, ConversionHints.AIRMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM2023_1.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM2023_1.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Ensure IWXXM conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM2023_1.getStatus());

    String airmetIWXXM2023_1 = Tools.readResource("./geowebairmets/airmet-box-multiple-intersections-mulitple-sides-geometry.IWXXM20231");
    assertFalse(iwxxmHasDifferences(airmetIWXXM2023_1, resultAsIWXXM2023_1.getConvertedMessage().get()));

  }


  @Test
  public void testGeoWebAirmetToIWXXM2023_1ReadGeoWebAirmetAndCreateAirmetBKNCLD() throws Exception {
    String airmetJson = Tools.readResource("./airmet-readGeoWebAirmetAndCreateAirmetBKNCLD.json");
    ObjectMapper geowebAirmetObjectMapper = CreateAirmetObjectMapper.getOm();
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );
    if (geowebAirmet.getFirGeometry()==null) {
      // Add FIR from firStore
      FeatureCollection collection = new FeatureCollection();
      collection.add(firStore.lookup(geowebAirmet.getFirName(), true));
      geowebAirmet.setFirGeometry(collection);
    }
    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
    assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM2023_1 */
    final ConversionSpecification<AIRMET, String> airmetToIWXXMSpec = IWXXMConverter.AIRMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIWXXM2023_1 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiAirmet.getConvertedMessage().get(), airmetToIWXXMSpec, ConversionHints.AIRMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM2023_1.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM2023_1.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Ensure IWXXM conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM2023_1.getStatus());
    String airmetIWXXM2023_1 = Tools.readResource("./airmet-readGeoWebAirmetAndCreateAirmetBKNCLD-FMI.IWXXM20231");
    assertFalse(iwxxmHasDifferences(airmetIWXXM2023_1, resultAsIWXXM2023_1.getConvertedMessage().get()));

  }

  @Test
  public void testGeoWebAirmetToIWXXM2023_1ReadGeoWebAirmetAndCreateAirmetBKNCLDSFC() throws Exception {
    String airmetJson = Tools.readResource("./airmet-readGeoWebAirmetAndCreateAirmetBKNCLDSFC.json");
    ObjectMapper geowebAirmetObjectMapper = CreateAirmetObjectMapper.getOm();
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );
    if (geowebAirmet.getFirGeometry()==null) {
      // Add FIR from firStore
      FeatureCollection collection = new FeatureCollection();
      collection.add(firStore.lookup(geowebAirmet.getFirName(), true));
      geowebAirmet.setFirGeometry(collection);
    }
    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
    assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM2023_1 */
    final ConversionSpecification<AIRMET, String> airmetToIWXXMSpec = IWXXMConverter.AIRMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIWXXM2023_1 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiAirmet.getConvertedMessage().get(), airmetToIWXXMSpec, ConversionHints.AIRMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM2023_1.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM2023_1.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Ensure IWXXM conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM2023_1.getStatus());

    String airmetIWXXM2023_1 = Tools.readResource("./airmet-readGeoWebAirmetAndCreateAirmetBKNCLDSFC-FMI.IWXXM20231");

    assertFalse(iwxxmHasDifferences(airmetIWXXM2023_1, resultAsIWXXM2023_1.getConvertedMessage().get()));

  }

  @Test
  public void testGeoWebAirmetToIWXXM2023_1ReadGeoWebAirmetAndCreateAirmetOVCCLDABV() throws Exception {
    String airmetJson = Tools.readResource("./airmet-readGeoWebAirmetAndCreateAirmetOVCCLDABV.json");
    ObjectMapper geowebAirmetObjectMapper = CreateAirmetObjectMapper.getOm();
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );
    if (geowebAirmet.getFirGeometry()==null) {
      // Add FIR from firStore
      FeatureCollection collection = new FeatureCollection();
      collection.add(firStore.lookup(geowebAirmet.getFirName(), true));
      geowebAirmet.setFirGeometry(collection);
    }
    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
    assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM2023_1 */
    final ConversionSpecification<AIRMET, String> airmetToIWXXMSpec = IWXXMConverter.AIRMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIWXXM2023_1 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiAirmet.getConvertedMessage().get(), airmetToIWXXMSpec, ConversionHints.AIRMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM2023_1.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM2023_1.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Ensure IWXXM conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM2023_1.getStatus());

    String airmetIWXXM2023_1 = Tools.readResource("./airmet-readGeoWebAirmetAndCreateAirmetOVCCLDABV-FMI.IWXXM20231");

    assertFalse(iwxxmHasDifferences(airmetIWXXM2023_1, resultAsIWXXM2023_1.getConvertedMessage().get()));

  }

  @Test
  public void testGeoWebAirmetToIWXXM2023_1ReadGeoWebAirmetAndCreateAirmetOVCCLDSFCABV() throws Exception {
    String airmetJson = Tools.readResource("./airmet-readGeoWebAirmetAndCreateAirmetOVCCLDSFCABV.json");
    ObjectMapper geowebAirmetObjectMapper = CreateAirmetObjectMapper.getOm();
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );
    if (geowebAirmet.getFirGeometry()==null) {
      // Add FIR from firStore
      FeatureCollection collection = new FeatureCollection();
      collection.add(firStore.lookup(geowebAirmet.getFirName(), true));
      geowebAirmet.setFirGeometry(collection);
    }
    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
    assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM2023_1 */
    final ConversionSpecification<AIRMET, String> airmetToIWXXMSpec = IWXXMConverter.AIRMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIWXXM2023_1 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiAirmet.getConvertedMessage().get(), airmetToIWXXMSpec, ConversionHints.AIRMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM2023_1.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM2023_1.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Ensure IWXXM conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM2023_1.getStatus());

    String airmetIWXXM2023_1 = Tools.readResource("./airmet-readGeoWebAirmetAndCreateAirmetOVCCLDSFCABV-FMI.IWXXM20231");

    assertFalse(iwxxmHasDifferences(airmetIWXXM2023_1, resultAsIWXXM2023_1.getConvertedMessage().get()));

  }

  @Test
  public void testGeoWebAirmetToIWXXM2023_1ReadGeoWebAirmetAndCreateAirmetSFCWIND() throws Exception {
    String airmetJson = Tools.readResource("./airmet-readGeoWebAirmetAndCreateAirmetSFCWIND.json");
    ObjectMapper geowebAirmetObjectMapper = CreateAirmetObjectMapper.getOm();
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );
    if (geowebAirmet.getFirGeometry()==null) {
      // Add FIR from firStore
      FeatureCollection collection = new FeatureCollection();
      collection.add(firStore.lookup(geowebAirmet.getFirName(), true));
      geowebAirmet.setFirGeometry(collection);
    }
    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
    assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM2023_1 */
    final ConversionSpecification<AIRMET, String> airmetToIWXXMSpec = IWXXMConverter.AIRMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIWXXM2023_1 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiAirmet.getConvertedMessage().get(), airmetToIWXXMSpec, ConversionHints.AIRMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM2023_1.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM2023_1.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Ensure IWXXM conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM2023_1.getStatus());

    String airmetIWXXM2023_1 = Tools.readResource("./airmet-readGeoWebAirmetAndCreateAirmetSFCWIND-FMI.IWXXM20231");

    assertFalse(iwxxmHasDifferences(airmetIWXXM2023_1, resultAsIWXXM2023_1.getConvertedMessage().get()));

  }

}