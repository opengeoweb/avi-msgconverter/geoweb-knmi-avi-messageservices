package nl.knmi.geoweb.backend.product.GeoWebAirmet;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fi.fmi.avi.converter.AviMessageConverter;
import fi.fmi.avi.converter.ConversionHints;
import fi.fmi.avi.converter.ConversionIssue;
import fi.fmi.avi.converter.ConversionResult;
import fi.fmi.avi.converter.ConversionSpecification;
import fi.fmi.avi.converter.tac.conf.TACConverter;
import fi.fmi.avi.model.sigmet.AIRMET;
import lombok.extern.slf4j.Slf4j;
import nl.knmi.geoweb.TestConfig;
import nl.knmi.geoweb.backend.aviation.Tools;
import nl.knmi.geoweb.backend.converters.AirmetConverters.GeoWebAirmetToFMIAirmet;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.om.CreateAirmetObjectMapper;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { TestConfig.class })
public class TestGeoWebAirmetToFMITAC {

  @Autowired
  @Qualifier("geowebTafObjectMapper")
  private ObjectMapper geowebTafObjectMapper;


  @Autowired
  @Qualifier("geowebAviMessageConverter")
  private AviMessageConverter geowebAviMessageConverter;

  public ConversionHints getTokenizerParsingHints() {
    return new ConversionHints(ConversionHints.KEY_VALIDTIME_FORMAT,
        ConversionHints.VALUE_VALIDTIME_FORMAT_PREFER_LONG);
  }

  @Test
  public void testGeoWebAirmetToTAC() throws Exception {
    String airmetJson = Tools.readResource("./geowebairmets/published.json");
    ObjectMapper geowebAirmetObjectMapper = CreateAirmetObjectMapper.getOm();
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );
    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
    assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to TAC */
    final ConversionSpecification<AIRMET, String> airmetToTacSpec = TACConverter.AIRMET_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiAirmet.getConvertedMessage().get(), airmetToTacSpec, getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());
    String airmetTac = Tools.readResource("./geowebairmets/published-FMI.tac");
    airmetTac = airmetTac.replace("\n", "\r\n");
    assertEquals(airmetTac, resultAsTAC.getConvertedMessage().get());

  }
  @Test
  public void testGeoWebAirmetToTACTestMovement() throws Exception {
    String airmetJson = Tools.readResource("./geowebairmets/testingmovement.json");
    ObjectMapper geowebAirmetObjectMapper = CreateAirmetObjectMapper.getOm();
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );
    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
    assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to TAC */
    final ConversionSpecification<AIRMET, String> airmetToTacSpec = TACConverter.AIRMET_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiAirmet.getConvertedMessage().get(), airmetToTacSpec, getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());
    

    String airmetTac = Tools.readResource("./geowebairmets/testingmovement.tac");
    
    airmetTac = airmetTac.replace("\n", "\r\n");
    assertEquals(airmetTac, resultAsTAC.getConvertedMessage().get());

  }

  @Test
  public void testGeoWebAirmetToTAC_draft_outsidefir() throws Exception {
    String airmetJson = Tools.readResource("./geowebairmets/draft-outsidefir.json");
  
    ObjectMapper geowebAirmetObjectMapper = CreateAirmetObjectMapper.getOm();
    GeoWebAirmet geowebAirmet = geowebAirmetObjectMapper.readValue(
      airmetJson,
      GeoWebAirmet.class
    );
    ConversionResult<AIRMET> fmiAirmet = GeoWebAirmetToFMIAirmet.convert(geowebAirmet);
    assertEquals(fmiAirmet.getStatus(), ConversionResult.Status.FAIL);
    assertEquals(fmiAirmet.getConversionIssues().get(0).getType(),ConversionIssue.Type.LOGICAL);
    assertEquals(fmiAirmet.getConversionIssues().get(0).getMessage(), "The start position needs to be (partly) inside the FIR");
  }
}
