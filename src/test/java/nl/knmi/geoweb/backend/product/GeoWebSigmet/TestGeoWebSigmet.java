package nl.knmi.geoweb.backend.product.GeoWebSigmet;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.knmi.geoweb.backend.ApplicationConfig;
import nl.knmi.geoweb.backend.aviation.Tools;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.enums.ChangeEnum;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.enums.LevelInfoModeEnum;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.enums.LevelUnitEnum;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.enums.MovementTypeEnum;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.enums.ObservationOrForecastEnum;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.enums.PhenomenaEnum;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.enums.StatusEnum;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.enums.TypeEnum;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.om.CreateSigmetObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Import(ApplicationConfig.class)
public class TestGeoWebSigmet {

  @Autowired
  @Qualifier("sigmetObjectMapper")
  private ObjectMapper sigmetObjectMapper;

  @Test
  public void readGeoWebSigmetAndCheckProps() throws Exception {
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    String sigmetJson = Tools.readResource(
      "sigmet-readGeoWebSigmetAndCheckProps.json"
    );
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    assertEquals(geowebSigmet.getLocationIndicatorATSR(), "EHAA");
    assertEquals(geowebSigmet.getLocationIndicatorATSU(), "EHAA");
    assertEquals(geowebSigmet.getLocationIndicatorMWO(), "EHDB");
    assertEquals(geowebSigmet.getFirName(), "AMSTERDAM FIR");
    assertEquals(geowebSigmet.getChange(), ChangeEnum.NC);
    assertEquals(geowebSigmet.getLevel().getUnit(), LevelUnitEnum.FL);
    assertEquals(geowebSigmet.getLevel().getValue(), 150);
    assertEquals(
      geowebSigmet.getIsObservationOrForecast(),
      ObservationOrForecastEnum.OBS
    );
    assertEquals(geowebSigmet.getLevelInfoMode(), LevelInfoModeEnum.AT);
    assertEquals(geowebSigmet.getPhenomenon(), PhenomenaEnum.SEV_ICE);
    assertEquals(geowebSigmet.getType(), TypeEnum.TEST);
    assertEquals(
      CreateSigmetObjectMapper
        .getOm()
        .writeValueAsString(geowebSigmet.getObservationOrForecastTime()),
      "\"2020-11-09T13:15:00Z\""
    );
    assertEquals(
      CreateSigmetObjectMapper
        .getOm()
        .writeValueAsString(geowebSigmet.getIssueDate()),
      "\"2020-11-09T13:13:26Z\""
    );
    assertEquals(
      CreateSigmetObjectMapper
        .getOm()
        .writeValueAsString(geowebSigmet.getValidDateStart()),
      "\"2020-11-09T13:15:00Z\""
    );
    assertEquals(
      CreateSigmetObjectMapper
        .getOm()
        .writeValueAsString(geowebSigmet.getValidDateEnd()),
      "\"2020-11-09T14:59:29Z\""
    );
    assertEquals(
      geowebSigmet.getUuid(),
      "2c5c5792-228d-11eb-9b02-72f96a695f62"
    );
    assertEquals(geowebSigmet.getSequence(), "2");
    assertEquals(
      geowebSigmet.getMovementType(),
      MovementTypeEnum.FORECAST_POSITION
    );
    assertEquals(geowebSigmet.getStatus(), StatusEnum.CANCELLED);
  }
}
