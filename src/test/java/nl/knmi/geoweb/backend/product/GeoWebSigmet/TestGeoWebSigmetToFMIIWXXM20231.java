package nl.knmi.geoweb.backend.product.GeoWebSigmet;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.diff.Diff;

import fi.fmi.avi.converter.AviMessageConverter;
import fi.fmi.avi.converter.ConversionHints;
import fi.fmi.avi.converter.ConversionIssue;
import fi.fmi.avi.converter.ConversionResult;
import fi.fmi.avi.converter.ConversionSpecification;
import fi.fmi.avi.converter.iwxxm.conf.IWXXMConverter;
import fi.fmi.avi.model.sigmet.SIGMET;
import lombok.extern.slf4j.Slf4j;
import nl.knmi.geoweb.TestConfig;
import nl.knmi.geoweb.backend.aviation.FIRStore;
import nl.knmi.geoweb.backend.aviation.Tools;
import nl.knmi.geoweb.backend.converters.SigmetConverters.GeoWebSigmetToFMISigmet;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.om.CreateSigmetObjectMapper;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { TestConfig.class })
public class TestGeoWebSigmetToFMIIWXXM20231 {
  @Autowired
  FIRStore firStore;

  @Autowired
  @Qualifier("geowebAviMessageConverter")
  private AviMessageConverter geowebAviMessageConverter;

  /**
   * Compare two IWXXM documents and check if they are equal, leaving out unique id's.
   * @param expected
   * @param result
   * @return
   */
  static public Boolean iwxxmHasDifferences (String expected, String result) {
    Diff xmlDiff = DiffBuilder.compare(expected)
    .withTest(result)
    .withAttributeFilter(attr -> {
      return !(attr.getName().equals("gml:id") || attr.getName().equals("xlink:href") || attr.getName().equals("volcanoId"));
    }).withNodeFilter(node -> {
      return !(node.getNodeName().equals("gml:timePosition"));
    })
    .ignoreComments()
    .ignoreWhitespace()
    .build();
    if (xmlDiff.hasDifferences()) {
      log.error(xmlDiff.toString());
    }
    return xmlDiff.hasDifferences();
  }

  @Test
  public void testGeoWebSigmetToIWXXM20231() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/published.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM20231 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM20231Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIwxxm20231 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM20231Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIwxxm20231.getStatus()) {
        for (ConversionIssue issue:resultAsIwxxm20231.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }
    /* Assure IWXXM20231 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIwxxm20231.getStatus());

    String sigmetIWXXM20231 = Tools.readResource("./geowebsigmets/published-FMI.IWXXM20231") ;

    assertFalse(iwxxmHasDifferences(sigmetIWXXM20231, resultAsIwxxm20231.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetToIWXXM20231_cancel() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/cancels.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM20231*/
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM20231Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIwxxm20231 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM20231Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIwxxm20231.getStatus()) {
        for (ConversionIssue issue:resultAsIwxxm20231.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM20231 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIwxxm20231.getStatus());

    String sigmetIWXXM20231 = Tools.readResource("./geowebsigmets/cancels-FMI.IWXXM20231");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM20231, resultAsIwxxm20231.getConvertedMessage().get()));

  }

  @Test
  public void testGeoWebSigmetToIWXXM20231_draft() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/draft.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM20231 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM20231Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIwxxm20231 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM20231Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIwxxm20231.getStatus()) {
        for (ConversionIssue issue:resultAsIwxxm20231.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM20231 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIwxxm20231.getStatus());

    String sigmetIWXXM20231 = Tools.readResource("./geowebsigmets/draft-FMI.IWXXM20231");
    String resultAsIwxxm20231AsString = resultAsIwxxm20231.getConvertedMessage().get();
    assertFalse(iwxxmHasDifferences(sigmetIWXXM20231, resultAsIwxxm20231AsString));
  }


  @Test
  public void testGeoWebSigmetToIWXXM20231_draft_nogeom() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/draft-nogeom.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.FAIL);
    assertEquals(fmiSigmet.getConversionIssues().get(0).getType(),ConversionIssue.Type.MISSING_DATA);
    assertEquals(fmiSigmet.getConversionIssues().get(0).getMessage(), "SIGMET start area missing");
  }

  @Test
  public void testGeoWebSigmetToIWXXM20231_moving() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/moving.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM20231 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM20231Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIwxxm20231 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM20231Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIwxxm20231.getStatus()) {
        for (ConversionIssue issue:resultAsIwxxm20231.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM20231 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIwxxm20231.getStatus());

    String sigmetIWXXM20231 = Tools.readResource("./geowebsigmets/moving-FMI.IWXXM20231");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM20231, resultAsIwxxm20231.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetToIWXXM20231_between_SFC() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-between-SFC.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM20231 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM20231Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIwxxm20231 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM20231Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIwxxm20231.getStatus()) {
        for (ConversionIssue issue:resultAsIwxxm20231.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM20231 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIwxxm20231.getStatus());

    String sigmetIWXXM20231 = Tools.readResource("./geowebsigmets/sigmet-between-SFC-FMI.IWXXM20231");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM20231, resultAsIwxxm20231.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetToIWXXM20231_twolevels_ds() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/twolevels-ds.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM20231 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM20231Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIwxxm20231 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM20231Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIwxxm20231.getStatus()) {
        for (ConversionIssue issue:resultAsIwxxm20231.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM20231 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIwxxm20231.getStatus());
    String sigmetIWXXM20231 = Tools.readResource("./geowebsigmets/twolevels-ds-FMI.IWXXM20231");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM20231, resultAsIwxxm20231.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetToIWXXM20231_vafields() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-vafields.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM20231 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM20231Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIwxxm20231 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM20231Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIwxxm20231.getStatus()) {
        for (ConversionIssue issue:resultAsIwxxm20231.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM20231 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIwxxm20231.getStatus());
    String sigmetIWXXM20231 = Tools.readResource("./geowebsigmets/sigmet-vafields-FMI.IWXXM20231");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM20231, resultAsIwxxm20231.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetToIWXXM20231_vafields_withcoordinates() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-vafields-withcoordinates.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM20231 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM20231Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIwxxm20231 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM20231Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIwxxm20231.getStatus()) {
        for (ConversionIssue issue:resultAsIwxxm20231.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM20231 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIwxxm20231.getStatus());
    String sigmetIWXXM20231 = Tools.readResource("./geowebsigmets/sigmet-vafields-withcoordinates-FMI.IWXXM20231");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM20231, resultAsIwxxm20231.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetVaCld() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-vacld.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM20231 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM20231Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIwxxm20231 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM20231Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIwxxm20231.getStatus()) {
        for (ConversionIssue issue:resultAsIwxxm20231.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM20231 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIwxxm20231.getStatus());
    String sigmetIWXXM20231 = Tools.readResource("./geowebsigmets/sigmet-vacld-FMI.IWXXM20231");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM20231, resultAsIwxxm20231.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetVaCancel() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-vacancel.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM20231*/
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM20231Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIwxxm20231 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM20231Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIwxxm20231.getStatus()) {
        for (ConversionIssue issue:resultAsIwxxm20231.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM20231 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIwxxm20231.getStatus());
    String sigmetIWXXM20231 = Tools.readResource("./geowebsigmets/sigmet-vacancel-FMI.IWXXM20231");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM20231, resultAsIwxxm20231.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetRdoactCldKnmi() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-rdoact-cld-KNMI.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM20231*/
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM20231Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIwxxm20231 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM20231Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIwxxm20231.getStatus()) {
        for (ConversionIssue issue:resultAsIwxxm20231.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM20231 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIwxxm20231.getStatus());
    String sigmetIWXXM20231 = Tools.readResource("./geowebsigmets/sigmet-rdoact-cld-KNMI.IWXXM20231");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM20231, resultAsIwxxm20231.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetRdoactCldFmi() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-rdoact-cld-FMI.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM20231*/
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM20231Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIwxxm20231 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM20231Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIwxxm20231.getStatus()) {
        for (ConversionIssue issue:resultAsIwxxm20231.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM20231 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIwxxm20231.getStatus());
    String sigmetIWXXM20231 = Tools.readResource("./geowebsigmets/sigmet-rdoact-cld-FMI.IWXXM20231");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM20231, resultAsIwxxm20231.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetRdoactCldMetno() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-rdoact-cld-METNO.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM20231*/
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM20231Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIwxxm20231 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM20231Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIwxxm20231.getStatus()) {
        for (ConversionIssue issue:resultAsIwxxm20231.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM20231 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIwxxm20231.getStatus());
    String sigmetIWXXM20231 = Tools.readResource("./geowebsigmets/sigmet-rdoact-cld-METNO.IWXXM20231");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM20231, resultAsIwxxm20231.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetShortTest() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-short-test.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
            sigmetJson,
            GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM20231*/
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM20231Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIwxxm20231 =
            geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM20231Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIwxxm20231.getStatus()) {
      for (ConversionIssue issue:resultAsIwxxm20231.getConversionIssues()) {
        log.warn(issue.toString());
      }
    }

    /* Assert IWXXM20231 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIwxxm20231.getStatus());
    String sigmetIWXXM20231 = Tools.readResource("./geowebsigmets/sigmet-short-test.IWXXM20231");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM20231, resultAsIwxxm20231.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetShortVaTest() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-short-va-test.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
            sigmetJson,
            GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM20231*/
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM20231Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIwxxm20231 =
            geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM20231Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIwxxm20231.getStatus()) {
      for (ConversionIssue issue:resultAsIwxxm20231.getConversionIssues()) {
        log.warn(issue.toString());
      }
    }

    /* Assert IWXXM20231 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIwxxm20231.getStatus());
    String sigmetIWXXM20231 = Tools.readResource("./geowebsigmets/sigmet-short-va-test.IWXXM20231");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM20231, resultAsIwxxm20231.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetShortTestCancel() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-short-test-cancel.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
            sigmetJson,
            GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM20231*/
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM20231Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIwxxm20231 =
            geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM20231Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIwxxm20231.getStatus()) {
      for (ConversionIssue issue:resultAsIwxxm20231.getConversionIssues()) {
        log.warn(issue.toString());
      }
    }

    /* Assert IWXXM20231 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIwxxm20231.getStatus());
    String sigmetIWXXM20231 = Tools.readResource("./geowebsigmets/sigmet-short-test-cancel.IWXXM20231");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM20231, resultAsIwxxm20231.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetShortVaTestCancel() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-short-va-test-cancel.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
            sigmetJson,
            GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM20231*/
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM20231Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM2023_1_STRING;
    final ConversionResult<String> resultAsIwxxm20231 =
            geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM20231Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIwxxm20231.getStatus()) {
      for (ConversionIssue issue:resultAsIwxxm20231.getConversionIssues()) {
        log.warn(issue.toString());
      }
    }

    /* Assert IWXXM20231 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIwxxm20231.getStatus());
    String sigmetIWXXM20231 = Tools.readResource("./geowebsigmets/sigmet-short-va-test-cancel.IWXXM20231");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM20231, resultAsIwxxm20231.getConvertedMessage().get()));
  }

}
