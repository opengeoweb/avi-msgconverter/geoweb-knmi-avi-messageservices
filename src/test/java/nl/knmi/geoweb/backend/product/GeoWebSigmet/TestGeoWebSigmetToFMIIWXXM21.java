package nl.knmi.geoweb.backend.product.GeoWebSigmet;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.diff.Diff;

import fi.fmi.avi.converter.AviMessageConverter;
import fi.fmi.avi.converter.ConversionHints;
import fi.fmi.avi.converter.ConversionIssue;
import fi.fmi.avi.converter.ConversionResult;
import fi.fmi.avi.converter.ConversionSpecification;
import fi.fmi.avi.converter.iwxxm.conf.IWXXMConverter;
import fi.fmi.avi.model.sigmet.SIGMET;
import lombok.extern.slf4j.Slf4j;
import nl.knmi.geoweb.TestConfig;
import nl.knmi.geoweb.backend.aviation.FIRStore;
import nl.knmi.geoweb.backend.aviation.Tools;
import nl.knmi.geoweb.backend.converters.SigmetConverters.GeoWebSigmetToFMISigmet;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.om.CreateSigmetObjectMapper;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { TestConfig.class })
public class TestGeoWebSigmetToFMIIWXXM21 {

  @Autowired
  FIRStore firStore;

  @Autowired
  @Qualifier("geowebAviMessageConverter")
  private AviMessageConverter geowebAviMessageConverter;

  /**
   * Compare two IWXXM documents and check if they are equal, leaving out unique id's.
   * @param expected
   * @param result
   * @return
   */
  static public Boolean iwxxmHasDifferences (String expected, String result) {
    Diff xmlDiff = DiffBuilder.compare(expected)
    .withTest(result)
    .withAttributeFilter(attr -> {
      return !(attr.getName().equals("gml:id") || attr.getName().equals("xlink:href"));
    }).withNodeFilter(node -> {
      return !(node.getNodeName().equals("gml:timePosition"));
    })
    .ignoreComments()
    .ignoreWhitespace()
    .build();
    if (xmlDiff.hasDifferences()) {
      log.error(xmlDiff.toString());
    }
    return xmlDiff.hasDifferences();
  }

  @Test
  public void testGeoWebSigmetToIWXXM21() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/published.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM21 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM21Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM21_STRING;
    final ConversionResult<String> resultAsIWXXM21 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM21Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM21.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM21.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }
    /* Assure IWXXM21 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM21.getStatus());

    String sigmetIWXXM21 = Tools.readResource("./geowebsigmets/published-FMI.IWXXM21") ;

    assertFalse(iwxxmHasDifferences(sigmetIWXXM21, resultAsIWXXM21.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetToIWXXM21_cancel() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/cancels.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM21 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM21Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM21_STRING;
    final ConversionResult<String> resultAsIWXXM21 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM21Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM21.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM21.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM21 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM21.getStatus());
    String sigmetIWXXM21 = Tools.readResource("./geowebsigmets/cancels-FMI.IWXXM21");
    sigmetIWXXM21 = sigmetIWXXM21.replace("\n", "\r\n");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM21, resultAsIWXXM21.getConvertedMessage().get()));

  }

  @Test
  public void testGeoWebSigmetToIWXXM21_draft() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/draft.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM21 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM21Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM21_STRING;
    final ConversionResult<String> resultAsIWXXM21 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM21Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM21.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM21.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM21 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM21.getStatus());

    String sigmetIWXXM21 = Tools.readResource("./geowebsigmets/draft-FMI.IWXXM21");
    String resultAsIWXXM21AsString = resultAsIWXXM21.getConvertedMessage().get();
    assertFalse(iwxxmHasDifferences(sigmetIWXXM21, resultAsIWXXM21AsString));
  }

  @Test
  public void testGeoWebSigmetToIWXXM21_moving() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/moving.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM21 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM21Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM21_STRING;
    final ConversionResult<String> resultAsIWXXM21 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM21Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM21.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM21.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM21 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM21.getStatus());

    String sigmetIWXXM21 = Tools.readResource("./geowebsigmets/moving-FMI.IWXXM21");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM21, resultAsIWXXM21.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetToIWXXM21_between_SFC() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-between-SFC.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM21 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM21Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM21_STRING;
    final ConversionResult<String> resultAsIWXXM21 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM21Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM21.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM21.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM21 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM21.getStatus());

    String sigmetIWXXM21 = Tools.readResource("./geowebsigmets/sigmet-between-SFC-FMI.IWXXM21");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM21, resultAsIWXXM21.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetToIWXXM21_twolevels_ds() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/twolevels-ds.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM21 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM21Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM21_STRING;
    final ConversionResult<String> resultAsIWXXM21 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM21Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM21.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM21.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM21 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM21.getStatus());
    String sigmetIWXXM21 = Tools.readResource("./geowebsigmets/twolevels-ds-FMI.IWXXM21");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM21, resultAsIWXXM21.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetToIWXXM21_vafields() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-vafields.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM21 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM21Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM21_STRING;
    final ConversionResult<String> resultAsIWXXM21 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM21Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM21.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM21.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM21 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM21.getStatus());
    String sigmetIWXXM21 = Tools.readResource("./geowebsigmets/sigmet-vafields-FMI.IWXXM21");
assertFalse(iwxxmHasDifferences(sigmetIWXXM21, resultAsIWXXM21.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetToIWXXM21_vafields_withcoordinates() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-vafields-withcoordinates.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM21 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM21Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM21_STRING;
    final ConversionResult<String> resultAsIWXXM21 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM21Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM21.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM21.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }
    /* Assure IWXXM21 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM21.getStatus());
    String sigmetIWXXM21 = Tools.readResource("./geowebsigmets/sigmet-vafields-withcoordinates-FMI.IWXXM21");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM21, resultAsIWXXM21.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetVaCld() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-vacld.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM21 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM21Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM21_STRING;
    final ConversionResult<String> resultAsIWXXM21 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM21Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM21.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM21.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM21 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM21.getStatus());
    String sigmetIWXXM21 = Tools.readResource("./geowebsigmets/sigmet-vacld-FMI.IWXXM21");
assertFalse(iwxxmHasDifferences(sigmetIWXXM21, resultAsIWXXM21.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetVaCancel() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-vacancel.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM21 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM21Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM21_STRING;
    final ConversionResult<String> resultAsIWXXM21 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM21Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM21.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM21.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM21 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM21.getStatus());
    String sigmetIWXXM21 = Tools.readResource("./geowebsigmets/sigmet-vacancel-FMI.IWXXM21");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM21, resultAsIWXXM21.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetRdoactCldKnmi() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-rdoact-cld-KNMI.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM21 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM21Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM21_STRING;
    final ConversionResult<String> resultAsIWXXM21 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM21Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM21.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM21.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM21 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM21.getStatus());
    String sigmetIWXXM21 = Tools.readResource("./geowebsigmets/sigmet-rdoact-cld-KNMI.IWXXM21");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM21, resultAsIWXXM21.getConvertedMessage().get()));
  }
  @Test
  public void testGeoWebSigmetRdoactCldFmi() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-rdoact-cld-FMI.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM21 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM21Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM21_STRING;
    final ConversionResult<String> resultAsIWXXM21 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM21Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM21.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM21.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM21 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM21.getStatus());
    String sigmetIWXXM21 = Tools.readResource("./geowebsigmets/sigmet-rdoact-cld-FMI.IWXXM21");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM21, resultAsIWXXM21.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetRdoactMetno() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-rdoact-cld-METNO.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM21 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM21Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM21_STRING;
    final ConversionResult<String> resultAsIWXXM21 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM21Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM21.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM21.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM21 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM21.getStatus());
    String sigmetIWXXM21 = Tools.readResource("./geowebsigmets/sigmet-rdoact-cld-METNO.IWXXM21");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM21, resultAsIWXXM21.getConvertedMessage().get()));
  }
}
