package nl.knmi.geoweb.backend.product.GeoWebSigmet;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.diff.Diff;

import fi.fmi.avi.converter.AviMessageConverter;
import fi.fmi.avi.converter.ConversionHints;
import fi.fmi.avi.converter.ConversionIssue;
import fi.fmi.avi.converter.ConversionResult;
import fi.fmi.avi.converter.ConversionSpecification;
import fi.fmi.avi.converter.iwxxm.conf.IWXXMConverter;
import fi.fmi.avi.model.sigmet.SIGMET;
import lombok.extern.slf4j.Slf4j;
import nl.knmi.geoweb.TestConfig;
import nl.knmi.geoweb.backend.aviation.FIRStore;
import nl.knmi.geoweb.backend.aviation.Tools;
import nl.knmi.geoweb.backend.converters.SigmetConverters.GeoWebSigmetToFMISigmet;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.om.CreateSigmetObjectMapper;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { TestConfig.class })
public class TestGeoWebSigmetToFMIIWXXM30 {
  @Autowired
  FIRStore firStore;

  @Autowired
  @Qualifier("geowebAviMessageConverter")
  private AviMessageConverter geowebAviMessageConverter;

  /**
   * Compare two IWXXM documents and check if they are equal, leaving out unique id's.
   * @param expected
   * @param result
   * @return
   */
  static public Boolean iwxxmHasDifferences (String expected, String result) {
    Diff xmlDiff = DiffBuilder.compare(expected)
    .withTest(result)
    .withAttributeFilter(attr -> {
      return !(attr.getName().equals("gml:id") || attr.getName().equals("xlink:href") || attr.getName().equals("volcanoId"));
    }).withNodeFilter(node -> {
      return !(node.getNodeName().equals("gml:timePosition"));
    })
    .ignoreComments()
    .ignoreWhitespace()
    .build();
    if (xmlDiff.hasDifferences()) {
      log.error(xmlDiff.toString());
    }
    return xmlDiff.hasDifferences();
  }

  @Test
  public void testGeoWebSigmetToIWXXM30() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/published.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM30 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM30Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM30_STRING;
    final ConversionResult<String> resultAsIWXXM30 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM30Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM30.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM30.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }
    /* Assure IWXXM30 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM30.getStatus());

    String sigmetIWXXM30 = Tools.readResource("./geowebsigmets/published-FMI.IWXXM30") ;

    assertFalse(iwxxmHasDifferences(sigmetIWXXM30, resultAsIWXXM30.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetToIWXXM30_cancel() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/cancels.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM30*/
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM30Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM30_STRING;
    final ConversionResult<String> resultAsIWXXM30 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM30Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM30.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM30.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM30 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM30.getStatus());

    String sigmetIWXXM30 = Tools.readResource("./geowebsigmets/cancels-FMI.IWXXM30");
    sigmetIWXXM30 = sigmetIWXXM30.replace("\n", "\r\n");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM30, resultAsIWXXM30.getConvertedMessage().get()));

  }

  @Test
  public void testGeoWebSigmetToIWXXM30_draft() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/draft.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM30 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM30Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM30_STRING;
    final ConversionResult<String> resultAsIWXXM30 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM30Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM30.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM30.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM30 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM30.getStatus());

    String sigmetIWXXM30 = Tools.readResource("./geowebsigmets/draft-FMI.IWXXM30");
    String resultAsIWXXM30AsString = resultAsIWXXM30.getConvertedMessage().get();
    assertFalse(iwxxmHasDifferences(sigmetIWXXM30, resultAsIWXXM30AsString));
  }

  @Test
  public void testGeoWebSigmetToIWXXM30_moving() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/moving.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM30 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM30Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM30_STRING;
    final ConversionResult<String> resultAsIWXXM30 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM30Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM30.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM30.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM30 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM30.getStatus());

    String sigmetIWXXM30 = Tools.readResource("./geowebsigmets/moving-FMI.IWXXM30");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM30, resultAsIWXXM30.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetToIWXXM30_between_SFC() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-between-SFC.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM30 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM30Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM30_STRING;
    final ConversionResult<String> resultAsIWXXM30 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM30Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM30.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM30.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM30 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM30.getStatus());

    String sigmetIWXXM30 = Tools.readResource("./geowebsigmets/sigmet-between-SFC-FMI.IWXXM30");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM30, resultAsIWXXM30.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetToIWXXM30_twolevels_ds() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/twolevels-ds.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM30 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM30Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM30_STRING;
    final ConversionResult<String> resultAsIWXXM30 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM30Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM30.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM30.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM30 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM30.getStatus());
    String sigmetIWXXM30 = Tools.readResource("./geowebsigmets/twolevels-ds-FMI.IWXXM30");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM30, resultAsIWXXM30.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetToIWXXM30_vafields() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-vafields.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM30 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM30Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM30_STRING;
    final ConversionResult<String> resultAsIWXXM30 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM30Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM30.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM30.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM30 conversion is successful */
    /* In this particular case conversion should give a warning */
    assertEquals(ConversionResult.Status.WITH_WARNINGS, resultAsIWXXM30.getStatus());
    String sigmetIWXXM30 = Tools.readResource("./geowebsigmets/sigmet-vafields-FMI.IWXXM30");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM30, resultAsIWXXM30.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetToIWXXM30_vafields_withcoordinates() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-vafields-withcoordinates.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM30 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM30Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM30_STRING;
    final ConversionResult<String> resultAsIWXXM30 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM30Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM30.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM30.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM30 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM30.getStatus());
    String sigmetIWXXM30 = Tools.readResource("./geowebsigmets/sigmet-vafields-withcoordinates-FMI.IWXXM30");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM30, resultAsIWXXM30.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetVaCld() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-vacld.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM30 */
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM30Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM30_STRING;
    final ConversionResult<String> resultAsIWXXM30 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM30Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM30.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM30.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM30 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM30.getStatus());
    String sigmetIWXXM30 = Tools.readResource("./geowebsigmets/sigmet-vacld-FMI.IWXXM30");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM30, resultAsIWXXM30.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetVaCancel() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-vacancel.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM30*/
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM30Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM30_STRING;
    final ConversionResult<String> resultAsIWXXM30 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM30Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM30.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM30.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM30 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM30.getStatus());
    String sigmetIWXXM30 = Tools.readResource("./geowebsigmets/sigmet-vacancel-FMI.IWXXM30");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM30, resultAsIWXXM30.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetRdoactCldKnmi() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-rdoact-cld-KNMI.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM30*/
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM30Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM30_STRING;
    final ConversionResult<String> resultAsIWXXM30 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM30Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM30.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM30.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM30 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM30.getStatus());
    String sigmetIWXXM30 = Tools.readResource("./geowebsigmets/sigmet-rdoact-cld-KNMI.IWXXM30");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM30, resultAsIWXXM30.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetRdoactCldFmi() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-rdoact-cld-FMI.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM30*/
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM30Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM30_STRING;
    final ConversionResult<String> resultAsIWXXM30 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM30Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM30.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM30.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM30 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM30.getStatus());
    String sigmetIWXXM30 = Tools.readResource("./geowebsigmets/sigmet-rdoact-cld-FMI.IWXXM30");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM30, resultAsIWXXM30.getConvertedMessage().get()));
  }

  @Test
  public void testGeoWebSigmetRdoactCldMetno() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-rdoact-cld-METNO.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to IWXXM30*/
    final ConversionSpecification<SIGMET, String> sigmetToIWXXM30Spec = IWXXMConverter.SIGMET_POJO_TO_IWXXM30_STRING;
    final ConversionResult<String> resultAsIWXXM30 =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToIWXXM30Spec, ConversionHints.SIGMET);

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsIWXXM30.getStatus()) {
        for (ConversionIssue issue:resultAsIWXXM30.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure IWXXM30 conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsIWXXM30.getStatus());
    String sigmetIWXXM30 = Tools.readResource("./geowebsigmets/sigmet-rdoact-cld-METNO.IWXXM30");
    assertFalse(iwxxmHasDifferences(sigmetIWXXM30, resultAsIWXXM30.getConvertedMessage().get()));
  }

}
