package nl.knmi.geoweb.backend.product.GeoWebSigmet;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

import fi.fmi.avi.model.AviationCodeListUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import fi.fmi.avi.converter.AviMessageConverter;
import fi.fmi.avi.converter.ConversionHints;
import fi.fmi.avi.converter.ConversionResult;
import fi.fmi.avi.converter.ConversionSpecification;
import fi.fmi.avi.converter.tac.conf.TACConverter;
import fi.fmi.avi.model.PointGeometry;
import fi.fmi.avi.model.PolygonGeometry;
import fi.fmi.avi.model.sigmet.SIGMET;
import lombok.extern.slf4j.Slf4j;
import nl.knmi.geoweb.backend.ApplicationConfig;
import nl.knmi.geoweb.backend.aviation.FIRStore;
import nl.knmi.geoweb.backend.aviation.Tools;
import nl.knmi.geoweb.backend.converters.SigmetConverters.GeoWebSigmetToFMISigmet;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.om.CreateSigmetObjectMapper;

import static org.junit.Assert.*;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@Import(ApplicationConfig.class)
public class TestGeoWebSigmetToFMISigmet {
  @Autowired
  FIRStore firStore;

  @Autowired
  @Qualifier("sigmetObjectMapper")
  private ObjectMapper sigmetObjectMapper;

  @Test
  public void readPublishedSigmet() throws Exception {
    /* Read json and tac, convert json to tac, results should be equal */
    String sigmetJson = Tools.readResource("./geowebsigmets/published.json");

    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
       sigmetJson,
       GeoWebSigmet.class
     );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);
  }

  @Test
  public void readCancelSigmet() throws Exception {
    /* Read json and tac, convert json to tac, results should be equal */
    String sigmetJson = Tools.readResource("./geowebsigmets/cancels.json");

    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
       sigmetJson,
       GeoWebSigmet.class
     );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);
    assertEquals(fmiSigmet.getConvertedMessage().get().getCancelledReference().get().getSequenceNumber(), "A01");
  }

  @Test
  public void readCancelVASigmet() throws Exception {
    /* Read json and tac, convert json to tac, results should be equal */
    String sigmetJson = Tools.readResource("./geowebsigmets/cancelva.json");

    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
       sigmetJson,
       GeoWebSigmet.class
     );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);
  }

  @Test
  public void readMovingSigmet() throws Exception {
    /* Read json and tac, convert json to tac, results should be equal */
    String sigmetJson = Tools.readResource("./geowebsigmets/moving.json");

    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
       sigmetJson,
       GeoWebSigmet.class
     );
    ConversionResult<SIGMET> fmiSigmetConversion = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmetConversion.getStatus(), ConversionResult.Status.SUCCESS);
    assertEquals(fmiSigmetConversion.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getMovingSpeed().get().getValue(), Double.valueOf(3.0));
    assertEquals(fmiSigmetConversion.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getMovingSpeed().get().getUom(), "KMH");
    assertEquals(fmiSigmetConversion.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getMovingDirection().get().getValue(), Double.valueOf(22.5));
  }

  @Test
  public void readVAFieldsWithCoordsSigmet() throws Exception {
    /* Read json and tac, convert json to tac, results should be equal */
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-vafields-withcoordinates.json");

    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
       sigmetJson,
       GeoWebSigmet.class
     );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);
  }

  @Autowired
  @Qualifier("geowebAviMessageConverter")
  private AviMessageConverter geowebAviMessageConverter;

  @Test
  public void readPointSigmet() throws Exception {
    /* Read json and tac, convert json to tac, results should be equal */
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-point.json");

    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
       sigmetJson,
       GeoWebSigmet.class
     );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    final ConversionSpecification<SIGMET, String> sigmetToTacSpec = TACConverter.SIGMET_POJO_TO_TAC;
    geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToTacSpec, ConversionHints.EMPTY);
  }

  @Test
  public void readPointSigmetRounding() throws Exception {
    /* Read json and tac, convert json to tac, results should be equal */
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-point-rounding.json");

    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
       sigmetJson,
       GeoWebSigmet.class
     );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    //Test without hints: results in N56 without trailing 00
    final ConversionSpecification<SIGMET, String> sigmetToTacSpec = TACConverter.SIGMET_POJO_TO_TAC;
    ConversionResult<String> res = geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToTacSpec, ConversionHints.EMPTY);
    String tac = res.getConvertedMessage().get();
    log.info("TAC:"+tac);
    assertTrue(tac.endsWith("FCST AT 1459Z N56 E00501="));

    // Test with hint set: results in N5600 with trailing 00, the KNMI way.
    ConversionHints hints = new ConversionHints();
    hints.put(ConversionHints.KEY_COORDINATE_MINUTES, ConversionHints.VALUE_COORDINATE_MINUTES_INCLUDE_ZERO);
    ConversionResult<String> res2 = geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToTacSpec, hints);
    String tac2 = res2.getConvertedMessage().get();
    log.info("TAC:"+tac2);
    assertTrue(tac2.endsWith("FCST AT 1459Z N5600 E00501="));
  }

  @Test
  public void readVAFieldsSigmet() throws Exception {
    /* Read json and tac, convert json to tac, results should be equal */
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-vafields.json");

    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
       sigmetJson,
       GeoWebSigmet.class
     );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    final ConversionSpecification<SIGMET, String> sigmetToTacSpec = TACConverter.SIGMET_POJO_TO_TAC;
    ConversionResult<String> tac = geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToTacSpec, ConversionHints.EMPTY);
  }

  @Test
  public void readVAFieldsWithCoordinatesSigmet() throws Exception {
    /* Read json and tac, convert json to tac, results should be equal */
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-vafields-withcoordinates.json");

    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
       sigmetJson,
       GeoWebSigmet.class
     );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    final ConversionSpecification<SIGMET, String> sigmetToTacSpec = TACConverter.SIGMET_POJO_TO_TAC;
    ConversionResult<String> tac = geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToTacSpec, ConversionHints.EMPTY);
    assertTrue(tac.getConvertedMessage().get().contains("VA ERUPTION MT EYJAFJALLAJÖKULL PSN N6337 W01937 VA CLD"));
    assertTrue(tac.getConvertedMessage().get().contains("NO VA EXP"));
  }


  // Drawn:
  // [52.752303811296045, 2.381969736260152, 51.56384439662024, 0.6207187263244109, 51.636777020700315, 2.96905340623873, 52.752303811296045, 2.381969736260152]
  // N5245 E00223 - N5138 E00258 - N5134 E00037 - N5245 E00223
  // Intersection:
  // [52.274632333864965, 2.6333605219412513, 51.60946036704992, 2.0894980945086408, 51.636777, 2.969053, 52.274632333864965, 2.6333605219412513]
  // N5216 E00238 - N5138 E00258 - N5136 E00205 - N5216 E00238
  @Test
    public void read3IntersectionGeometrySigmet() throws Exception {
      /* The intersection polygon should be used in the TAC */
      String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-3-coordinates-geometry.json");

      ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
      GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
        sigmetJson,
        GeoWebSigmet.class
      );

      ConversionResult<SIGMET> fmiSigmetConversion = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
      assertEquals(fmiSigmetConversion.getStatus(), ConversionResult.Status.SUCCESS);
      assertEquals(fmiSigmetConversion.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getTacGeometry().get().getTacContent(), "WI N5216 E00238 - N5138 E00258 - N5137 E00205 - N5216 E00238");
      List<Double> intersectCoords = Arrays.asList(52.274632333864965, 2.6333605219412513, 51.636777, 2.969053, 51.60946036704992, 2.0894980945086408, 52.274632333864965, 2.6333605219412513);
      assertEquals(((PolygonGeometry) (fmiSigmetConversion.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getGeoGeometry().get())).getExteriorRingPositions(),intersectCoords);
    }


  // Drawn:
  // [1.8972202640043532, 52.00225609676087, 5.51192036851197, 49.11837323675346, 8.4778281465695, 51.86891924321323, 1.8972202640043532, 52.00225609676087]
  // N5200 E00154 - N5152 E00829 - N4907 E00531 - N5200 E00154
  // Intersection:
  // [2.4022909006635103, 51.99202219809572, 2.2079578858334536, 51.7543428380748, 2.5980337339471675, 51.44313219622742, 3.370001, 51.369722, 3.362223, 51.320002, 3.36389, 51.313608, 3.373613, 51.309999, 3.952501, 51.214441, 4.397501, 51.452776, 5.078611, 51.391665, 5.848333, 51.139444, 5.651667, 50.824717, 6.011797, 50.757273, 5.934168, 51.036386, 6.222223, 51.361666, 5.94639, 51.811663, 6.405001, 51.830828, 6.5285677285655845, 51.90841512510155, 2.4022909006635103, 51.99202219809572]
  @Test
  public void readMore6IntersectionGeometrySigmet() throws Exception {
    /* The drawn polygon should be used in the TAC */
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-more-6-coordinates-geometry.json");

    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
       sigmetJson,
       GeoWebSigmet.class
     );

     ConversionResult<SIGMET> fmiSigmetConversion = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
     assertEquals(fmiSigmetConversion.getStatus(), ConversionResult.Status.SUCCESS);
     assertEquals(fmiSigmetConversion.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getTacGeometry().get().getTacContent(), "WI N5200 E00154 - N5152 E00829 - N4907 E00531 - N5200 E00154");
     List<Double> intersectCoords = Arrays.asList(52.00225609676087, 1.8972202640043532, 51.86891924321323, 8.4778281465695, 49.11837323675346, 5.51192036851197, 52.00225609676087, 1.8972202640043532);
     assertEquals(((PolygonGeometry) (fmiSigmetConversion.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getGeoGeometry().get())).getExteriorRingPositions(),intersectCoords);
  }

  @Test
  public void readPointIntersectionGeometrySigmet() throws Exception {
    /* The drawn polygon should be used in the TAC */
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-points-geometry.json");

    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmetConversion = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmetConversion.getStatus(), ConversionResult.Status.SUCCESS);
    List<Double> intersectCoords = Arrays.asList(53.444948, 3.518629);
    assertEquals(((PointGeometry) (fmiSigmetConversion.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getGeoGeometry().get())).getCoordinates(),intersectCoords);
  }

  @Test
  public void readFIRGeometrySigmet() throws Exception {
    /* The drawn polygon should be used in the TAC */
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-FIR-geometry.json");

    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
       sigmetJson,
       GeoWebSigmet.class
     );

     ConversionResult<SIGMET> fmiSigmetConversion = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
     assertEquals(fmiSigmetConversion.getStatus(), ConversionResult.Status.SUCCESS);
     assertEquals(fmiSigmetConversion.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getTacGeometry().get().getTacContent(), "ENTIRE FIR");
     List<Double> intersectCoords = Arrays.asList(55.332644, 4.331914, 55.764314, 3.368817, 54.379261, 2.761908, 52.913554, 3.15576, 51.500002, 2.000002, 51.369722, 3.370001, 51.320002, 3.362223, 51.313608, 3.36389, 51.309999, 3.373613, 51.214441, 3.952501, 51.452776, 4.397501, 51.391665, 5.078611, 51.139444, 5.848333, 50.824717, 5.651667, 50.757273, 6.011797, 51.036386, 5.934168, 51.361666, 6.222223, 51.811663, 5.94639, 51.830828, 6.405001, 52.237764, 7.053095, 52.268885, 7.031389, 52.346109, 7.063612, 52.385828, 7.065557, 52.888887, 7.133055, 52.898244, 7.14218, 53.3, 7.191667, 53.666667, 6.5, 55.000002, 6.500002, 55.0, 5.0, 55.332644, 4.331914);
     assertEquals(((PolygonGeometry) (fmiSigmetConversion.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getGeoGeometry().get())).getExteriorRingPositions(),intersectCoords);
  }

  @Test
  public void readBoxOneSideGeometrySigmet() throws Exception {
    /* The drawn polygon should be used in the TAC */
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-box-one-side-geometry.json");

    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
       sigmetJson,
       GeoWebSigmet.class
     );

     ConversionResult<SIGMET> fmiSigmetConversion = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
     assertEquals(fmiSigmetConversion.getStatus(), ConversionResult.Status.SUCCESS);
     assertEquals(fmiSigmetConversion.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getTacGeometry().get().getTacContent(), "N OF N5506");
     List<Double> intersectCoords = Arrays.asList(55.102156117325436, 0.8555521943158428, 55.102156117325436, 8.282160619544873,  56.358231580380156, 8.282160619544873, 56.358231580380156, 0.8555521943158428, 55.102156117325436, 0.8555521943158428);
     assertEquals(((PolygonGeometry) (fmiSigmetConversion.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getGeoGeometry().get())).getExteriorRingPositions(),intersectCoords);
  }

  @Test
  public void readBoxTwoSideGeometrySigmet() throws Exception {
    /* The drawn polygon should be used in the TAC */
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-box-two-side-geometry.json");

    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
       sigmetJson,
       GeoWebSigmet.class
     );

     ConversionResult<SIGMET> fmiSigmetConversion = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
     assertEquals(ConversionResult.Status.SUCCESS, fmiSigmetConversion.getStatus());
     assertEquals("N OF N5400 AND E OF E00510", fmiSigmetConversion.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getTacGeometry().get().getTacContent());
     List<Double> intersectCoords = Arrays.asList(53.99557237183801, 5.170617168658404, 53.99557237183801, 8.810535922525597, 55.503182552990275, 8.810535922525597, 55.503182552990275, 5.170617168658404, 53.99557237183801, 5.170617168658404);
     assertEquals(((PolygonGeometry) (fmiSigmetConversion.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getGeoGeometry().get())).getExteriorRingPositions(),intersectCoords);
  }

  @Test
  public void readBoxThreeSideGeometrySigmet() throws Exception {
    /* The intersection polygon should be used in the TAC */
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-box-three-side-geometry.json");

    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
       sigmetJson,
       GeoWebSigmet.class
     );

     ConversionResult<SIGMET> fmiSigmetConversion = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
     assertEquals(fmiSigmetConversion.getStatus(), ConversionResult.Status.SUCCESS);
     assertEquals(fmiSigmetConversion.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getTacGeometry().get().getTacContent(), "WI N5320 E00421 - N5519 E00421 - N5500 E00500 - N5500 E00615 - N5320 E00615 - N5320 E00421");
     List<Double> intersectCoords = Arrays.asList(53.33467608564436, 4.348700030688394, 53.33467608564436, 6.256721958118777, 55.569626607203254, 6.256721958118777, 55.569626607203254, 4.348700030688394, 53.33467608564436, 4.348700030688394);
     assertEquals(((PolygonGeometry) (fmiSigmetConversion.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getGeoGeometry().get())).getExteriorRingPositions(),intersectCoords);
  }

  @Test
  public void readBoxThreeSide8IntersectionsGeometrySigmet() throws Exception {
    /* The drawn polygon should be used in the TAC */
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-box-three-side-eight-intersections-geometry.json");

    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
       sigmetJson,
       GeoWebSigmet.class
     );

     ConversionResult<SIGMET> fmiSigmetConversion = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
     assertEquals(fmiSigmetConversion.getStatus(), ConversionResult.Status.SUCCESS);
     assertEquals(fmiSigmetConversion.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getTacGeometry().get().getTacContent(), "WI N5112 E00149 - N5226 E00149 - N5226 E00629 - N5112 E00629 - N5112 E00149");
     List<Double> intersectCoords = Arrays.asList(51.19742239025377, 1.8242402497805006, 51.19742239025377, 6.491555426110208, 52.431323302349035, 6.491555426110208, 52.431323302349035, 1.8242402497805006, 51.19742239025377, 1.8242402497805006);
     assertEquals(((PolygonGeometry) (fmiSigmetConversion.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getGeoGeometry().get())).getExteriorRingPositions(),intersectCoords);
  }

  @Test
  public void readPolygonMultipleIntersectionsGeometrySigmet() throws Exception {
    /* The drawn polygon should be used in the TAC */
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-polygon-multiple-intersections-geometry.json");

    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
       sigmetJson,
       GeoWebSigmet.class
     );

     ConversionResult<SIGMET> fmiSigmetConversion = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
     assertEquals(fmiSigmetConversion.getStatus(), ConversionResult.Status.SUCCESS);
     assertEquals(fmiSigmetConversion.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getTacGeometry().get().getTacContent(), "WI N5118 E00253 - N5123 E00631 - N4949 E00526 - N5118 E00253");
     List<Double> intersectCoords = Arrays.asList(51.30765706651498, 2.880990855741944, 51.3810000779706, 6.520909609609139, 49.816056343989636, 5.434804820148765, 51.30765706651498, 2.880990855741944);
     assertEquals(((PolygonGeometry) (fmiSigmetConversion.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getGeoGeometry().get())).getExteriorRingPositions(),intersectCoords);
  }

  @Test
  public void readBoxMultipleIntersectionsGeometrySigmet() throws Exception {
    /* The drawn box should be used in the TAC */
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-box-multiple-intersections-geometry.json");

    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
       sigmetJson,
       GeoWebSigmet.class
     );

     ConversionResult<SIGMET> fmiSigmetConversion = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
     assertEquals(fmiSigmetConversion.getStatus(), ConversionResult.Status.SUCCESS);
     assertEquals(fmiSigmetConversion.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getTacGeometry().get().getTacContent(), "S OF N5120");
     List<Double> intersectCoords = Arrays.asList(50.549110289194275, 2.96905340623873, 50.549110289194275, 6.873159811596286, 51.32600382365317, 6.873159811596286, 51.32600382365317, 2.96905340623873, 50.549110289194275, 2.96905340623873);
     assertEquals(((PolygonGeometry) (fmiSigmetConversion.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getGeoGeometry().get())).getExteriorRingPositions(),intersectCoords);
  }

  @Test
  public void readBoxMultipleIntersectionsMultipleSidesGeometrySigmet() throws Exception {
    /* The drawn box should be used in the TAC */
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-box-multiple-intersections-mulitple-sides-geometry.json");

    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
       sigmetJson,
       GeoWebSigmet.class
     );

     ConversionResult<SIGMET> fmiSigmetConversion = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
     assertEquals(fmiSigmetConversion.getStatus(), ConversionResult.Status.SUCCESS);
     assertEquals(fmiSigmetConversion.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getTacGeometry().get().getTacContent(), "WI N5012 E00656 - N5012 E00605 - N5333 E00605 - N5333 E00656 - N5012 E00656");
     List<Double> intersectCoords = Arrays.asList(50.19338677117774, 6.931868178594142, 53.54450043024857,6.931868178594142, 53.54450043024857, 6.0805968571252, 50.19338677117774,  6.0805968571252, 50.19338677117774, 6.931868178594142);
     assertEquals(((PolygonGeometry) (fmiSigmetConversion.getConvertedMessage().get().getAnalysisGeometries().get().get(0).getGeometry().get().getGeoGeometry().get())).getExteriorRingPositions(),intersectCoords);
  }

  @Test
  public void readShortTestSigmet() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-short-test.json");

    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
            sigmetJson,
            GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmetConversion = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmetConversion.getStatus(), ConversionResult.Status.SUCCESS);
    assertTrue(fmiSigmetConversion.getConvertedMessage().isPresent());

    SIGMET sigmet = fmiSigmetConversion.getConvertedMessage().get();
    assertTrue(sigmet.getPhenomenon().isEmpty());
    assertEquals(sigmet.getPhenomenonType(), AviationCodeListUser.SigmetPhenomenonType.SIGMET);
  }

  @Test
  public void readMinimalVaTestSigmet() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-short-va-test.json");

    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
            sigmetJson,
            GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmetConversion = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmetConversion.getStatus(), ConversionResult.Status.SUCCESS);
    assertTrue(fmiSigmetConversion.getConvertedMessage().isPresent());

    SIGMET sigmet = fmiSigmetConversion.getConvertedMessage().get();
    assertTrue(sigmet.getPhenomenon().isEmpty());
    assertEquals(sigmet.getPhenomenonType(), AviationCodeListUser.SigmetPhenomenonType.VOLCANIC_ASH_SIGMET);
  }
}