package nl.knmi.geoweb.backend.product.GeoWebSigmet;

import static org.junit.jupiter.api.Assertions.assertEquals;


import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fi.fmi.avi.converter.AviMessageConverter;
import fi.fmi.avi.converter.ConversionHints;
import fi.fmi.avi.converter.ConversionIssue;
import fi.fmi.avi.converter.ConversionResult;
import fi.fmi.avi.converter.ConversionSpecification;
import fi.fmi.avi.converter.tac.conf.TACConverter;
import fi.fmi.avi.model.sigmet.SIGMET;
import lombok.extern.slf4j.Slf4j;
import nl.knmi.geoweb.TestConfig;
import nl.knmi.geoweb.backend.aviation.FIRStore;
import nl.knmi.geoweb.backend.aviation.Tools;
import nl.knmi.geoweb.backend.converters.SigmetConverters.GeoWebSigmetToFMISigmet;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.om.CreateSigmetObjectMapper;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { TestConfig.class })
public class TestGeoWebSigmetToFMITAC {
  @Autowired
  FIRStore firStore;

  @Autowired
  @Qualifier("geowebTafObjectMapper")
  private ObjectMapper geowebTafObjectMapper;


  @Autowired
  @Qualifier("geowebAviMessageConverter")
  private AviMessageConverter geowebAviMessageConverter;

  public ConversionHints getTokenizerParsingHints() {
    return new ConversionHints(ConversionHints.KEY_VALIDTIME_FORMAT,
        ConversionHints.VALUE_VALIDTIME_FORMAT_PREFER_LONG);
  }

  @Test
  public void testGeoWebSigmetToTAC() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/published.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to TAC */
    final ConversionSpecification<SIGMET, String> sigmetToTacSpec = TACConverter.SIGMET_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToTacSpec, getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());
    String sigmetTac = Tools.readResource("./geowebsigmets/published-FMI.tac");
    sigmetTac = sigmetTac.replace("\n", "\r\n");
    assertEquals(sigmetTac, resultAsTAC.getConvertedMessage().get());

  }
  @Test
  public void testGeoWebSigmetToTAC_cancel() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/cancels.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to TAC */
    final ConversionSpecification<SIGMET, String> sigmetToTacSpec = TACConverter.SIGMET_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToTacSpec, getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());
    String sigmetTac = Tools.readResource("./geowebsigmets/cancels-FMI.tac");
    sigmetTac = sigmetTac.replace("\n", "\r\n");
    assertEquals(sigmetTac, resultAsTAC.getConvertedMessage().get());
  }

  @Test
  public void testGeoWebSigmetToTAC_draft() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/draft.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to TAC */
    final ConversionSpecification<SIGMET, String> sigmetToTacSpec = TACConverter.SIGMET_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToTacSpec, getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());
    String sigmetTac = Tools.readResource("./geowebsigmets/draft-FMI.tac");
    sigmetTac = sigmetTac.replace("\n", "\r\n");
    assertEquals(sigmetTac, resultAsTAC.getConvertedMessage().get());
  }


  @Test
  public void testGeoWebSigmetToTAC_draft_nogeom() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/draft-nogeom.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.FAIL);
    assertEquals(fmiSigmet.getConversionIssues().get(0).getType(),ConversionIssue.Type.MISSING_DATA);
    assertEquals(fmiSigmet.getConversionIssues().get(0).getMessage(), "SIGMET start area missing");
  }
  
  @Test
  public void testGeoWebSigmetToTAC_moving_5knots() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/moving_5knots.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to TAC */
    final ConversionSpecification<SIGMET, String> sigmetToTacSpec = TACConverter.SIGMET_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToTacSpec, getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());
    String sigmetTac = Tools.readResource("./geowebsigmets/moving_5knots-FMI.tac");
    sigmetTac = sigmetTac.replace("\n", "\r\n");
    assertEquals(sigmetTac, resultAsTAC.getConvertedMessage().get());
  }

  @Test
  public void testGeoWebSigmetToTAC_moving() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/moving.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to TAC */
    final ConversionSpecification<SIGMET, String> sigmetToTacSpec = TACConverter.SIGMET_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToTacSpec, getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());
    String sigmetTac = Tools.readResource("./geowebsigmets/moving-FMI.tac");
    sigmetTac = sigmetTac.replace("\n", "\r\n");
    assertEquals(sigmetTac, resultAsTAC.getConvertedMessage().get());
  }

  @Test
  public void testGeoWebSigmetToTAC_between_SFC() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-between-SFC.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to TAC */
    final ConversionSpecification<SIGMET, String> sigmetToTacSpec = TACConverter.SIGMET_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToTacSpec, getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());
    String sigmetTac = Tools.readResource("./geowebsigmets/sigmet-between-SFC-FMI.tac");
    sigmetTac = sigmetTac.replace("\n", "\r\n");
    assertEquals(sigmetTac, resultAsTAC.getConvertedMessage().get());
  }

  @Test
  public void testGeoWebSigmetToTAC_twolevels_ds() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/twolevels-ds.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to TAC */
    final ConversionSpecification<SIGMET, String> sigmetToTacSpec = TACConverter.SIGMET_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToTacSpec, getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());
    String sigmetTac = Tools.readResource("./geowebsigmets/twolevels-ds-FMI.tac");
    sigmetTac = sigmetTac.replace("\n", "\r\n");
    assertEquals(sigmetTac, resultAsTAC.getConvertedMessage().get());
  }

  @Test
  public void testGeoWebSigmetToTAC_vafields() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-vafields.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to TAC */
    final ConversionSpecification<SIGMET, String> sigmetToTacSpec = TACConverter.SIGMET_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToTacSpec, getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());
    String sigmetTac = Tools.readResource("./geowebsigmets/sigmet-vafields-FMI.tac");
    sigmetTac = sigmetTac.replace("\n", "\r\n");
    assertEquals(sigmetTac, resultAsTAC.getConvertedMessage().get());
  }

  @Test
  public void testGeoWebSigmetToTAC_vafields_withcoordinates() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-vafields-withcoordinates.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to TAC */
    final ConversionSpecification<SIGMET, String> sigmetToTacSpec = TACConverter.SIGMET_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToTacSpec, getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());
    String sigmetTac = Tools.readResource("./geowebsigmets/sigmet-vafields-withcoordinates-FMI.tac");
    sigmetTac = sigmetTac.replace("\n", "\r\n");
    assertEquals(sigmetTac, resultAsTAC.getConvertedMessage().get());
  }

  @Test
  public void testGeoWebSigmetVaCld() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-vacld.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to TAC */
    final ConversionSpecification<SIGMET, String> sigmetToTacSpec = TACConverter.SIGMET_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToTacSpec, getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());
    String sigmetTac = Tools.readResource("./geowebsigmets/sigmet-vacld-FMI.tac");
    sigmetTac = sigmetTac.replace("\n", "\r\n");
    assertEquals(sigmetTac, resultAsTAC.getConvertedMessage().get());
  }

  @Test
  public void testGeoWebSigmetVaCancel() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-vacancel.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to TAC */
    final ConversionSpecification<SIGMET, String> sigmetToTacSpec = TACConverter.SIGMET_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToTacSpec, getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());
    String sigmetTac = Tools.readResource("./geowebsigmets/sigmet-vacancel-FMI.tac");
    sigmetTac = sigmetTac.replace("\n", "\r\n");
    assertEquals(sigmetTac, resultAsTAC.getConvertedMessage().get());
  }
  @Test
  public void testGeoWebSigmetRdoactCldKnmi() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-rdoact-cld-KNMI.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to TAC */
    final ConversionSpecification<SIGMET, String> sigmetToTacSpec = TACConverter.SIGMET_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToTacSpec, getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());
    String sigmetTac = Tools.readResource("./geowebsigmets/sigmet-rdoact-cld-KNMI.tac");
    sigmetTac = sigmetTac.replace("\n", "\r\n");
    assertEquals(sigmetTac, resultAsTAC.getConvertedMessage().get());
  }

  @Test
  public void testGeoWebSigmetRdoactCldFmi() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-rdoact-cld-FMI.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to TAC */
    final ConversionSpecification<SIGMET, String> sigmetToTacSpec = TACConverter.SIGMET_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToTacSpec, getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());
    String sigmetTac = Tools.readResource("./geowebsigmets/sigmet-rdoact-cld-FMI.tac");
    sigmetTac = sigmetTac.replace("\n", "\r\n");
    assertEquals(sigmetTac, resultAsTAC.getConvertedMessage().get());
  }

  @Test
  public void testGeoWebSigmetRdoactCldMetno() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-rdoact-cld-METNO.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
      sigmetJson,
      GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to TAC */
    final ConversionSpecification<SIGMET, String> sigmetToTacSpec = TACConverter.SIGMET_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToTacSpec, getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());
    String sigmetTac = Tools.readResource("./geowebsigmets/sigmet-rdoact-cld-METNO.tac");
    sigmetTac = sigmetTac.replace("\n", "\r\n");
    assertEquals(sigmetTac, resultAsTAC.getConvertedMessage().get());
  }

  @Test
  public void testGeoWebSigmetShortTest() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-short-test.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
            sigmetJson,
            GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to TAC */
    final ConversionSpecification<SIGMET, String> sigmetToTacSpec = TACConverter.SIGMET_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
            geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToTacSpec, getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
      for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
        log.warn(issue.toString());
      }
    }

    /* Assert TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());
    String sigmetTac = Tools.readResource("./geowebsigmets/sigmet-short-test.tac");
    sigmetTac = sigmetTac.replace("\n", "\r\n");
    assertEquals(sigmetTac, resultAsTAC.getConvertedMessage().get());
  }

  @Test
  public void testGeoWebSigmetShortVaTest() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-short-va-test.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
            sigmetJson,
            GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to TAC */
    final ConversionSpecification<SIGMET, String> sigmetToTacSpec = TACConverter.SIGMET_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
            geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToTacSpec, getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
      for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
        log.warn(issue.toString());
      }
    }

    /* Assert TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());
    String sigmetTac = Tools.readResource("./geowebsigmets/sigmet-short-va-test.tac");
    sigmetTac = sigmetTac.replace("\n", "\r\n");
    assertEquals(sigmetTac, resultAsTAC.getConvertedMessage().get());
  }

  @Test
  public void testGeoWebSigmetShortTestCancel() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-short-test-cancel.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
            sigmetJson,
            GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to TAC */
    final ConversionSpecification<SIGMET, String> sigmetToTacSpec = TACConverter.SIGMET_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
            geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToTacSpec, getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
      for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
        log.warn(issue.toString());
      }
    }

    /* Assert TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());
    String sigmetTac = Tools.readResource("./geowebsigmets/sigmet-short-test-cancel.tac");
    sigmetTac = sigmetTac.replace("\n", "\r\n");
    assertEquals(sigmetTac, resultAsTAC.getConvertedMessage().get());
  }

  @Test
  public void testGeoWebSigmetShortVaTestCancel() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/sigmet-short-va-test-cancel.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
            sigmetJson,
            GeoWebSigmet.class
    );

    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.SUCCESS);

    /* Now convert to TAC */
    final ConversionSpecification<SIGMET, String> sigmetToTacSpec = TACConverter.SIGMET_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
            geowebAviMessageConverter.convertMessage(fmiSigmet.getConvertedMessage().get(), sigmetToTacSpec, getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
      for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
        log.warn(issue.toString());
      }
    }

    /* Assert TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());
    String sigmetTac = Tools.readResource("./geowebsigmets/sigmet-short-va-test-cancel.tac");
    sigmetTac = sigmetTac.replace("\n", "\r\n");
    assertEquals(sigmetTac, resultAsTAC.getConvertedMessage().get());
  }

  @Test
  public void testGeoWebSigmetToTAC_draft_outsidefir_start() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/draft-outsidefir-start.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
            sigmetJson,
            GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.FAIL);
    assertEquals(fmiSigmet.getConversionIssues().get(0).getType(),ConversionIssue.Type.LOGICAL);
    assertEquals(fmiSigmet.getConversionIssues().get(0).getMessage(), "The start position needs to be (partly) inside the FIR");
  }

  @Test
  public void testGeoWebSigmetToTAC_draft_outsidefir_end() throws Exception {
    String sigmetJson = Tools.readResource("./geowebsigmets/draft-outsidefir-end.json");
    ObjectMapper geowebSigmetObjectMapper = CreateSigmetObjectMapper.getOm();
    GeoWebSigmet geowebSigmet = geowebSigmetObjectMapper.readValue(
            sigmetJson,
            GeoWebSigmet.class
    );
    ConversionResult<SIGMET> fmiSigmet = GeoWebSigmetToFMISigmet.convert(geowebSigmet, firStore);
    assertEquals(fmiSigmet.getStatus(), ConversionResult.Status.FAIL);
    assertEquals(fmiSigmet.getConversionIssues().get(0).getType(),ConversionIssue.Type.LOGICAL);
    assertEquals(fmiSigmet.getConversionIssues().get(0).getMessage(), "The end position needs to be (partly) inside the FIR");
  }

}
