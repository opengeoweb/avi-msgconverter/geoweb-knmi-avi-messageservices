package nl.knmi.geoweb.backend.product.GeoWebTAF.adverse;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fi.fmi.avi.converter.tac.lexer.Lexeme;
import fi.fmi.avi.converter.tac.lexer.LexemeIdentity;
import fi.fmi.avi.converter.tac.lexer.LexingFactory;
import nl.knmi.geoweb.TestConfig;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { TestConfig.class })
public class TestTacValue {

    @Autowired
    @Qualifier("lexingFactory")
    private LexingFactory lexingFactory;

    @Test
    public void testAdverseSurfaceWind() {
        String[] tokens = { "VRB20KT", "VRB20G20KT", "18099KT", "18099G99KT", "180P99KT", "180P99GP99KT" };
        for (String token : tokens) {
            Lexeme lexeme = lexingFactory.createLexeme(token,
                    LexemeIdentity.SURFACE_WIND);
            TacValue tacValue = new TacValue(lexeme);
            assert tacValue.get("text") == token;
            assert tacValue.containsKey("type") == true;
            assert tacValue.get("type") == "adverse";
        }
    }

    @Test
    public void testNonAdverseSurfaceWind() {
        String[] tokens = { "VRB19KT", "VRB18G19KT", "180P99MPS", "180P99GP99MPS" };
        for (String token : tokens) {
            Lexeme lexeme = lexingFactory.createLexeme(token, LexemeIdentity.SURFACE_WIND);
            TacValue tacValue = new TacValue(lexeme);
            assertEquals(token, tacValue.get("text"));
            assertFalse(tacValue.containsKey("type"));
        }
    }

    @Test
    public void testAdverseHorizontalVisibility() {
        String[] tokens = { "0000", "0050", "0100", "0150", "0200", "0250", "0300", "0350", "0400", "0450", "0500",
                "0550", "0600", "0650", "0700", "0750", "0800", "0850", "0900", "0950", "1000" };
        for (String token : tokens) {
            Lexeme lexeme = lexingFactory.createLexeme(token,
                    LexemeIdentity.HORIZONTAL_VISIBILITY);
            TacValue tacValue = new TacValue(lexeme);
            assert tacValue.get("text") == token;
            assert tacValue.containsKey("type") == true;
            assert tacValue.get("type") == "adverse";
        }
    }

    @Test
    public void testNonAdverseHorizontalVisibility() {
        String[] tokens = { "1050", "1100", "1150", "1200", "1250" };
        for (String token : tokens) {
            Lexeme lexeme = lexingFactory.createLexeme(token,
                    LexemeIdentity.HORIZONTAL_VISIBILITY);
            TacValue tacValue = new TacValue(lexeme);
            assert tacValue.get("text") == token;
            assert tacValue.containsKey("type") == false;
        }
    }

    @Test
    public void testAdverseWeather() {
        String[] tokens = { "TS", "FG", "FZ", "SN", "PL", "FC", "SQ", "SS", "DS", "WS", "GS", "GR", "+RA", "MIFG", "FGMI" };
        for (String token : tokens) {
            Lexeme lexeme = lexingFactory.createLexeme(token, LexemeIdentity.WEATHER);
            TacValue tacValue = new TacValue(lexeme);
            assert tacValue.get("text") == token;
            assert tacValue.containsKey("type") == true;
            assert tacValue.get("type") == "adverse";
        }
    }

    @Test
    public void testNonAdverseWeather() {
        String[] tokens = { "DZ", "RA", "DZSGRA", "-RA" };
        for (String token : tokens) {
            Lexeme lexeme = lexingFactory.createLexeme(token, LexemeIdentity.WEATHER);
            TacValue tacValue = new TacValue(lexeme);
            assert tacValue.get("text") == token;
            assert tacValue.containsKey("type") == false;
        }
    }

    @Test
    public void testAdverseCloud() {
        String[] tokens = { "VV000", "VV001", "VV002", "VV003", "VV//" };
        for (String token : tokens) {
            Lexeme lexeme = lexingFactory.createLexeme(token, LexemeIdentity.CLOUD);
            TacValue tacValue = new TacValue(lexeme);
            assert tacValue.get("text") == token;
            assert tacValue.containsKey("type") == true;
            assert tacValue.get("type") == "adverse";
        }
    }

    @Test
    public void testNonAdverseCloud() {
        String[] tokens = { "VV004", "VV005", "VV006" };
        for (String token : tokens) {
            Lexeme lexeme = lexingFactory.createLexeme(token, LexemeIdentity.CLOUD);
            TacValue tacValue = new TacValue(lexeme);
            assert tacValue.get("text") == token;
            assert tacValue.containsKey("type") == false;
        }
    }
}
