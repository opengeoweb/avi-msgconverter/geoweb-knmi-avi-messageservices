package nl.knmi.geoweb.backend.product.taf;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;

@JsonPropertyOrder(alphabetic = true)
@JsonInclude(Include.NON_NULL)
@Getter
@Setter
class DateTester {
  @JsonFormat(shape = JsonFormat.Shape.STRING)
  public OffsetDateTime dateToTest;
}
