package nl.knmi.geoweb.backend.product.taf;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fi.fmi.avi.converter.AviMessageConverter;
import fi.fmi.avi.converter.ConversionHints;
import fi.fmi.avi.converter.ConversionResult;
import fi.fmi.avi.converter.ConversionSpecification;
import fi.fmi.avi.converter.json.conf.JSONConverter;
import fi.fmi.avi.converter.tac.conf.TACConverter;
import fi.fmi.avi.model.taf.TAF;
import nl.knmi.geoweb.TestConfig;
import nl.knmi.geoweb.backend.aviation.Tools;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { TestConfig.class })
public class FMITafToTACTest {

  @Autowired
  @Qualifier("tafObjectMapper")
  private ObjectMapper tafObjectMapper;

  @Autowired
  @Qualifier("geowebAviMessageConverter")
  private AviMessageConverter geowebAviMessageConverter;

  // @Test
  // public void testTAFBulletin() {

  //   final TAFBulletin bulletin = TAFBulletinImpl.builder().setHeading(BulletinHeadingImpl.builder()
  //       .setLocationIndicator("EHDB").setBulletinNumber(99).setGeographicalDesignator("NL")
  //       .setDataTypeDesignatorT2(
  //           DataTypeDesignatorT2.ForecastsDataTypeDesignatorT2.FCT_AERODROME_VT_LONG)
  //       .setIssueTime(PartialOrCompleteTimeInstant.createIssueTime("020500")).build()).build();

  //   final ConversionResult<String> tacResult = geowebAviMessageConverter.convertMessage(bulletin,
  //       TACConverter.TAF_BULLETIN_POJO_TO_TAC);
  //   assertTrue(tacResult.getConversionIssues().isEmpty());
  //   final Optional<String> tacBulletin = tacResult.getConvertedMessage();

  //   assertTrue(tacBulletin.isPresent());
  //   assertEquals(CARRIAGE_RETURN.getContent() + CARRIAGE_RETURN.getContent() + LINE_FEED.getContent()
  //       + "FTNL99 EHDB 020500", tacBulletin.get());
  // }

  public ConversionHints getTokenizerParsingHints() {
    return new ConversionHints(ConversionHints.KEY_VALIDTIME_FORMAT,
        ConversionHints.VALUE_VALIDTIME_FORMAT_PREFER_LONG);
  }

  @Test
  public void tesFMIAVIJSONtoTAF() throws IOException {
    String json = Tools.readResource("./taf/avitaf/taf11.json");
    assertNotNull(json);
    ConversionResult<TAF> resultAsTAF = geowebAviMessageConverter.
      convertMessage(json, JSONConverter.JSON_STRING_TO_TAF_POJO, ConversionHints.EMPTY);
    assertTrue(ConversionResult.Status.SUCCESS == resultAsTAF.getStatus());
}

  @Test
  public void testFMIAVIJSONtoTAFtoTAC() throws IOException {
    final ConversionSpecification<String, TAF> jsonToTafSpec = JSONConverter.JSON_STRING_TO_TAF_POJO;
    final ConversionSpecification<TAF, String> tafToTacSpec = TACConverter.TAF_POJO_TO_TAC;
    String json = Tools.readResource("./taf/avitaf/taf11.json");
    String tac = Tools.readResource("./taf/avitaf/taf11.tac");
    final ConversionResult<TAF> resultAsTAF =
      (ConversionResult<TAF>)geowebAviMessageConverter.convertMessage(json, jsonToTafSpec, getTokenizerParsingHints());

    assertTrue(ConversionResult.Status.SUCCESS == resultAsTAF.getStatus());

    TAF taf = resultAsTAF.getConvertedMessage().get();

    final ConversionResult<String> resultAsTAC =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(taf, tafToTacSpec, getTokenizerParsingHints());

      String tafAsTac = resultAsTAC.getConvertedMessage().get().replaceAll("\\r", "");
    assertEquals(tac, tafAsTac);
  }
}
