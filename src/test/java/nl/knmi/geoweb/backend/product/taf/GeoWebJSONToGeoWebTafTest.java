package nl.knmi.geoweb.backend.product.taf;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import nl.knmi.geoweb.TestConfig;
import nl.knmi.geoweb.backend.aviation.Tools;
import nl.knmi.geoweb.backend.converters.TAFConverters.GeoWebTAFJsonToGeoWebTaf;
import nl.knmi.geoweb.backend.product.GeoWebTAF.GeoWebTAF;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { TestConfig.class })
public class GeoWebJSONToGeoWebTafTest {

  @Autowired
  @Qualifier("geowebTafObjectMapper")
  private ObjectMapper geowebTafObjectMapper;


  @Test
  public void checkGeowebTAFObjectMapper() throws IOException {
    assertNotNull(geowebTafObjectMapper);
  }

  @Test
  public void deserializeDateString() throws IOException {
    DateTester dateToTest = geowebTafObjectMapper.readValue(
      "{\"dateToTest\":\"2020-12-16T18:00:00Z\"}",
      DateTester.class
    );
    assertEquals("2020-12-16T18:00Z", dateToTest.dateToTest.toString());
  }

  public void testGeoWebTAFJSONtoGeoWebTAFtoGeoWebTAFJSONHelper (String json) throws JsonMappingException, JsonProcessingException {
    assertNotNull(json);
    GeoWebTAF geoWebTaF = (new GeoWebTAFJsonToGeoWebTaf()).convert(json, geowebTafObjectMapper);
    assertNotNull(geoWebTaF);

    String convertedJSON = (new GeoWebTAFJsonToGeoWebTaf()).convert(geoWebTaF, geowebTafObjectMapper);

    JsonNode actualObj1 = new ObjectMapper().readTree(json);
    JsonNode actualObj2 = geowebTafObjectMapper.readTree(convertedJSON);

    String s1 = new SortedNode().convertNode(actualObj1);
    String s2 = new SortedNode().convertNode(actualObj2);

    assertEquals(s1, s2);
  }

  @Test
  public void testGeoWebTAFJSONtoGeoWebTAFtoGeoWebTAFJSON() throws Exception {
    String json = Tools.readResource("./taf/geowebtaf/geowebtaf.json");
    testGeoWebTAFJSONtoGeoWebTAFtoGeoWebTAFJSONHelper(json);
  }


  @Test
  public void testConversionWithCavOK() throws Exception {
    String json = Tools.readResource("./taf/geowebtaf/withCavok.json");
    testGeoWebTAFJSONtoGeoWebTAFtoGeoWebTAFJSONHelper(json);
  }

}
