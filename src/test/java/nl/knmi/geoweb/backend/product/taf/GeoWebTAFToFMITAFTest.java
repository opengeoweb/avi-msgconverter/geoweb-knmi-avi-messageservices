package nl.knmi.geoweb.backend.product.taf;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fi.fmi.avi.converter.AviMessageConverter;
import fi.fmi.avi.converter.ConversionIssue;
import fi.fmi.avi.converter.ConversionResult;
import fi.fmi.avi.converter.ConversionSpecification;
import fi.fmi.avi.converter.tac.conf.TACConverter;
import fi.fmi.avi.model.AviationCodeListUser.CloudAmount;
import fi.fmi.avi.model.AviationCodeListUser.CloudType;
import fi.fmi.avi.model.AviationWeatherMessage.ReportStatus;
import fi.fmi.avi.model.taf.TAF;
import fi.fmi.avi.model.taf.TAFChangeForecast;
import lombok.extern.slf4j.Slf4j;
import nl.knmi.geoweb.TestConfig;
import nl.knmi.geoweb.backend.aviation.Tools;
import nl.knmi.geoweb.backend.converters.TAFConverters.GeoWebTAFToFMITAF;
import nl.knmi.geoweb.backend.converters.TAFConverters.TafUtils;
import nl.knmi.geoweb.backend.product.GeoWebTAF.GeoWebTAF;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { TestConfig.class })
public class GeoWebTAFToFMITAFTest {

  @Autowired
  @Qualifier("geowebTafObjectMapper")
  private ObjectMapper geowebTafObjectMapper;


  @Autowired
  @Qualifier("geowebAviMessageConverter")
  private AviMessageConverter geowebAviMessageConverter;


  @Test
  public void checkGeowebTAFObjectMapper() throws IOException {
    assertNotNull(geowebTafObjectMapper);
  }



  @Test
  public void testGeoWebTAFToFMITAF() throws Exception {
    GeoWebTAF geoWebTaF = TafUtils.getGeoWebTafFromResource("./taf/geowebtaf/geowebtaf.json",geowebTafObjectMapper);
    ConversionResult<TAF> conversionResultfmiTaf = (new GeoWebTAFToFMITAF()).convert(geoWebTaF, geowebTafObjectMapper, TafUtils.getTokenizerParsingHints());;
    TAF fmiTAF = conversionResultfmiTaf.getConvertedMessage().get();
    assertNotNull(fmiTAF);
  }

  @Test
  public void testGeoWebTAFNoIssueTimeToFMITAF() throws Exception {
    GeoWebTAF geoWebTaF = TafUtils.getGeoWebTafFromResource("./taf/geowebtaf/geowebtafnoissuetime.json",geowebTafObjectMapper);
    ConversionResult<TAF> conversionResultfmiTaf = (new GeoWebTAFToFMITAF()).convert(geoWebTaF, geowebTafObjectMapper, TafUtils.getTokenizerParsingHints());;
    TAF fmiTAF = conversionResultfmiTaf.getConvertedMessage().get();
    assertNotNull(fmiTAF);
  }

  @Test
  public void  testSmallTAFGeoWebTAFToTAC() throws Exception {
    /* Load the GeoWeb Taf JSON and convert it to a GeoWeb TAF POJO */
    GeoWebTAF geoWebTaF = TafUtils.getGeoWebTafFromResource("./taf/geowebtaf/smallvalidtafwithbaseforecast.json", geowebTafObjectMapper);

    /* Convert the GeoWeb TAF Pojo to the FMI TAF Pojo */
    ConversionResult<TAF> conversionResultfmiTaf = (new GeoWebTAFToFMITAF()).convert(geoWebTaF, geowebTafObjectMapper, TafUtils.getTokenizerParsingHints());;
    TAF fmiTAF = conversionResultfmiTaf.getConvertedMessage().get();

    /* Check not null */
    assertNotNull(fmiTAF);

    /* Check issueTime */
    assertEquals("2020-12-16T18:00Z", fmiTAF.getIssueTime().get().getCompleteTime().get().toString());

    /* Check validDateStart */
    assertEquals("2020-12-16T18:00Z", fmiTAF.getValidityTime().get().getStartTime().get().getCompleteTime().get().toString());

    /* Check validDateEnd */
    assertEquals("2020-12-18T00:00Z", fmiTAF.getValidityTime().get().getEndTime().get().getCompleteTime().get().toString());

    /* Check location */
    assertEquals("EHAM", fmiTAF.getAerodrome().getDesignator());

    /* Check FMI status which is very different in GeoWeb */
    assertEquals(ReportStatus.NORMAL, fmiTAF.getReportStatus());

    /* TODO check type (NORMAL) */

    /* TODO check messageType (ORG) */

    /* Check BaseForecast NSW */
    assertFalse(fmiTAF.getBaseForecast().get().isNoSignificantWeather());

    /* Check BaseForecast CavOK */
    assertFalse(fmiTAF.getBaseForecast().get().isCeilingAndVisibilityOk());

    /* TODO check if validatestart is same as start in baseforecast */

    /* Check visibility range */
    assertEquals(6000, fmiTAF.getBaseForecast().get().getPrevailingVisibility().get().getValue());

    /* Check visibility unit */
    assertEquals("m", fmiTAF.getBaseForecast().get().getPrevailingVisibility().get().getUom());

    /* Check wind direction */
    assertEquals(180.0, fmiTAF.getBaseForecast().get().getSurfaceWind().get().getMeanWindDirection().get().getValue());

    /* Check wind direction unit */
    assertEquals("deg", fmiTAF.getBaseForecast().get().getSurfaceWind().get().getMeanWindDirection().get().getUom());

    /* Check wind speed */
    assertEquals(10.0, fmiTAF.getBaseForecast().get().getSurfaceWind().get().getMeanWindSpeed().getValue());

    /* Check wind speed unit */
    assertEquals("[kn_i]", fmiTAF.getBaseForecast().get().getSurfaceWind().get().getMeanWindSpeed().getUom());

    /* Check cloud type NSC */
    assertFalse(fmiTAF.getBaseForecast().get().getCloud().get().isNoSignificantCloud());

    /* Check cloud type NSC */
    // TODO , this is actually missing: assertTrue(fmiTAF.getBaseForecast().get().getCloud().get().isVerticalVisibilityMissing());

    /* Check nr of cloud layers is 1*/
    assertEquals(1, fmiTAF.getBaseForecast().get().getCloud().get().getLayers().get().size());

    /* Check first cloud layer is BKN */
    assertEquals(CloudAmount.OVC, fmiTAF.getBaseForecast().get().getCloud().get().getLayers().get().get(0).getAmount().get());

    /* Check first cloud layer height*/
    assertEquals(2000, fmiTAF.getBaseForecast().get().getCloud().get().getLayers().get().get(0).getBase().get().getValue());

    /* Check first cloud layer appendix*/
    assertEquals(CloudType.CB, fmiTAF.getBaseForecast().get().getCloud().get().getLayers().get().get(0).getCloudType().get());

    /* Check first cloud layer unit*/
    assertEquals("[ft_i]", fmiTAF.getBaseForecast().get().getCloud().get().getLayers().get().get(0).getBase().get().getUom());

    /* Check there is 1 weather layer */
    assertEquals(1, fmiTAF.getBaseForecast().get().getForecastWeather().get().size());

    /* Check first weather code is SHRA */
    assertEquals("SHRA", fmiTAF.getBaseForecast().get().getForecastWeather().get().get(0).getCode());

    /* Now convert to TAC */
    final ConversionSpecification<TAF, String> tafToTacSpec = TACConverter.TAF_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiTAF, tafToTacSpec, TafUtils.getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());

    /* Make sure the TAC is the same as always */
    String tac = Tools.readResource("./taf/geowebtaf/smallvalidtafwithbaseforecast.tac");
    assertEquals(tac, resultAsTAC.getConvertedMessage().get());


  }


  @Test
  public void testTAFWithChangeGroupsToTAC() throws Exception {
    /* Load the GeoWeb Taf JSON and convert it to a GeoWeb TAF POJO */
    GeoWebTAF geoWebTaF = TafUtils.getGeoWebTafFromResource("./taf/geowebtaf/geowebtaf.json",geowebTafObjectMapper);

    /* Convert the GeoWeb TAF Pojo to the FMI TAF Pojo */
    ConversionResult<TAF> conversionResultfmiTaf = (new GeoWebTAFToFMITAF()).convert(geoWebTaF, geowebTafObjectMapper, TafUtils.getTokenizerParsingHints());;
    TAF fmiTAF = conversionResultfmiTaf.getConvertedMessage().get();

    /* Check not null */
    assertNotNull(fmiTAF);

    /* Check issueTime */
    assertEquals("2020-12-16T18:00Z", fmiTAF.getIssueTime().get().getCompleteTime().get().toString());

    /* Check validDateStart */
    assertEquals("2020-12-16T18:00Z", fmiTAF.getValidityTime().get().getStartTime().get().getCompleteTime().get().toString());

    /* Check validDateEnd */
    assertEquals("2020-12-18T00:00Z", fmiTAF.getValidityTime().get().getEndTime().get().getCompleteTime().get().toString());

    /* Check location */
    assertEquals("EHAM", fmiTAF.getAerodrome().getDesignator());

    /* Check FMI status which is very different in GeoWeb */
    assertEquals(ReportStatus.NORMAL, fmiTAF.getReportStatus());

    /* TODO check type (NORMAL) */

    /* TODO check messageType (ORG) */

    /* Check BaseForecast NSW */
    assertFalse(fmiTAF.getBaseForecast().get().isNoSignificantWeather());

    /* Check BaseForecast CavOK */
    assertFalse(fmiTAF.getBaseForecast().get().isCeilingAndVisibilityOk());

    /* TODO check if validatestart is same as start in baseforecast */

    /* Check visibility range */
    assertEquals(6000, fmiTAF.getBaseForecast().get().getPrevailingVisibility().get().getValue());

    /* Check visibility unit */
    assertEquals("m", fmiTAF.getBaseForecast().get().getPrevailingVisibility().get().getUom());

    /* Check wind direction */
    assertEquals(180.0, fmiTAF.getBaseForecast().get().getSurfaceWind().get().getMeanWindDirection().get().getValue());

    /* Check wind direction unit */
    assertEquals("deg", fmiTAF.getBaseForecast().get().getSurfaceWind().get().getMeanWindDirection().get().getUom());

    /* Check wind speed */
    assertEquals(10.0, fmiTAF.getBaseForecast().get().getSurfaceWind().get().getMeanWindSpeed().getValue());

    /* Check wind speed unit */
    assertEquals("[kn_i]", fmiTAF.getBaseForecast().get().getSurfaceWind().get().getMeanWindSpeed().getUom());

    /* Check cloud type NSC */
    assertFalse(fmiTAF.getBaseForecast().get().getCloud().get().isNoSignificantCloud());

    /* Check nr of cloud layers is 2*/
    assertEquals(2, fmiTAF.getBaseForecast().get().getCloud().get().getLayers().get().size());

    /* Check first cloud layer is BKN */
    assertEquals(CloudAmount.BKN, fmiTAF.getBaseForecast().get().getCloud().get().getLayers().get().get(0).getAmount().get());

    /* Check first cloud layer height*/
    assertEquals(3500, fmiTAF.getBaseForecast().get().getCloud().get().getLayers().get().get(0).getBase().get().getValue());

    /* Check first cloud layer type is not set*/
    assertTrue(fmiTAF.getBaseForecast().get().getCloud().get().getLayers().get().get(0).getCloudType().isEmpty());

    /* Check first cloud layer unit*/
    assertEquals("[ft_i]", fmiTAF.getBaseForecast().get().getCloud().get().getLayers().get().get(0).getBase().get().getUom());

    /* Check there are 2 weather layers */
    assertEquals(2, fmiTAF.getBaseForecast().get().getForecastWeather().get().size());

    /* Check first weather code is SHRA */
    assertEquals("SHRA", fmiTAF.getBaseForecast().get().getForecastWeather().get().get(0).getCode());

    /* ============= First changegroup ================= */
    /* Check number of changegroups */
    assertEquals(3, fmiTAF.getChangeForecasts().get().size());

    TAFChangeForecast firstChangeGroup = fmiTAF.getChangeForecasts().get().get(0);
    /* Check firstChangeGroup NSW */
    assertFalse(firstChangeGroup.isNoSignificantWeather());

    /* Check firstChangeGroup CavOK */
    assertFalse(firstChangeGroup.isCeilingAndVisibilityOk());

    /* TODO check if validatestart is same as start in baseforecast */

    /* Check visibility range */
    assertEquals(1000, firstChangeGroup.getPrevailingVisibility().get().getValue());

    /* Check visibility unit */
    assertEquals("m", firstChangeGroup.getPrevailingVisibility().get().getUom());

    /* Check wind direction */
    assertEquals(180.0, firstChangeGroup.getSurfaceWind().get().getMeanWindDirection().get().getValue());

    /* Check wind direction unit */
    assertEquals("deg", firstChangeGroup.getSurfaceWind().get().getMeanWindDirection().get().getUom());

    /* Check wind speed */
    assertEquals(10.0, firstChangeGroup.getSurfaceWind().get().getMeanWindSpeed().getValue());

    /* Check wind speed unit */
    assertEquals("[kn_i]", firstChangeGroup.getSurfaceWind().get().getMeanWindSpeed().getUom());

    /* Check cloud type NSC */
    assertFalse(firstChangeGroup.getCloud().get().isNoSignificantCloud());

    /* Check cloud type NSC */
    // TODO , this is actually missing: assertTrue(firstChangeGroup.getCloud().get().isVerticalVisibilityMissing());

    /* Check nr of cloud layers is 1*/
    assertEquals(1, firstChangeGroup.getCloud().get().getLayers().get().size());

    /* Check first cloud layer is BKN */
    assertEquals(CloudAmount.BKN, firstChangeGroup.getCloud().get().getLayers().get().get(0).getAmount().get());

    /* Check first cloud layer height*/
    assertEquals(3500, firstChangeGroup.getCloud().get().getLayers().get().get(0).getBase().get().getValue());

    /* Check first cloud layer type is not set*/
    assertTrue(firstChangeGroup.getCloud().get().getLayers().get().get(0).getCloudType().isEmpty());

    /* Check first cloud layer unit*/
    assertEquals("[ft_i]", firstChangeGroup.getCloud().get().getLayers().get().get(0).getBase().get().getUom());

    /* Check there is 1 weather layer */
    assertEquals(1, firstChangeGroup.getForecastWeather().get().size());

    /* Check first weather code is SHRA */
    assertEquals("MIFG", firstChangeGroup.getForecastWeather().get().get(0).getCode());

    /* ============= Second changegroup ================= */

    TAFChangeForecast thirdChangeGroup = fmiTAF.getChangeForecasts().get().get(2);
    /* Check BaseForecast NSW */
    assertFalse(thirdChangeGroup.isNoSignificantWeather());

    /* Check BaseForecast CavOK */
    assertFalse(thirdChangeGroup.isCeilingAndVisibilityOk());

    /* TODO check if validatestart is same as start in baseforecast */

    /* Check visibility range */
    assertEquals(6000, thirdChangeGroup.getPrevailingVisibility().get().getValue());

    /* Check visibility unit */
    assertEquals("m", thirdChangeGroup.getPrevailingVisibility().get().getUom());

    /* Check wind direction present */
    assertFalse(thirdChangeGroup.getSurfaceWind().isPresent());

    /* Check cloud type NSC */
    assertFalse(thirdChangeGroup.getCloud().get().isNoSignificantCloud());

    /* Check cloud type VV */
    // TODO , this is actually missing: assertTrue(secondChangeGroup.getCloud().get().isVerticalVisibilityMissing());

    /* Check nr of cloud layers is 2 in secondChangeGroup*/
    assertEquals(2, thirdChangeGroup.getCloud().get().getLayers().get().size());

    /* Check first cloud layer is BKN */
    assertEquals(CloudAmount.BKN, thirdChangeGroup.getCloud().get().getLayers().get().get(0).getAmount().get());

    /* Check first cloud layer height*/
    assertEquals(4000, thirdChangeGroup.getCloud().get().getLayers().get().get(0).getBase().get().getValue());

    /* Check first cloud layer type is not set*/
    assertTrue(thirdChangeGroup.getCloud().get().getLayers().get().get(0).getCloudType().isEmpty());

    /* Check first cloud layer unit*/
    assertEquals("[ft_i]", thirdChangeGroup.getCloud().get().getLayers().get().get(0).getBase().get().getUom());

    /* Check second cloud layer is OVC */
    assertEquals(CloudAmount.OVC, thirdChangeGroup.getCloud().get().getLayers().get().get(1).getAmount().get());

    /* Check second cloud layer height*/
    assertEquals(6000, thirdChangeGroup.getCloud().get().getLayers().get().get(1).getBase().get().getValue());

    /* Check second cloud layer type is CB*/
    assertEquals(CloudType.CB, thirdChangeGroup.getCloud().get().getLayers().get().get(1).getCloudType().get());

    /* Check second cloud layer unit*/
    assertEquals("[ft_i]", thirdChangeGroup.getCloud().get().getLayers().get().get(1).getBase().get().getUom());

    /* Check there are 2 weather layer2 */
    assertEquals(2, thirdChangeGroup.getForecastWeather().get().size());

    /* Check first weather code is SHRA */
    assertEquals("+SHRA", thirdChangeGroup.getForecastWeather().get().get(0).getCode());

    /* Now convert to TAC */
    final ConversionSpecification<TAF, String> tafToTacSpec = TACConverter.TAF_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiTAF, tafToTacSpec, TafUtils.getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());

    /* Make sure the TAC is the same as always */
    String tac = Tools.readResource("./taf/geowebtaf/geowebtaf.tac");
    assertEquals(tac, resultAsTAC.getConvertedMessage().get().replaceAll("\\r", ""));


  }






}
