package nl.knmi.geoweb.backend.product.taf;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class SortedNode {
  private static final ObjectMapper SORTED_MAPPER = new ObjectMapper();
  static {
      SORTED_MAPPER.configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true);
  }

  public String convertNode(final JsonNode node) throws JsonProcessingException {
      final Object obj = SORTED_MAPPER.treeToValue(node, Object.class);
      final String json = SORTED_MAPPER.writeValueAsString(obj);
      return json;
  }
}
