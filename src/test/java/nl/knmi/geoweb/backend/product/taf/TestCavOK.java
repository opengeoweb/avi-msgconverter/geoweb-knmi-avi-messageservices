package nl.knmi.geoweb.backend.product.taf;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fi.fmi.avi.converter.AviMessageConverter;
import fi.fmi.avi.converter.ConversionIssue;
import fi.fmi.avi.converter.ConversionResult;
import fi.fmi.avi.converter.ConversionSpecification;
import fi.fmi.avi.converter.tac.conf.TACConverter;
import fi.fmi.avi.model.taf.TAF;
import lombok.extern.slf4j.Slf4j;
import nl.knmi.geoweb.TestConfig;
import nl.knmi.geoweb.backend.aviation.Tools;
import nl.knmi.geoweb.backend.converters.TAFConverters.GeoWebTAFToFMITAF;
import nl.knmi.geoweb.backend.converters.TAFConverters.TafUtils;
import nl.knmi.geoweb.backend.product.GeoWebTAF.GeoWebTAF;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { TestConfig.class })
public class TestCavOK {

  @Autowired
  @Qualifier("geowebTafObjectMapper")
  private ObjectMapper geowebTafObjectMapper;


  @Autowired
  @Qualifier("geowebAviMessageConverter")
  private AviMessageConverter geowebAviMessageConverter;


  @Test
  public void testCavOKInBaseForecast() throws Exception {
    /* Load the GeoWeb Taf JSON and convert it to a GeoWeb TAF POJO */
    GeoWebTAF geoWebTaF = TafUtils.getGeoWebTafFromResource("./taf/geowebtaf/withCavok.json", geowebTafObjectMapper);

    /* Convert the GeoWeb TAF Pojo to the FMI TAF Pojo */
    ConversionResult<TAF> conversionResultfmiTaf = (new GeoWebTAFToFMITAF()).convert(geoWebTaF, geowebTafObjectMapper, TafUtils.getTokenizerParsingHints());
    /* If GeoWeb JSON to FMI Model conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != conversionResultfmiTaf.getStatus()) {
      for (ConversionIssue issue:conversionResultfmiTaf.getConversionIssues()) {
        log.warn(issue.toString());
      }
    }

    /* Make sure conversion succeeded */
    assertTrue(ConversionResult.Status.SUCCESS == conversionResultfmiTaf.getStatus());

    /* Shorthand to FMI taf model */
    TAF fmiTAF = conversionResultfmiTaf.getConvertedMessage().get();
      /* Check BaseForecast CavOK */
    assertTrue(fmiTAF.getBaseForecast().get().isCeilingAndVisibilityOk());

    /* Now convert to TAC */
    final ConversionSpecification<TAF, String> tafToTacSpec = TACConverter.TAF_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiTAF, tafToTacSpec, TafUtils.getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());

    /* Make sure the TAC is the same as always */
    String tac = Tools.readResource("./taf/geowebtaf/withCavok.tac");
    assertEquals(tac, resultAsTAC.getConvertedMessage().get().replaceAll("\\r", ""));


  }


}
