package nl.knmi.geoweb.backend.product.taf;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fi.fmi.avi.converter.AviMessageConverter;
import fi.fmi.avi.converter.ConversionIssue;
import fi.fmi.avi.converter.ConversionResult;
import fi.fmi.avi.model.taf.TAF;
import lombok.extern.slf4j.Slf4j;
import nl.knmi.geoweb.TestConfig;
import nl.knmi.geoweb.backend.converters.TAFConverters.GeoWebTAFToFMITAF;
import nl.knmi.geoweb.backend.converters.TAFConverters.TafUtils;
import nl.knmi.geoweb.backend.product.GeoWebTAF.GeoWebTAF;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { TestConfig.class })
public class TestInvalidTafs {

  @Autowired
  @Qualifier("geowebTafObjectMapper")
  private ObjectMapper geowebTafObjectMapper;
  

  @Autowired
  @Qualifier("geowebAviMessageConverter")
  private AviMessageConverter geowebAviMessageConverter;

  @Test
  public void testFromAndProb30() throws Exception {
    /* Load the GeoWeb Taf JSON and convert it to a GeoWeb TAF POJO */
    GeoWebTAF geoWebTaF = TafUtils.getGeoWebTafFromResource("./taf/geowebtaf/testinvalidtaf-fromprob30.json", geowebTafObjectMapper);
    
    /* Convert the GeoWeb TAF Pojo to the FMI TAF Pojo */
    ConversionResult<TAF> conversionResultfmiTaf = (new GeoWebTAFToFMITAF()).convert(geoWebTaF, geowebTafObjectMapper, TafUtils.getTokenizerParsingHints());
    /* If GeoWeb JSON to FMI Model conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != conversionResultfmiTaf.getStatus()) {
      for (ConversionIssue issue:conversionResultfmiTaf.getConversionIssues()) {
        log.warn(issue.toString());
      }
    }

    /* Make sure conversion failed */
    assertTrue(ConversionResult.Status.FAIL == conversionResultfmiTaf.getStatus());

    assertEquals("updateChangeForecasts: Combination of PROB30 and FM is not allowed.", conversionResultfmiTaf.getConversionIssues().get(0).getMessage());
  }


}
