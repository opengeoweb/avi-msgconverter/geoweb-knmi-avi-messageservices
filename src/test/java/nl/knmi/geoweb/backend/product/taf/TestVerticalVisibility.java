package nl.knmi.geoweb.backend.product.taf;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fi.fmi.avi.converter.AviMessageConverter;
import fi.fmi.avi.converter.ConversionIssue;
import fi.fmi.avi.converter.ConversionResult;
import fi.fmi.avi.converter.ConversionSpecification;
import fi.fmi.avi.converter.tac.conf.TACConverter;
import fi.fmi.avi.model.taf.TAF;
import fi.fmi.avi.model.taf.TAFChangeForecast;
import lombok.extern.slf4j.Slf4j;
import nl.knmi.geoweb.TestConfig;
import nl.knmi.geoweb.backend.aviation.Tools;
import nl.knmi.geoweb.backend.converters.TAFConverters.GeoWebTAFToFMITAF;
import nl.knmi.geoweb.backend.converters.TAFConverters.TafUtils;
import nl.knmi.geoweb.backend.product.GeoWebTAF.GeoWebTAF;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { TestConfig.class })
public class TestVerticalVisibility {

  @Autowired
  @Qualifier("geowebTafObjectMapper")
  private ObjectMapper geowebTafObjectMapper;
  

  @Autowired
  @Qualifier("geowebAviMessageConverter")
  private AviMessageConverter geowebAviMessageConverter;

  @Test
  public void testVerticalVisibilityTafBaseforecast() throws Exception {
    /* Load the GeoWeb Taf JSON and convert it to a GeoWeb TAF POJO */
    GeoWebTAF geoWebTaF = TafUtils.getGeoWebTafFromResource("./taf/geowebtaf/verticalvisibilitytaf-baseforecast.json", geowebTafObjectMapper);
    
    /* Convert the GeoWeb TAF Pojo to the FMI TAF Pojo */
    ConversionResult<TAF> conversionResultfmiTaf = (new GeoWebTAFToFMITAF()).convert(geoWebTaF, geowebTafObjectMapper, TafUtils.getTokenizerParsingHints());
    /* If GeoWeb JSON to FMI Model conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != conversionResultfmiTaf.getStatus()) {
      for (ConversionIssue issue:conversionResultfmiTaf.getConversionIssues()) {
        log.warn(issue.toString());
      }
    }

    /* Make sure conversion succeeded */
    assertTrue(ConversionResult.Status.SUCCESS == conversionResultfmiTaf.getStatus());

    /* Shorthand to FMI taf model */
    TAF fmiTAF = conversionResultfmiTaf.getConvertedMessage().get();
    
    /* Check BaseForecast CavOK */
    assertFalse(fmiTAF.getBaseForecast().get().isCeilingAndVisibilityOk());

    /* Check baseforecast vertical visibility missing */
    assertFalse(fmiTAF.getBaseForecast().get().getCloud().get().isVerticalVisibilityMissing());

    /* Check baseforecast vertical visibility value */
    assertEquals(500.0, fmiTAF.getBaseForecast().get().getCloud().get().getVerticalVisibility().get().getValue());

    /* Check baseforecast vertical visibility units */
    assertEquals("[ft_i]", fmiTAF.getBaseForecast().get().getCloud().get().getVerticalVisibility().get().getUom());

    /* Get first changegroup */
    TAFChangeForecast firstChangeGroup = fmiTAF.getChangeForecasts().get().get(0);
    
    /* Check firstChangeGroup vertical visibility missing */
    // TODO: Check why this is not true
    assertFalse(firstChangeGroup.getCloud().get().isVerticalVisibilityMissing());


    /* Now convert to TAC */
    final ConversionSpecification<TAF, String> tafToTacSpec = TACConverter.TAF_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC = 
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiTAF, tafToTacSpec, TafUtils.getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }
  
    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());
       
    /* Print TAC */
    System.out.println("resultAsTAC ["+resultAsTAC.getConvertedMessage().get()+"]");

    /* Make sure the TAC is the same as always */
    String tac = Tools.readResource("./taf/geowebtaf/verticalvisibilitytaf-baseforecast.tac");
    assertEquals(tac, resultAsTAC.getConvertedMessage().get().replaceAll("\\r", ""));
  }

  @Test
  public void testVerticalVisibilityTafChangeGroup() throws Exception {
    /* Load the GeoWeb Taf JSON and convert it to a GeoWeb TAF POJO */
    GeoWebTAF geoWebTaF = TafUtils.getGeoWebTafFromResource("./taf/geowebtaf/verticalvisibilitytaf-changegroup.json", geowebTafObjectMapper);
    
    /* Convert the GeoWeb TAF Pojo to the FMI TAF Pojo */
    ConversionResult<TAF> conversionResultfmiTaf = (new GeoWebTAFToFMITAF()).convert(geoWebTaF, geowebTafObjectMapper, TafUtils.getTokenizerParsingHints());
    /* If GeoWeb JSON to FMI Model conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != conversionResultfmiTaf.getStatus()) {
      for (ConversionIssue issue:conversionResultfmiTaf.getConversionIssues()) {
        log.warn(issue.toString());
      }
    }

    /* Make sure conversion succeeded */
    assertTrue(ConversionResult.Status.SUCCESS == conversionResultfmiTaf.getStatus());

    /* Shorthand to FMI taf model */
    TAF fmiTAF = conversionResultfmiTaf.getConvertedMessage().get();
    
    /* Check BaseForecast CavOK */
    assertFalse(fmiTAF.getBaseForecast().get().isCeilingAndVisibilityOk());

    /* Get first changegroup */
    TAFChangeForecast firstChangeGroup = fmiTAF.getChangeForecasts().get().get(0);
    
    /* Check firstChangeGroup vertical visibility missing */
    assertFalse(firstChangeGroup.getCloud().get().isVerticalVisibilityMissing());

    /* Check firstChangeGroup vertical visibility value */
    assertEquals(500.0, firstChangeGroup.getCloud().get().getVerticalVisibility().get().getValue());

    /* Check firstChangeGroup vertical visibility units */
    assertEquals("[ft_i]", firstChangeGroup.getCloud().get().getVerticalVisibility().get().getUom());

    /* Now convert to TAC */
    final ConversionSpecification<TAF, String> tafToTacSpec = TACConverter.TAF_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC = 
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiTAF, tafToTacSpec, TafUtils.getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }
  
    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());
       
    /* Print TAC */
    System.out.println("resultAsTAC ["+resultAsTAC.getConvertedMessage().get()+"]");

    /* Make sure the TAC is the same as always */
    String tac = Tools.readResource("./taf/geowebtaf/verticalvisibilitytaf-changegroup.tac");
    assertEquals(tac, resultAsTAC.getConvertedMessage().get().replaceAll("\\r", ""));
  }

}
