package nl.knmi.geoweb.backend.product.taf;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fi.fmi.avi.converter.AviMessageConverter;
import fi.fmi.avi.converter.ConversionIssue;
import fi.fmi.avi.converter.ConversionResult;
import fi.fmi.avi.converter.ConversionSpecification;
import fi.fmi.avi.converter.tac.conf.TACConverter;
import fi.fmi.avi.model.AviationCodeListUser;
import fi.fmi.avi.model.taf.TAF;
import fi.fmi.avi.model.taf.TAFChangeForecast;
import lombok.extern.slf4j.Slf4j;
import nl.knmi.geoweb.TestConfig;
import nl.knmi.geoweb.backend.aviation.Tools;
import nl.knmi.geoweb.backend.converters.TAFConverters.GeoWebTAFToFMITAF;
import nl.knmi.geoweb.backend.converters.TAFConverters.TafUtils;
import nl.knmi.geoweb.backend.product.GeoWebTAF.GeoWebTAF;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { TestConfig.class })
public class TestWind {

  @Autowired
  @Qualifier("geowebTafObjectMapper")
  private ObjectMapper geowebTafObjectMapper;


  @Autowired
  @Qualifier("geowebAviMessageConverter")
  private AviMessageConverter geowebAviMessageConverter;

  @Test
  public void testWindInBaseForecast() throws Exception {
    /* Load the GeoWeb Taf JSON and convert it to a GeoWeb TAF POJO */
    GeoWebTAF geoWebTaF = TafUtils.getGeoWebTafFromResource("./taf/geowebtaf/wind-baseforecast-taf.json", geowebTafObjectMapper);

    /* Convert the GeoWeb TAF Pojo to the FMI TAF Pojo */
    ConversionResult<TAF> conversionResultfmiTaf = (new GeoWebTAFToFMITAF()).convert(geoWebTaF, geowebTafObjectMapper, TafUtils.getTokenizerParsingHints());
    /* If GeoWeb JSON to FMI Model conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != conversionResultfmiTaf.getStatus()) {
      for (ConversionIssue issue:conversionResultfmiTaf.getConversionIssues()) {
        log.warn(issue.toString());
      }
    }

    /* Make sure conversion succeeded */
    assertTrue(ConversionResult.Status.SUCCESS == conversionResultfmiTaf.getStatus());

    /* Shorthand to FMI taf model */
    TAF fmiTAF = conversionResultfmiTaf.getConvertedMessage().get();
    /* Check BaseForecast CavOK */
    assertFalse(fmiTAF.getBaseForecast().get().isCeilingAndVisibilityOk());

    /* Check BaseForecast NSC */
    assertFalse(fmiTAF.getBaseForecast().get().getCloud().get().isNoSignificantCloud());

    /* Check baseforecast wind */
    assertEquals(180.0, fmiTAF.getBaseForecast().get().getSurfaceWind().get().getMeanWindDirection().get().getValue());

    assertEquals(200.0, fmiTAF.getBaseForecast().get().getSurfaceWind().get().getMeanWindSpeed().getValue());

    assertEquals(20.0, fmiTAF.getBaseForecast().get().getSurfaceWind().get().getWindGust().get().getValue());

    /* Check NSC in changegroup */
    TAFChangeForecast firstChangeGroup = fmiTAF.getChangeForecasts().get().get(0);
    /* Check firstChangeGroup NSW */
    assertFalse(firstChangeGroup.isNoSignificantWeather());

    /* Check firstChangeGroup CavOK */
    assertFalse(firstChangeGroup.isCeilingAndVisibilityOk());

    /* Check NSC in first changegroup*/
    assertFalse(firstChangeGroup.getCloud().get().isNoSignificantCloud());

    /* Check NSC in second changegroup*/
    TAFChangeForecast secondChangeGroup = fmiTAF.getChangeForecasts().get().get(1);
    assertFalse(secondChangeGroup.getCloud().get().isNoSignificantCloud());



    /* Now convert to TAC */
    final ConversionSpecification<TAF, String> tafToTacSpec = TACConverter.TAF_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiTAF, tafToTacSpec, TafUtils.getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());

    /* Print TAC */
    System.out.println("resultAsTAC ["+resultAsTAC.getConvertedMessage().get()+"]");

    /* Make sure the TAC is the same as always */
    String tac = Tools.readResource("./taf/geowebtaf/wind-baseforecast-taf.tac");
    assertEquals(tac, resultAsTAC.getConvertedMessage().get().replaceAll("\\r", ""));
  }


  @Test
  public void testWindVRBInBaseForecast() throws Exception {
    /* Load the GeoWeb Taf JSON and convert it to a GeoWeb TAF POJO */
    GeoWebTAF geoWebTaF = TafUtils.getGeoWebTafFromResource("./taf/geowebtaf/windvrb-inbaseforecast.json", geowebTafObjectMapper);

    /* Convert the GeoWeb TAF Pojo to the FMI TAF Pojo */
    ConversionResult<TAF> conversionResultfmiTaf = (new GeoWebTAFToFMITAF()).convert(geoWebTaF, geowebTafObjectMapper, TafUtils.getTokenizerParsingHints());
    /* If GeoWeb JSON to FMI Model conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != conversionResultfmiTaf.getStatus()) {
      for (ConversionIssue issue:conversionResultfmiTaf.getConversionIssues()) {
        log.warn(issue.toString());
      }
    }

    /* Make sure conversion succeeded */
    assertTrue(ConversionResult.Status.SUCCESS == conversionResultfmiTaf.getStatus());

    /* Shorthand to FMI taf model */
    TAF fmiTAF = conversionResultfmiTaf.getConvertedMessage().get();
    /* Check BaseForecast CavOK */
    assertTrue(fmiTAF.getBaseForecast().get().isCeilingAndVisibilityOk());

    /* Check BaseForecast NSC */
    assertFalse(fmiTAF.getBaseForecast().get().getCloud().isPresent());

    /* Check baseforecast wind */
    assertTrue( fmiTAF.getBaseForecast().get().getSurfaceWind().get().isVariableDirection());

    assertEquals(20.0, fmiTAF.getBaseForecast().get().getSurfaceWind().get().getMeanWindSpeed().getValue());

    /* Now convert to TAC */
    final ConversionSpecification<TAF, String> tafToTacSpec = TACConverter.TAF_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiTAF, tafToTacSpec, TafUtils.getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());

    /* Print TAC */
    System.out.println("resultAsTAC ["+resultAsTAC.getConvertedMessage().get()+"]");

    /* Make sure the TAC is the same as always */
    String tac = "TAF EHGG 271404Z 2718/2824 VRB20KT CAVOK=";
    assertEquals(tac, resultAsTAC.getConvertedMessage().get().replaceAll("\\r", ""));
  }

  @Test
  public void testWindVRBInChangeGroup() throws Exception {
    /* Load the GeoWeb Taf JSON and convert it to a GeoWeb TAF POJO */
    GeoWebTAF geoWebTaF = TafUtils.getGeoWebTafFromResource("./taf/geowebtaf/windvrb-inchangegroup.json", geowebTafObjectMapper);

    /* Convert the GeoWeb TAF Pojo to the FMI TAF Pojo */
    ConversionResult<TAF> conversionResultfmiTaf = (new GeoWebTAFToFMITAF()).convert(geoWebTaF, geowebTafObjectMapper, TafUtils.getTokenizerParsingHints());
    /* If GeoWeb JSON to FMI Model conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != conversionResultfmiTaf.getStatus()) {
      for (ConversionIssue issue:conversionResultfmiTaf.getConversionIssues()) {
        log.warn(issue.toString());
      }
    }

    /* Make sure conversion succeeded */
    assertTrue(ConversionResult.Status.SUCCESS == conversionResultfmiTaf.getStatus());

    /* Shorthand to FMI taf model */
    TAF fmiTAF = conversionResultfmiTaf.getConvertedMessage().get();
    /* Check BaseForecast CavOK */
    assertTrue(fmiTAF.getBaseForecast().get().isCeilingAndVisibilityOk());

    /* Check BaseForecast NSC */
    assertFalse(fmiTAF.getBaseForecast().get().getCloud().isPresent());

    /* Check baseforecast wind */
    assertFalse( fmiTAF.getBaseForecast().get().getSurfaceWind().get().isVariableDirection());

    assertEquals(20.0, fmiTAF.getBaseForecast().get().getSurfaceWind().get().getMeanWindSpeed().getValue());

    /* Check changegroup wind */
    TAFChangeForecast firstChangeGroup = fmiTAF.getChangeForecasts().get().get(0);

    /* Check wind direction */
    assertTrue(firstChangeGroup.getSurfaceWind().get().isVariableDirection());

    /* Now convert to TAC */
    final ConversionSpecification<TAF, String> tafToTacSpec = TACConverter.TAF_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiTAF, tafToTacSpec, TafUtils.getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());

    /* Print TAC */
    System.out.println("resultAsTAC ["+resultAsTAC.getConvertedMessage().get()+"]");

    /* Make sure the TAC is the same as always */
    String tac = "TAF EHAM 262321Z 2718/2824 20020KT CAVOK\n"+
      "FM272000 VRB20G35KT CAVOK=";
    assertEquals(tac, resultAsTAC.getConvertedMessage().get().replaceAll("\\r", ""));
  }

  @Test
  public void testWindP99InBaseForecast() throws Exception {
    /* Load the GeoWeb Taf JSON and convert it to a GeoWeb TAF POJO */
    GeoWebTAF geoWebTaF = TafUtils.getGeoWebTafFromResource("./taf/geowebtaf/windp99-inbaseforecast.json", geowebTafObjectMapper);

    /* Convert the GeoWeb TAF Pojo to the FMI TAF Pojo */
    ConversionResult<TAF> conversionResultfmiTaf = (new GeoWebTAFToFMITAF()).convert(geoWebTaF, geowebTafObjectMapper, TafUtils.getTokenizerParsingHints());
    /* If GeoWeb JSON to FMI Model conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != conversionResultfmiTaf.getStatus()) {
      for (ConversionIssue issue:conversionResultfmiTaf.getConversionIssues()) {
        log.warn(issue.toString());
      }
    }

    /* Make sure conversion succeeded */
    assertTrue(ConversionResult.Status.SUCCESS == conversionResultfmiTaf.getStatus());

    /* Shorthand to FMI taf model */
    TAF fmiTAF = conversionResultfmiTaf.getConvertedMessage().get();
    /* Check BaseForecast CavOK */
    assertTrue(fmiTAF.getBaseForecast().get().isCeilingAndVisibilityOk());

    /* Check BaseForecast NSC */
    assertFalse(fmiTAF.getBaseForecast().get().getCloud().isPresent());

    /* Check baseforecast windgust */
    assertEquals( AviationCodeListUser.RelationalOperator.ABOVE, fmiTAF.getBaseForecast().get().getSurfaceWind().get().getWindGustOperator().get());
    assertEquals(99.0, fmiTAF.getBaseForecast().get().getSurfaceWind().get().getWindGust().get().getValue());

    /* Check MeanWindSpeed windgust */
    assertEquals( AviationCodeListUser.RelationalOperator.ABOVE, fmiTAF.getBaseForecast().get().getSurfaceWind().get().getMeanWindSpeedOperator().get());
    assertEquals(99.0, fmiTAF.getBaseForecast().get().getSurfaceWind().get().getMeanWindSpeed().getValue());

    TAFChangeForecast firstChangeGroup = fmiTAF.getChangeForecasts().get().get(0);

    /* Check changegroup windgust */
    assertEquals( AviationCodeListUser.RelationalOperator.ABOVE, firstChangeGroup.getSurfaceWind().get().getWindGustOperator().get());
    assertEquals(99.0, firstChangeGroup.getSurfaceWind().get().getWindGust().get().getValue());

    /* Check MeanWindSpeed windgust */
    assertEquals( AviationCodeListUser.RelationalOperator.ABOVE, firstChangeGroup.getSurfaceWind().get().getMeanWindSpeedOperator().get());
    assertEquals(99.0, firstChangeGroup.getSurfaceWind().get().getMeanWindSpeed().getValue());


  //   /* Check wind direction */
  //   assertTrue(firstChangeGroup.getSurfaceWind().get().isVariableDirection());

    /* Now convert to TAC */
    final ConversionSpecification<TAF, String> tafToTacSpec = TACConverter.TAF_POJO_TO_TAC;
    final ConversionResult<String> resultAsTAC =
      (ConversionResult<String>)geowebAviMessageConverter.convertMessage(fmiTAF, tafToTacSpec, TafUtils.getTokenizerParsingHints());

    /* If conversion fails, give at least a hint why */
    if (ConversionResult.Status.SUCCESS != resultAsTAC.getStatus()) {
        for (ConversionIssue issue:resultAsTAC.getConversionIssues()) {
            log.warn(issue.toString());
        }
    }

    /* Assure TAC conversion is successful */
    assertEquals(ConversionResult.Status.SUCCESS, resultAsTAC.getStatus());

    /* Print TAC */
    System.out.println("resultAsTAC ["+resultAsTAC.getConvertedMessage().get()+"]");

    /* Make sure the TAC is the same as always */
    String tac = "TAF EHAM 271659Z 2718/2824 200P99GP99KT CAVOK\n"+
      "FM272000 VRBP99GP99KT CAVOK=";
    assertEquals(tac, resultAsTAC.getConvertedMessage().get().replaceAll("\\r", ""));
  }
}
