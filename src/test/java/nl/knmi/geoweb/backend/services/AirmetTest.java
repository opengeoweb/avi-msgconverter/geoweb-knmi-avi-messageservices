package nl.knmi.geoweb.backend.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import nl.knmi.geoweb.backend.ApplicationConfig;
import nl.knmi.geoweb.backend.aviation.Tools;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.TestGeoWebAirmetToFMIIWXXM21;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.TestGeoWebAirmetToFMIIWXXM30;
import nl.knmi.geoweb.backend.product.GeoWebAirmet.TestGeoWebAirmetToFMIIWXXM20231;

@RunWith(SpringRunner.class)
@SpringBootTest
@Import(ApplicationConfig.class)
public class AirmetTest {

  private MockMvc mockMvc;

  /** The Spring web application context. */
  @Autowired
  private WebApplicationContext webApplicationContext;

  @Before
  public void setUp() {
    mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
  }

  @Test
  public void testPlainTextGet() throws Exception {
    mockMvc
      .perform(get("/").contentType(MediaType.TEXT_PLAIN).content(""))
      .andExpect(status().isOk());
  }

  public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(
    MediaType.APPLICATION_JSON.getType(),
    MediaType.APPLICATION_JSON.getSubtype(),
    Charset.forName("utf8")
  );

  @Test
  public void testApplicationJSONPost() throws Exception {
    String airmetJson = Tools.readResource(
      "airmet-readGeoWebAirmetAndCreateAirmet.json"
    );
    String airmetTac = Tools.readResource(
      "airmet-readGeoWebAirmetAndCreateAirmet.tac"
    );

    MvcResult result = mockMvc
      .perform(post("/getairmettac").contentType(APPLICATION_JSON_UTF8).content(airmetJson))
      .andExpect(status().isOk())
      .andReturn();

    String content = result.getResponse().getContentAsString();

    assertEquals(airmetTac, content);
  }

  @Test
  public void testApplicationJSONPostSFCVIS() throws Exception {
    String airmetJson = Tools.readResource(
      "airmet-readGeoWebAirmetAndCreateAirmetSFCVIS.json"
    );
    String airmetTac = Tools.readResource(
      "airmet-readGeoWebAirmetAndCreateAirmetSFCVIS.tac"
    );

    MvcResult result = mockMvc
      .perform(post("/getairmettac").contentType(APPLICATION_JSON_UTF8).content(airmetJson))
      .andExpect(status().isOk())
      .andReturn();

    String content = result.getResponse().getContentAsString();

    assertEquals(airmetTac, content);
  }

  @Test
  public void testAirmetPointTrailingZeroes() throws Exception {
    //Tests if a coordinate with 0 for the minutes part ends up with two trailing 00 in the TAC (N5200)
    //when using the AirmetController
    String airmetJson = Tools.readResource(
      "airmet-point-rounding.json"
    );
    String airmetTac = Tools.readResource(
      "airmet-point-rounding.tac"
    );
    MvcResult result = mockMvc
      .perform(post("/getairmettac").contentType(APPLICATION_JSON_UTF8).content(airmetJson))
      .andExpect(status().isOk())
      .andReturn();

    String content = result.getResponse().getContentAsString();

    assertEquals(content, airmetTac);
  }


  @Test
  public void testApplicationJSONPostSFCWIND() throws Exception {
    String airmetJson = Tools.readResource(
      "airmet-readGeoWebAirmetAndCreateAirmetSFCWIND.json"
    );
    String airmetTac = Tools.readResource(
      "airmet-readGeoWebAirmetAndCreateAirmetSFCWIND.tac"
    );

    MvcResult result = mockMvc
      .perform(post("/getairmettac").contentType(APPLICATION_JSON_UTF8).content(airmetJson))
      .andExpect(status().isOk())
      .andReturn();

    String content = result.getResponse().getContentAsString();

    assertEquals(airmetTac, content);
  }

  @Test
  public void testApplicationJSONPostBKNCLD() throws Exception {
    String airmetJson = Tools.readResource(
      "airmet-readGeoWebAirmetAndCreateAirmetBKNCLD.json"
    );
    String airmetTac = Tools.readResource(
      "airmet-readGeoWebAirmetAndCreateAirmetBKNCLD.tac"
    );

    MvcResult result = mockMvc
      .perform(post("/getairmettac").contentType(APPLICATION_JSON_UTF8).content(airmetJson))
      .andExpect(status().isOk())
      .andReturn();

    String content = result.getResponse().getContentAsString();

    assertEquals(airmetTac, content);
  }

  @Test
  public void testApplicationJSONPostBKNCLDSFC() throws Exception {
    String airmetJson = Tools.readResource(
      "airmet-readGeoWebAirmetAndCreateAirmetBKNCLDSFC.json"
    );
    String airmetTac = Tools.readResource(
      "airmet-readGeoWebAirmetAndCreateAirmetBKNCLDSFC.tac"
    );

    MvcResult result = mockMvc
      .perform(post("/getairmettac").contentType(APPLICATION_JSON_UTF8).content(airmetJson))
      .andExpect(status().isOk())
      .andReturn();

    String content = result.getResponse().getContentAsString();

    assertEquals(airmetTac, content);
  }

  @Test
  public void testApplicationJSONPostOVCCLDSFCABV() throws Exception {
    String airmetJson = Tools.readResource(
      "airmet-readGeoWebAirmetAndCreateAirmetOVCCLDSFCABV.json"
    );
    String airmetTac = Tools.readResource(
      "airmet-readGeoWebAirmetAndCreateAirmetOVCCLDSFCABV.tac"
    );

    MvcResult result = mockMvc
      .perform(post("/getairmettac").contentType(APPLICATION_JSON_UTF8).content(airmetJson))
      .andExpect(status().isOk())
      .andReturn();

    String content = result.getResponse().getContentAsString();

    assertEquals(airmetTac, content);
  }

  @Test
  public void testApplicationJSONPostOVCCLDABV() throws Exception {
    String airmetJson = Tools.readResource(
      "airmet-readGeoWebAirmetAndCreateAirmetOVCCLDABV.json"
    );
    String airmetTac = Tools.readResource(
      "airmet-readGeoWebAirmetAndCreateAirmetOVCCLDABV.tac"
    );

    MvcResult result = mockMvc
      .perform(post("/getairmettac").contentType(APPLICATION_JSON_UTF8).content(airmetJson))
      .andExpect(status().isOk())
      .andReturn();

    String content = result.getResponse().getContentAsString();

    assertEquals(airmetTac, content);
  }

  @Test
  public void testAirmetgetAirmetiwxxm21() throws Exception {
    String airmetJson = Tools.readResource(
      "./geowebairmets/published.json"
    );
    String airmetIWXXM21 = Tools.readResource(
      "./geowebairmets/published-FMI.IWXXM21"
    );
    MvcResult result = mockMvc
      .perform(post("/getairmetiwxxm21").contentType(MediaType.APPLICATION_JSON_VALUE).content(airmetJson))
      .andExpect(status().isOk())
      .andReturn();

    String content = result.getResponse().getContentAsString();

    assertFalse(TestGeoWebAirmetToFMIIWXXM21.iwxxmHasDifferences(content, airmetIWXXM21));
  }

  @Test
  public void testAirmetgetAirmetiwxxm30() throws Exception {
    String airmetJson = Tools.readResource(
      "./geowebairmets/published.json"
    );
    String airmetIWXXM30 = Tools.readResource(
      "./geowebairmets/published-FMI.IWXXM30"
    );
    MvcResult result = mockMvc
      .perform(post("/getairmetiwxxm30").contentType(MediaType.APPLICATION_JSON_VALUE).content(airmetJson))
      .andExpect(status().isOk())
      .andReturn();

    String content = result.getResponse().getContentAsString();
    assertFalse(TestGeoWebAirmetToFMIIWXXM30.iwxxmHasDifferences(content, airmetIWXXM30));
  }

  @Test
  public void testAirmetgetAirmetiwxxm20231() throws Exception {
    String airmetJson = Tools.readResource(
      "./geowebairmets/published.json"
    );
    String airmetIWXXM20231 = Tools.readResource(
      "./geowebairmets/published-FMI.IWXXM20231"
    );
    MvcResult result = mockMvc
      .perform(post("/getairmetiwxxm2023_1").contentType(MediaType.APPLICATION_JSON_VALUE).content(airmetJson))
      .andExpect(status().isOk())
      .andReturn();

    String content = result.getResponse().getContentAsString();
    assertFalse(TestGeoWebAirmetToFMIIWXXM20231.iwxxmHasDifferences(content, airmetIWXXM20231));
  }

  @Test
  public void testAirmetgetairmettac_nogeom() throws Exception {
    //Tests making a AIRMET TAC when the startGeometry is missing
    String airmetJson = Tools.readResource(
      "./geowebairmets/testairmet_nogeom.json"
    );
    MvcResult result = mockMvc
      .perform(post("/getairmettac").contentType(MediaType.APPLICATION_JSON).content(airmetJson))
      .andExpect(status().isBadRequest())
      .andReturn();

      String content = result.getResponse().getContentAsString();
      assert(content.contains("AIRMET start area missing"));
  }

  @Test
  public void testAirmetCancelsSfcWind() throws Exception {
    //Tests making a AIRMET TAC that cancels a SFC_WIND AIRMET
    String airmetJson = Tools.readResource(
      "./geowebairmets/airmetcancelsfcwind.json"
    );
    MvcResult result = mockMvc
      .perform(post("/getairmettac").contentType(MediaType.APPLICATION_JSON).content(airmetJson))
      .andExpect(status().isOk())
      .andReturn();

    String content = result.getResponse().getContentAsString();
    assert(content.contains("EHAA AMSTERDAM FIR CNL AIRMET 1"));
  }

  @Test
  public void testAirmetCancelsSfcVis() throws Exception {
    //Tests making a AIRMET TAC that cancels a SFC_VIS AIRMET
    String airmetJson = Tools.readResource(
      "./geowebairmets/airmetcancelsfcvis.json"
    );
    MvcResult result = mockMvc
      .perform(post("/getairmettac").contentType(MediaType.APPLICATION_JSON).content(airmetJson))
      .andExpect(status().isOk())
      .andReturn();

    String content = result.getResponse().getContentAsString();
    assert(content.contains("EHAA AMSTERDAM FIR CNL AIRMET 1"));
  }

  @Test
  public void testAirmetCancelsBknCld_totac() throws Exception {
    //Tests making a AIRMET TAC that cancels a BKN_CLD AIRMET
    String airmetJson = Tools.readResource(
      "./geowebairmets/airmetcancelbkncld.json"
    );
    MvcResult result = mockMvc
      .perform(post("/getairmettac").contentType(MediaType.APPLICATION_JSON).content(airmetJson))
      .andExpect(status().isOk())
      .andReturn();

    String content = result.getResponse().getContentAsString();
    assert(content.contains("EHAA AMSTERDAM FIR CNL AIRMET 1"));
  }

  @Test
  public void testAirmetCancelsBknCld_toiwxxm21() throws Exception {
    //Tests making a AIRMET IWXXM21 that cancels a BKN_CLD AIRMET
    String airmetJson = Tools.readResource(
      "./geowebairmets/airmetcancelbkncld.json"
    );
    MvcResult result = mockMvc
      .perform(post("/getairmetiwxxm21").contentType(MediaType.APPLICATION_JSON).content(airmetJson))
      .andExpect(status().isOk())
      .andReturn();

    String content = result.getResponse().getContentAsString();
    assert(content.contains("status=\"CANCELLATION\""));
  }

  @Test
  public void testAirmetCancelsBknCld_toiwxxm30() throws Exception {
    //Tests making a AIRMET IWXXM30 that cancels a BKN_CLD AIRMET
    String airmetJson = Tools.readResource(
      "./geowebairmets/airmetcancelbkncld.json"
    );
    MvcResult result = mockMvc
      .perform(post("/getairmetiwxxm30").contentType(MediaType.APPLICATION_JSON).content(airmetJson))
      .andExpect(status().isOk())
      .andReturn();

    String content = result.getResponse().getContentAsString();
    assert(content.contains("isCancelReport=\"true\""));
  }

  @Test
  public void testAirmetCancelsBknCld_toiwxxm2023_1() throws Exception {
    //Tests making a AIRMET IWXXM2023.1 that cancels a BKN_CLD AIRMET
    String airmetJson = Tools.readResource(
      "./geowebairmets/airmetcancelbkncld.json"
    );
    MvcResult result = mockMvc
      .perform(post("/getairmetiwxxm2023_1").contentType(MediaType.APPLICATION_JSON).content(airmetJson))
      .andExpect(status().isOk())
      .andReturn();

    String content = result.getResponse().getContentAsString();
    assert(content.contains("isCancelReport=\"true\""));
  }
}
