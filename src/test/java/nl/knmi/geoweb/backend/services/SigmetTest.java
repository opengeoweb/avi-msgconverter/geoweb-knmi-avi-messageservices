package nl.knmi.geoweb.backend.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;
import nl.knmi.geoweb.backend.ApplicationConfig;
import nl.knmi.geoweb.backend.aviation.Tools;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.TestGeoWebSigmetToFMIIWXXM21;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.TestGeoWebSigmetToFMIIWXXM30;
import nl.knmi.geoweb.backend.product.GeoWebSigmet.TestGeoWebSigmetToFMIIWXXM20231;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
@Import(ApplicationConfig.class)
public class SigmetTest {

  private MockMvc mockMvc;

  /** The Spring web application context. */
  @Autowired
  private WebApplicationContext webApplicationContext;

  @Before
  public void setUp() {
    mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
  }

  @Test
  public void testPlainTextGet() throws Exception {
    mockMvc
      .perform(get("/").contentType(MediaType.TEXT_PLAIN).content(""))
      .andExpect(status().isOk());
  }

  public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(
    MediaType.APPLICATION_JSON.getType(),
    MediaType.APPLICATION_JSON.getSubtype(),
    Charset.forName("utf8")
  );

  @Test
  public void testApplicationJSONPostFMI() throws Exception {
    String sigmetJson = Tools.readResource(
      "sigmet-readGeoWebSigmetAndCreateSigmet.json"
    );
    String sigmetTac = Tools.readResource(
      "sigmet-readGeoWebSigmetAndCreateSigmet-FMI.tac"
    );
    MvcResult result = mockMvc
      .perform(post("/getsigmettac").contentType(APPLICATION_JSON_UTF8).content(sigmetJson))
      .andExpect(status().isOk())
      .andReturn();

    String content = result.getResponse().getContentAsString();

    assertEquals(content, sigmetTac);
  }
  @Test
  public void testApplicationJSONPostFMImoving() throws Exception {
    String sigmetJson = Tools.readResource(
      "sigmet-readGeoWebSigmetAndCreateSigmetmoving.json"
    );
    String sigmetTac = Tools.readResource(
      "sigmet-readGeoWebSigmetAndCreateSigmetmoving-FMI.tac"
    );
    MvcResult result = mockMvc
      .perform(post("/getsigmettac").contentType(APPLICATION_JSON_UTF8).content(sigmetJson))
      .andExpect(status().isOk())
      .andReturn();

    String content = result.getResponse().getContentAsString();

    assertEquals(content, sigmetTac);
  }

  @Test
  public void testApplicationJSONPostBox() throws Exception {
    String sigmetJson = Tools.readResource(
      "sigmet-readGeoWebSigmetAndCreateSigmetbox.json"
    );
    String sigmetTac = Tools.readResource(
      "sigmet-readGeoWebSigmetAndCreateSigmetbox-FMI.tac"
    );
    MvcResult result = mockMvc
      .perform(post("/getsigmettac").contentType(APPLICATION_JSON_UTF8).content(sigmetJson))
      .andExpect(status().isOk())
      .andReturn();

    String content = result.getResponse().getContentAsString();

    assertEquals(content, sigmetTac);
  }

  @Test
  public void testSigmetPointTrailingZeroes() throws Exception {
    //Tests if a coordinate with 0 for the minutes part ends up with two trailing 00 in the TAC (N5200)
    //when using the SigmetController
    String sigmetJson = Tools.readResource(
      "sigmet-point-rounding.json"
    );
    String sigmetTac = Tools.readResource(
      "sigmet-point-rounding.tac"
    );
    MvcResult result = mockMvc
      .perform(post("/getsigmettac").contentType(APPLICATION_JSON_UTF8).content(sigmetJson))
      .andExpect(status().isOk())
      .andReturn();

    String content = result.getResponse().getContentAsString();

    assertEquals(content, sigmetTac);
  }

  @Test
  public void testSigmetgetsigmetiwxxm21() throws Exception {
    String sigmetJson = Tools.readResource(
      "./geowebsigmets/published.json"
    );
    String sigmetIWXXM21 = Tools.readResource(
      "./geowebsigmets/published-FMI.IWXXM21"
    );
    MvcResult result = mockMvc
      .perform(post("/getsigmetiwxxm21").contentType(MediaType.APPLICATION_JSON_VALUE).content(sigmetJson))
      .andExpect(status().isOk())
      .andReturn();

    String content = result.getResponse().getContentAsString();

    assertFalse(TestGeoWebSigmetToFMIIWXXM21.iwxxmHasDifferences(content, sigmetIWXXM21));
  }

  @Test
  public void testSigmetgetsigmetiwxxm30() throws Exception {
    String sigmetJson = Tools.readResource(
      "./geowebsigmets/published.json"
    );
    String sigmetIWXXM30 = Tools.readResource(
      "./geowebsigmets/published-FMI.IWXXM30"
    );
    MvcResult result = mockMvc
      .perform(post("/getsigmetiwxxm30").contentType(MediaType.APPLICATION_JSON_VALUE).content(sigmetJson))
      .andExpect(status().isOk())
      .andReturn();

    String content = result.getResponse().getContentAsString();
    assertFalse(TestGeoWebSigmetToFMIIWXXM30.iwxxmHasDifferences(content, sigmetIWXXM30));
  }

  @Test
  public void testSigmetgetsigmetiwxxm2023_1() throws Exception {
    String sigmetJson = Tools.readResource(
      "./geowebsigmets/published.json"
    );
    String sigmetIWXXM20231 = Tools.readResource(
      "./geowebsigmets/published-FMI.IWXXM20231"
    );
    MvcResult result = mockMvc
      .perform(post("/getsigmetiwxxm2023_1").contentType(MediaType.APPLICATION_JSON_VALUE).content(sigmetJson))
      .andExpect(status().isOk())
      .andReturn();

    String content = result.getResponse().getContentAsString();
    assertFalse(TestGeoWebSigmetToFMIIWXXM20231.iwxxmHasDifferences(content, sigmetIWXXM20231));
  }

  @Test
  public void testSigmetgetsigmettac_nogeom() throws Exception {
    //Tests making a SIGMET TAC when the startGeometry is missing
    String sigmetJson = Tools.readResource(
      "./geowebsigmets/testsigmet_nogeom.json"
    );
    MvcResult result = mockMvc
      .perform(post("/getsigmettac").contentType(MediaType.APPLICATION_JSON).content(sigmetJson))
      .andExpect(status().isBadRequest())
      .andReturn();

      String content = result.getResponse().getContentAsString();
      assert(content.contains("SIGMET start area missing"));
  }

}
