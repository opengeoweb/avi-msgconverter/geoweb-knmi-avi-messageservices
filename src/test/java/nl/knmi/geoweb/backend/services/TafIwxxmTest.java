package nl.knmi.geoweb.backend.services;

import static org.junit.Assert.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;
import lombok.extern.slf4j.Slf4j;
import nl.knmi.geoweb.backend.aviation.Tools;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.diff.Diff;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@Import(TestServicesConfig.class)
public class TafIwxxmTest {

  private MockMvc mockMvc;

  /** The Spring web application context. */
  @Autowired
  private WebApplicationContext webApplicationContext;

  @Before
  public void setUp() {
    mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
  }

  @Test
  public void testPlainTextGet() throws Exception {
    mockMvc
      .perform(get("/").contentType(MediaType.APPLICATION_XML).content(""))
      .andExpect(status().isOk());
  }

  public static final MediaType APPLICATION_XML = new MediaType(
    MediaType.APPLICATION_XML.getType(),
    MediaType.APPLICATION_XML.getSubtype(),
    Charset.forName("utf8")
  );

  public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(
    MediaType.APPLICATION_JSON.getType(),
    MediaType.APPLICATION_JSON.getSubtype(),
    Charset.forName("utf8")
  );

    /**
   * Compare two IWXXM documents and check if they are equal, leaving out unique id's.
   * @param expected
   * @param result
   * @return
   */
  static public Boolean iwxxmHasDifferences (String expected, String result) {
    Diff xmlDiff = DiffBuilder.compare(expected)
    .withTest(result)
    .withAttributeFilter(attr -> {
      return !((attr.getName().equals("gml:id") || attr.getName().equals("xlink:href")) || attr.getName().equals("xsi:schemaLocation"));
    }).withNodeFilter(node -> {
      return !(node.getNodeName().equals("gml:timePosition"));
    })
    .ignoreComments()
    .ignoreWhitespace()
    .build();
    if (xmlDiff.hasDifferences()) {
      log.error(xmlDiff.toString());
    }
    return xmlDiff.hasDifferences();
  }

  public void testApplicationJSONPost(String tafName) throws Exception {
    String tafJson = Tools.readResource(
      tafName+".json"
    );

    MvcResult result21 = mockMvc
      .perform(post("/gettafiwxxm21").contentType(APPLICATION_JSON_UTF8).content(tafJson))
      .andExpect(status().isOk())
      .andReturn();

    String content21 = result21.getResponse().getContentAsString();
    log.info(content21);
    String tafIwxxm21 = Tools.readResource(
      tafName+".iwxxm21"
    );
    assertFalse(iwxxmHasDifferences(tafIwxxm21, content21));

    MvcResult result = mockMvc
      .perform(post("/gettafiwxxm30").contentType(APPLICATION_JSON_UTF8).content(tafJson))
      .andExpect(status().isOk())
      .andReturn();

    String content30 = result.getResponse().getContentAsString();
    log.info(content30);
    String tafIwxxm30 = Tools.readResource(
      tafName+".iwxxm30"
    );
    assertFalse(iwxxmHasDifferences(tafIwxxm30, content30));
  }

  @Test
  public void testTaf() throws Exception {
    testApplicationJSONPost("./taf/geowebtaf/geowebtaf");
  }

  @Test
  public void testTafAmended() throws Exception {
    testApplicationJSONPost("./taf/geowebtaf/amendedtaf");
  }

  @Test
  public void testTafCancelleded() throws Exception {
    testApplicationJSONPost("./taf/geowebtaf/cancelledtaf");
  }

  @Test
  public void testTafCorrectedTaf() throws Exception {
    testApplicationJSONPost("./taf/geowebtaf/correctedtaf");
  }

  @Test
  public void testTafDraftCorrectedTaf() throws Exception {
    testApplicationJSONPost("./taf/geowebtaf/draftcorrected");
  }

  @Test
  public void testTafExpiredTaf() throws Exception {
    testApplicationJSONPost("./taf/geowebtaf/expired");
  }

  @Test
  public void testTafNscInBaseForecast() throws Exception {
    testApplicationJSONPost("./taf/geowebtaf/nsc-in-baseforecast-taf");
  }

  @Test
  public void testTafNscInChangeForecast() throws Exception {
    testApplicationJSONPost("./taf/geowebtaf/nsc-in-changegroup-taf");
  }

  @Test
  public void testTafSmallValidTafWithBaseForecast() throws Exception {
    testApplicationJSONPost("./taf/geowebtaf/smallvalidtafwithbaseforecast");
  }

  @Test
  public void testTafWithProb() throws Exception {
    testApplicationJSONPost("./taf/geowebtaf/taf-with-prob");
  }

  @Test
  public void testTafWithVerticalVisibilityInBaseForecast() throws Exception {
    testApplicationJSONPost("./taf/geowebtaf/verticalvisibilitytaf-baseforecast");
  }

  @Test
  public void testTafWithVerticalVisibilityInChangeGroup() throws Exception {
    testApplicationJSONPost("./taf/geowebtaf/verticalvisibilitytaf-changegroup");
  }

  @Test
  public void testTafWindBaseForecast() throws Exception {
    testApplicationJSONPost("./taf/geowebtaf/wind-baseforecast-taf");
  }

  @Test
  public void testTafWindVrbInBaseForecast() throws Exception {
    testApplicationJSONPost("./taf/geowebtaf/windvrb-inbaseforecast");
  }

  @Test
  public void testTafWindVrbInChangeGroup() throws Exception {
    testApplicationJSONPost("./taf/geowebtaf/windvrb-inchangegroup");
  }

  @Test
  public void testTafWithCavok() throws Exception {
    testApplicationJSONPost("./taf/geowebtaf/withCavok");
  }

  @Test
  public void testTafWindP99InBaseForecast() throws Exception {
    testApplicationJSONPost("./taf/geowebtaf/windp99-inbaseforecast");
  }

}
