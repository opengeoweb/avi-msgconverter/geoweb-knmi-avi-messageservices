package nl.knmi.geoweb.backend.services;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;
import nl.knmi.geoweb.backend.aviation.Tools;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
@Import(TestServicesConfig.class)
public class TafTest {

  private MockMvc mockMvc;

  /** The Spring web application context. */
  @Autowired
  private WebApplicationContext webApplicationContext;

  @Before
  public void setUp() {
    mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
  }

  @Test
  public void testPlainTextGet() throws Exception {
    mockMvc
      .perform(get("/").contentType(MediaType.TEXT_PLAIN).content(""))
      .andExpect(status().isOk());
  }

  public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(
    MediaType.APPLICATION_JSON.getType(),
    MediaType.APPLICATION_JSON.getSubtype(),
    Charset.forName("utf8")
  );

  @Test
  public void testApplicationJSONPost() throws Exception {
    String tafJson = Tools.readResource("./taf/geowebtaf/geowebtaf.json");
    String tafTac = Tools.readResource("./taf/geowebtaf/geowebtaf.tac");
    MvcResult result = mockMvc
        .perform(post("/gettaftac").contentType(APPLICATION_JSON_UTF8).content(tafJson))
        .andExpect(status().isOk())
        .andReturn();
    String content = result.getResponse().getContentAsString();
    assertEquals(content, tafTac);
  }

  @Test
  public void testApplicationJSONPostAcceptAll() throws Exception {
    String tafJson = Tools.readResource("./taf/geowebtaf/geowebtaf.json");
    String tafTac = Tools.readResource("./taf/geowebtaf/geowebtaf.tac");
    MvcResult result = mockMvc
        .perform(post("/gettaftac").accept(MediaType.ALL).contentType(APPLICATION_JSON_UTF8).content(tafJson))
        .andExpect(status().isOk())
        .andReturn();
    String content = result.getResponse().getContentAsString();
    assertEquals(content, tafTac);
  }

  @Test
  public void testApplicationJSONPostAcceptPlainText() throws Exception {
    String tafJson = Tools.readResource("./taf/geowebtaf/geowebtaf.json");
    String tafTac = Tools.readResource("./taf/geowebtaf/geowebtaf.tac");
    MvcResult result = mockMvc
        .perform(post("/gettaftac").accept(MediaType.TEXT_PLAIN).contentType(APPLICATION_JSON_UTF8).content(tafJson))
        .andExpect(status().isOk())
        .andReturn();
    String content = result.getResponse().getContentAsString();
    assertEquals(content, tafTac);
  }

  @Test
  public void testApplicationJSONPostAcceptJSON() throws Exception {
    String tafJson = Tools.readResource("./taf/geowebtaf/geowebtaf.json");
    String tafTac = Tools.readResource("./taf/geowebtaf/geowebtaf.tac.json");
    MvcResult result = mockMvc
        .perform(
            post("/gettaftac").accept(MediaType.APPLICATION_JSON).contentType(APPLICATION_JSON_UTF8).content(tafJson))
        .andExpect(status().isOk())
        .andReturn();
    String content = result.getResponse().getContentAsString();
    assertEquals(content, tafTac);
  }

  @Test
  public void testApplicationJSONPostAcceptJSON_endsat2400() throws Exception {
    String tafJson = Tools.readResource("./taf/geowebtaf/geowebtaf_endsat2400.json");
    String tafTac = Tools.readResource("./taf/geowebtaf/geowebtaf_endsat2400.tac.json");
    MvcResult result = mockMvc
        .perform(
            post("/gettaftac").accept(MediaType.APPLICATION_JSON).contentType(APPLICATION_JSON_UTF8).content(tafJson))
        .andExpect(status().isOk())
        .andReturn();
    String content = result.getResponse().getContentAsString();
    assertEquals(content, tafTac);
  }

  @Test
  public void test_taf2tac_no_changegroup_time() throws Exception {
    String tafJson = Tools.readResource("./taf/geowebtaf/taf2tac_no_changegroup_time.json");
    MvcResult result = mockMvc
        .perform(
            post("/gettaftac").accept(MediaType.ALL_VALUE).contentType(APPLICATION_JSON_UTF8).content(tafJson))
        .andExpect(status().is(400))
        .andReturn();
    String content = result.getResponse().getContentAsString();
    assert(content.contains("Validity time start is not available in TAF change forecast"));
  }

  @Test
  public void test_taf2tac_no_changegroup_time_json() throws Exception {
    String tafJson = Tools.readResource("./taf/geowebtaf/taf2tac_no_changegroup_time.json");
    MvcResult result = mockMvc
        .perform(
            post("/gettaftac").accept(MediaType.APPLICATION_JSON).contentType(APPLICATION_JSON_UTF8).content(tafJson))
        .andExpect(status().is(400))
        .andReturn();
    String content = result.getResponse().getContentAsString();
    assert(content.contains("Validity time start is not available in TAF change forecast"));
  }


  @Test
  public void testApplicationWithIncorrectCancelledTAF() throws Exception {
    String tafJson = Tools.readResource(
      "./taf/geowebtaf/cancelledtafwithmissingprevious.json"

    );
    String tafTac = Tools.readResource(
      "./taf/geowebtaf/cancelledtafwithmissingprevious.tac"
    );
    MvcResult result = mockMvc
      .perform(post("/gettaftac").contentType(APPLICATION_JSON_UTF8).content(tafJson))
      .andExpect(status().is(400))
      .andReturn();

    String content = result.getResponse().getContentAsString().replaceAll("\\r", "").trim();

    System.out.println("["+content+"]");
    System.out.println("["+tafTac+"]");

    assertEquals(content, tafTac);
  }

  @Test
  public void testApplicationWithIncorrectFromProb30TAF() throws Exception {
    String tafJson = Tools.readResource(
      "./taf/geowebtaf/testinvalidtaf-fromprob30.json"
    );
    MvcResult result = mockMvc
      .perform(post("/gettaftac").contentType(APPLICATION_JSON_UTF8).content(tafJson))
      .andExpect(status().is(400))
      .andReturn();

    String content = result.getResponse().getContentAsString().replaceAll("\\r", "").trim();

    String expectedMessage="Unable to translate GeoWeb JSON to FMI Model\n"+
      "SYNTAX:updateChangeForecasts: Combination of PROB30 and FM is not allowed.";

    assertEquals(expectedMessage, content);
  }

  @Test
  public void testDraftAmendedEndpoint() throws Exception {
    /* Load the GeoWeb Taf JSON and convert it to a GeoWeb TAF POJO */
    String tafJson = Tools.readResource("./taf/geowebtaf/draftamended.json");
    String tafTac = Tools.readResource("./taf/geowebtaf/draftamended.tac");


    MvcResult result = mockMvc
    .perform(post("/gettaftac").contentType(APPLICATION_JSON_UTF8).content(tafJson))
    .andExpect(status().is(200))
    .andReturn();

    String content = result.getResponse().getContentAsString().replaceAll("\\r", "").trim();

    System.out.println("["+content+"]");
    System.out.println("["+tafTac+"]");

    assertEquals(content, tafTac);
  }

}
