package nl.knmi.geoweb.backend.services;

import static org.mockito.Mockito.mock;

import nl.knmi.geoweb.backend.aviation.FIRStore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@Profile("test")
@Configuration
public class TestServicesConfig {

  @Bean
  @Primary
  public FIRStore getFirStore() {
    return mock(FIRStore.class);
  }
}
