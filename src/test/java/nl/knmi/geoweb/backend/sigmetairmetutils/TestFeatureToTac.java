package nl.knmi.geoweb.backend.sigmetairmetutils;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.geojson.Feature;
import org.junit.Test;

import lombok.extern.slf4j.Slf4j;
import nl.knmi.geoweb.backend.aviation.Tools;
import nl.knmi.geoweb.backend.product.sigmetairmet.SigmetAirmetUtils;

@Slf4j
public class TestFeatureToTac {
    ObjectMapper om=null;

    private ObjectMapper getObjectMapper() {
        if (om==null) {
            om = new ObjectMapper();
        }
        return om;

    }

    public Feature getFir() {
        String firJson;
        Feature feature=null;
        try {
            firJson = Tools.readResource("sigmetairmetutils/featureFir.json");
            feature = getObjectMapper().readValue(firJson, Feature.class);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return feature;

    }

    public Feature getBoxInsideFir() {
        String featureJson;
        Feature feature=null;
        try {
            featureJson = Tools.readResource("sigmetairmetutils/featureBoxInsideFir.json");
            feature = getObjectMapper().readValue(featureJson, Feature.class);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return feature;
    }
  @Test
  public void testFeaturebox() {
      Feature box = getBoxInsideFir();
      Feature fir = getFir();
      Feature intersection = getBoxInsideFir(); // Completely inside FIR so same polygon
      log.info("FIR:"+fir);

      String tac = SigmetAirmetUtils.featureToTAC(box, getFir(), "box", intersection);
      assertEquals("WI N5200 E00500 - N5300 E00500 - N5300 E00600 - N5200 E00600 - N5200 E00500", tac);
  }
}
